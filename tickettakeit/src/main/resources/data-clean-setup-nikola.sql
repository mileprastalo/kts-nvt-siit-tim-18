/* Adds authorities */
INSERT INTO `authority` (`id`, `name`) VALUES ('1', 'REGISTERED');
INSERT INTO `authority` (`id`, `name`) VALUES ('2', 'ADMIN');

/* Adds registered users */
INSERT INTO `registered_user` (`id`, `confirmed`, `email`, `first_name`, `last_name`, `password`, `phone`, `username`, `paypal`) VALUES ('100', TRUE, 'user@mail.com', 'User', 'Useric', '$2y$12$7agwOLLBYv6.GzIxJnK3QOd/yeRMfYb1A6TIHJf.nS2UDfDOD0MJu', '03024104', 'user', 'nema');
/* Adds admin user */
INSERT INTO `registered_user` (`id`, `confirmed`, `email`, `first_name`, `last_name`, `password`, `phone`, `username`, `paypal`) VALUES ('200', TRUE, 'admin@mail.com', 'Admin', 'Adminic', '$2y$12$7agwOLLBYv6.GzIxJnK3QOd/yeRMfYb1A6TIHJf.nS2UDfDOD0MJu', '03024104', 'admin', 'nema');

/* Adds authorities to users */
INSERT INTO `user_authority` (`user_id`, `authority_id`) VALUES ('100', '1');
INSERT INTO `user_authority` (`user_id`, `authority_id`) VALUES ('200', '2');

/*Categories */
INSERT INTO `tickettakeit`.`category` (`id`, `color`, `icon`, `name`) VALUES ('100', 'blue', 'category_icons/MusicFestival.jpg', 'Music Festival');
INSERT INTO `tickettakeit`.`category` (`id`, `color`, `icon`, `name`) VALUES ('200', 'red', 'category_icons/ArtExibition.jpg', 'Art Exibition');
INSERT INTO `tickettakeit`.`category` (`id`, `color`, `icon`, `name`) VALUES ('300', 'green', 'category_icons/Sport.jpg', 'Sport');
INSERT INTO `tickettakeit`.`category` (`id`, `color`, `icon`, `name`) VALUES ('400', 'pink', 'category_icons/Cinema.jpg', 'Cinema');

/* Adds locations */
INSERT INTO `tickettakeit`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('100', 'Sutjeska 2', 'Novi Sad', 'Test Image 1', '45.255826', '19.8453', 'Spens', TRUE);
INSERT INTO `tickettakeit`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('200', 'Bulevar Arsenija Carnojevica 58', 'Belgrade', 'Test Image 2', '44.8141','20.4213', 'Beogradska arena', TRUE);
INSERT INTO `tickettakeit`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('300', 'Milentija Popovica 9', 'Novi Sad', 'Test Image 3', '44.8085', '20.4320', 'Sava Centar', TRUE);

/* Adds halls */
INSERT INTO `tickettakeit`.`hall` (`id`, `name`, `active`) VALUES ('100', 'Hall 1', TRUE);
INSERT INTO `tickettakeit`.`hall` (`id`, `name`, `active`) VALUES ('200', 'Hall 2', TRUE);
INSERT INTO `tickettakeit`.`hall` (`id`, `name`, `active`) VALUES ('300', 'Hall 3', TRUE);
INSERT INTO `tickettakeit`.`hall` (`id`, `name`, `active`) VALUES ('400', 'Hall 4', TRUE);

/* Adds halls to locations */
INSERT INTO `tickettakeit`.`location_halls` (`location_id`, `halls_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`location_halls` (`location_id`, `halls_id`) VALUES ('200', '200');
INSERT INTO `tickettakeit`.`location_halls` (`location_id`, `halls_id`) VALUES ('200', '300');
INSERT INTO `tickettakeit`.`location_halls` (`location_id`, `halls_id`) VALUES ('300', '400');

/* Adds sections */
INSERT INTO `tickettakeit`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`,`active`,`left_position`,`top`) VALUES ('100', '10', TRUE, 'Section 1', '100', '10',TRUE,0,0);
INSERT INTO `tickettakeit`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`,`active`,`left_position`,`top`) VALUES ('200', '0', FALSE, 'Section 2', '100', '0',TRUE,0,0);
INSERT INTO `tickettakeit`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`,`active`,`left_position`,`top`) VALUES ('300', '10', TRUE, 'Section 1', '100', '10',TRUE,0,0);
INSERT INTO `tickettakeit`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`,`active`,`left_position`,`top`) VALUES ('400', '0', FALSE, 'Section 2', '100', '0',TRUE,0,0);
INSERT INTO `tickettakeit`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`,`active`,`left_position`,`top`) VALUES ('500', '10', TRUE, 'Section 1', '100', '10',TRUE,0,0);
INSERT INTO `tickettakeit`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`,`active`,`left_position`,`top`) VALUES ('600', '0', FALSE, 'Section 2', '100', '0',TRUE,0,0);

/* Adds sections to halls */
INSERT INTO `tickettakeit`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('100', '200');
INSERT INTO `tickettakeit`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('200', '300');
INSERT INTO `tickettakeit`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('200', '400');
INSERT INTO `tickettakeit`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('300', '500');
INSERT INTO `tickettakeit`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('300', '600');

/* Adds events */
INSERT INTO `tickettakeit`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('100', FALSE, 'Lara Fabian finally on May 10, 2020 at Stark Arena.', '4', 'Lara Fabian', '2020-2-26', '0', '100', '100');
INSERT INTO `tickettakeit`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('200', FALSE, 'Traditional winter OTDG costume ball.', '3', 'Brod Zeppelin', '2020-2-28', '0', '200', '200');
INSERT INTO `tickettakeit`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('300', FALSE, 'Basketball', '3', 'Partizan-Bologna', '2020-1-28', '0', '100', '100');
INSERT INTO `tickettakeit`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('400', FALSE, 'MAC will expand, and in 2020 Slovenia will participate in it, in addition to Croatia, Serbia, Bosnia and Herzegovina, Northern Macedonia and Montenegro.', '3', 'MUSIC AWARDS CEREMONY (MAC)', '2020-2-29', '1', '100', '100');
INSERT INTO `tickettakeit`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('500', FALSE, 'European and British tour 2020 New Ghosteen album.', '3', 'NICK CAVE AND THE BAD SEEDS', '2020-2-08', '0', '200', '200');

/* Adds categories to events */
INSERT INTO `tickettakeit`.`event_categories` (`event_id`, `categories_id`) VALUES ('100', '100'); 
INSERT INTO `tickettakeit`.`event_categories` (`event_id`, `categories_id`) VALUES ('100', '200');
INSERT INTO `tickettakeit`.`event_categories` (`event_id`, `categories_id`) VALUES ('200', '100');
INSERT INTO `tickettakeit`.`event_categories` (`event_id`, `categories_id`) VALUES ('300', '300');
INSERT INTO `tickettakeit`.`event_categories` (`event_id`, `categories_id`) VALUES ('400', '100');
INSERT INTO `tickettakeit`.`event_categories` (`event_id`, `categories_id`) VALUES ('500', '100');

/* Adds dates to events */
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('100', '2020-03-01');
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('100', '2020-03-02');
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('100', '2020-03-03');

INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('200', '2020-03-02');
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('200', '2020-03-03');

INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('300', '2020-03-02');
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('400', '2020-03-03');

INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('500', '2020-02-28');
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('500', '2020-02-29');

/* Adds event sections */
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('100', '10', TRUE, 'Section 1', '100', '200', '10',0,0);
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('200', '0', FALSE, 'Section 2', '100', '100', '0',0,0);
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('300', '10', TRUE, 'Section 1', '100', '200', '10',0,0);
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('400', '0', FALSE, 'Section 2', '100', '100', '0',0,0);
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('500', '10', TRUE, 'Section 1', '100', '200', '10',0,0);
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('600', '10', TRUE, 'Section 1', '100', '200', '10',0,0);
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('700', '10', TRUE, 'Section 1', '100', '200', '10',0,0);
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('800', '10', TRUE, 'Section 1', '100', '200', '10',0,0);

/* Adds event sections to events */
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('100', '200');
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('200', '300');
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('200', '400');
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('300', '500');
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('400', '600');
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('500', '700');
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('500', '800');


/* Adds Ticket */
INSERT INTO `tickettakeit`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`)
VALUES ('100', TRUE, '10 10 Test', '5', '2019-12-28', '2020-03-01', '200', '5', '100', '100', '100');
INSERT INTO `tickettakeit`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`)
VALUES ('200', FALSE, '10 10 Test', '6', '2020-02-28', '2020-03-01', '200', '5', '100', '200', '100');
INSERT INTO `tickettakeit`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`)
VALUES ('300', TRUE, '10 10 Test', '7', '2019-12-28', '2020-03-02', '200', '7', '200', '400', '100');
INSERT INTO `tickettakeit`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`)
VALUES ('400', FALSE, '10 10 Test', '6', '2020-02-28', '2020-03-02', '200', '5', '200', '300', '100');
INSERT INTO `tickettakeit`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`)
VALUES ('500', TRUE, '10 10 Test', '7', '2020-02-23', '2020-02-28', '200', '7', '500', '800', '100');
INSERT INTO `tickettakeit`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`)
VALUES ('600', FALSE, '10 10 Test', '6', '2020-02-24', '2020-02-29', '200', '5', '500', '700', '100');

/* Adds Ticket to it's User */
INSERT INTO `tickettakeit`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('100', '200');
INSERT INTO `tickettakeit`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('100', '300');
INSERT INTO `tickettakeit`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('100', '400');
INSERT INTO `tickettakeit`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('100', '500');
INSERT INTO `tickettakeit`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('100', '600');

INSERT INTO `tickettakeit`.`event_application` (`id`, `queue_number`, `event_id`, `user_id`) VALUES ('100', '10', '100', '100');
INSERT INTO `tickettakeit`.`event_application` (`id`, `queue_number`, `event_id`, `user_id`) VALUES ('200', '5', '200', '100');

