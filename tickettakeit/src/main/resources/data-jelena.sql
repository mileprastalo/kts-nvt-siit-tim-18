
/* AUTHORITY */
insert into authority (id, name) values (1, 'REGISTERED');
insert into authority (id, name) values (2, 'ADMIN');

/* USERS */
-- password is 'user' (bcrypt encoded)
insert into registered_user (id, confirmed, email, first_name, last_name, password, phone, username, paypal)
values ( 100, true, 'mira@gmail.com', 'Mira', 'Miric','$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq', '023/848-111', 'mira', 'paypal');
-- non-activated user profile
insert into registered_user (id, confirmed, email, first_name, last_name, password, phone, username, paypal)
values ( 101, false, 'jana@gmail.com', 'Jana', 'Janic','$2y$12$gigF2nwyhtw3E6XxBTfA2.xN1w0mz8ECCv1/7A15TU.zEGhJx7vZS', '023/848-111', 'jana', 'paypal');
-- admin
insert into registered_user (id, confirmed, email, first_name, last_name, password, phone, username, paypal)
values ( 102, true, 'admin@gmail.com', 'Admin', 'Admin','$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq', '023/848-111', 'admin', 'paypal');


/* ADDS AUTHORITIES TO  USERS */
INSERT INTO user_authority (user_id,authority_id) values(100,1);
INSERT INTO user_authority (user_id,authority_id) values(101,1);
INSERT INTO user_authority (user_id,authority_id) values(102,2);

/* HALL */
insert into hall (id, active, name) values (100, true,'Hall2');

/* SECTION */
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats) values (100, 'east', 100, 100, true, 10000);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats) values (101, 'west', 100, 100, true, 10000);

/*LOCATIONS*/
INSERT INTO location (id, active,  name, city, address, longitude, latitude, image) values (100,true, 'Spens', 'Novi Sad', 'Sutjeska 2', 45.247167, 19.845349, '');
INSERT INTO location (id, name, city, address, longitude, latitude, image) values (101, 'Beogradska arena', 'Beograd', 'Bulevar Arsenija Carnojevica 58', 44.814344, 20.421300, '');

INSERT INTO location_halls (location_id, halls_id) VALUES (100, 100);

INSERT INTO hall_sections (hall_id, sections_id) VALUES (100, 100);
INSERT INTO hall_sections (hall_id, sections_id) VALUES (100, 101);

/* EVENTS */
--normal event
INSERT INTO event (id, controll_access, description, last_day_to_pay, name, sales_date, status, ticket_background, hall_id, location_id)
	values (100, true, 'Novi Sad- omladinska prestonica Evrope.', 3, 'OPENS2019', '2020-03-01 20:00:00', 0, 'background', 100, 100);
--over event
INSERT INTO event (id, controll_access, description, last_day_to_pay, name, sales_date, status, ticket_background, hall_id, location_id)
	values (101, true, '', 3, 'OPENS2019', '2020-03-01 20:00:00', 3, 'background', 100, 100);
--blocked event
INSERT INTO event (id, controll_access, description, last_day_to_pay, name, sales_date, status, ticket_background, hall_id, location_id)
	values (102, true, '', 3, 'Blocked Event', '2020-03-01 20:00:00', 1, 'background', 100, 100);
--sales date has passed
INSERT INTO event (id, controll_access, description, last_day_to_pay, name, sales_date, status, ticket_background, hall_id, location_id)
	values (103, true, 'Taken name and sales date has passed.', 3, 'Taken name', '2019-12-01 20:00:00', 0, 'background', 100, 100);
	
	
insert into event_section (id, seat_columns, is_numerated,name, number_of_seats, price, seat_rows )
	values (100, 10, true, 'Section Name', 100, 2500, 10);
	

insert into event_event_sections (event_id, event_sections_id) values (100,100);
insert into category (id, color, icon, name) values (100, 'red', 'icon', 'Culture');
insert into category (id, color, icon, name) values (101, 'green', 'icon', 'Music');
insert into event_categories (event_id, categories_id) values (100, 100);
insert into event_attachments (event_id, attachments) values (100, 'Attachment');

/*EVENT DATES*/
insert into event_date (event_id, date) values (100, '2020-03-06 20:00:00');
insert into event_date (event_id, date) values (100, '2020-03-07 20:00:00');
insert into event_date (event_id, date) values (100, '2020-03-08 20:00:00');
insert into event_date (event_id, date) values (101, '2020-03-09 20:00:00');


