package com.tickettakeit.tickettakeit.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tickettakeit.tickettakeit.converter.LocationConverter;
import com.tickettakeit.tickettakeit.dto.LocationDTO;
import com.tickettakeit.tickettakeit.exceptions.LocationAlreadyExistsExceptions;
import com.tickettakeit.tickettakeit.exceptions.LocationCreatingException;
import com.tickettakeit.tickettakeit.exceptions.LocationDeleteException;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.LocationUpdateException;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.service.LocationService;

@RestController
public class LocationController {

	@Autowired
	private LocationService locationService;

	// Gets one page of locations
	@GetMapping(value = "/api/locations", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<List<LocationDTO>> getLocations(Pageable pageable) {
		Page<Location> page = locationService.findAll(pageable);
		List<LocationDTO> locationDTOList = page.stream().map(location -> {
			LocationDTO dto = LocationConverter.toDto(location);
			dto.setTotalSize(page.getTotalElements());
			return dto;
		}).collect(Collectors.toList());
		return new ResponseEntity<>(locationDTOList, HttpStatus.OK);
		
	}

	// Gets all location names, used for add new Event.
	@GetMapping(value = "/api/locations/getAllLocationNames", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<List<String>> getAllLocationNames() {
		List<String> locationNameList = locationService.getAllLocationNames();
		return new ResponseEntity<>(locationNameList, HttpStatus.OK);
	}

	// Gets location by name.
	@GetMapping(value = "/api/locations/getLocationByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<LocationDTO> getLocationByName(@PathVariable("name") String name)
			throws LocationNotExistsException {
		LocationDTO location = locationService.findByName(name);

		return new ResponseEntity<LocationDTO>(location, HttpStatus.OK);
	}

	// Gets one location
	@GetMapping(value = "/api/locations/{locationID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<LocationDTO> getLocation(@PathVariable("locationID") Long locationID)
			throws LocationNotExistsException {
		Location location = locationService.getOneLocation(locationID);
		return new ResponseEntity<>(LocationConverter.toDto(location), HttpStatus.OK);
	}

	// Creates Location if there is no id given, else throws exception
	@PostMapping(value = "/api/locations", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<LocationDTO> createLocation(@RequestBody LocationDTO locationdto)
			throws LocationAlreadyExistsExceptions, LocationCreatingException, IOException {
		return new ResponseEntity<>(LocationConverter.toDto(locationService.createLocation(locationdto)),
				HttpStatus.OK);
	}

	// Updates location
	@PutMapping(value = "/api/locations", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<LocationDTO> updateLocation(@RequestBody LocationDTO locationdto)
			throws LocationNotExistsException, LocationUpdateException, FileNotFoundException, IOException {
		return new ResponseEntity<>(LocationConverter.toDto(locationService.updateLocation(locationdto)),
				HttpStatus.OK);
	}

	@DeleteMapping(value = "/api/locations/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<LocationDTO> deleteLocation(@PathVariable(name = "id") Long id)
			throws LocationDeleteException, LocationNotExistsException {
		Location location = locationService.removeLocation(id);
		return new ResponseEntity<>(LocationConverter.toDto(location), HttpStatus.OK);
	}
}
