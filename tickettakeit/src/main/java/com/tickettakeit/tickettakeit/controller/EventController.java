package com.tickettakeit.tickettakeit.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tickettakeit.tickettakeit.converter.EventApplicationConverter;
import com.tickettakeit.tickettakeit.converter.EventConverter;
import com.tickettakeit.tickettakeit.converter.EventSectionConverter;
import com.tickettakeit.tickettakeit.dto.AddNewAttachmentDTO;
import com.tickettakeit.tickettakeit.dto.ChangeEventCategoryDTO;
import com.tickettakeit.tickettakeit.dto.ChangeEventDateDTO;
import com.tickettakeit.tickettakeit.dto.ChangeLastDayToPayDTO;
import com.tickettakeit.tickettakeit.dto.EventApplicationDTO;
import com.tickettakeit.tickettakeit.dto.EventBasicInfoDTO;
import com.tickettakeit.tickettakeit.dto.EventDTO;
import com.tickettakeit.tickettakeit.dto.EventSectionDTO;
import com.tickettakeit.tickettakeit.dto.NewEventWithBackgroundDTO;
import com.tickettakeit.tickettakeit.dto.RemoveAttachmentDTO;
import com.tickettakeit.tickettakeit.dto.UpdateEventDescriptionDTO;
import com.tickettakeit.tickettakeit.dto.UpdateEventNameDTO;
import com.tickettakeit.tickettakeit.exceptions.ApplicationDateNotBegan;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFoundForGivenUser;
import com.tickettakeit.tickettakeit.exceptions.EventDateBeforeSalesDateException;
import com.tickettakeit.tickettakeit.exceptions.EventIsBlockedException;
import com.tickettakeit.tickettakeit.exceptions.EventIsOverException;
import com.tickettakeit.tickettakeit.exceptions.EventNameIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.HallIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.exceptions.LocationIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.NoLoggedUserException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SectionIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserAlreadyHasEventApplication;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventApplication;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.service.CategoryService;
import com.tickettakeit.tickettakeit.service.EventApplicationService;
import com.tickettakeit.tickettakeit.service.EventSectionService;
import com.tickettakeit.tickettakeit.service.EventService;
import com.tickettakeit.tickettakeit.service.FileUploadService;
import com.tickettakeit.tickettakeit.service.HallService;
import com.tickettakeit.tickettakeit.service.LocationService;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/events", produces = MediaType.APPLICATION_JSON_VALUE)
public class EventController {
	@Autowired
	EventService eventService;
	@Autowired
	HallService hallService;
	@Autowired
	CategoryService categoryService;
	@Autowired
	EventSectionService eventSectionService;
	@Autowired
	LocationService locationService;
	@Autowired
	FileUploadService fileUploadService;
	@Autowired
	EventApplicationService eventApplicationService;

	@GetMapping()
	public ResponseEntity<List<EventDTO>> getEvents(Pageable pageable,
			@RequestParam(required = false) String searchString, @RequestParam(defaultValue = "") String eventName,
			@RequestParam(defaultValue = "") String city, @RequestParam(defaultValue = "") String locationName,
			@RequestParam(defaultValue = "0") float fromPrice, @RequestParam(defaultValue = "999999999") float toPrice,
			@RequestParam(required = false) Long fromDateTs, @RequestParam(required = false) Long toDateTs,
			@RequestParam(required = false) List<Long> categoryId) throws InvalidDateException {

		searchString = searchString == null ? null : searchString.trim();
		eventName = eventName.trim();
		city = city.trim();
		locationName = locationName.trim();
		Page<Event> page;

		Date fromDate = fromDateTs == null ? null : new Date(fromDateTs);
		Date toDate = toDateTs == null ? null : new Date(toDateTs);

		List<EventDTO> eventDTOList;
		if (searchString != null) {
			page = eventService.generalSearch(pageable, searchString);

		} else if (eventName.equals("") && city.equals("") && locationName.equals("") && fromPrice == 0
				&& toPrice == 999999999 && fromDate == null && toDate == null && categoryId == null) {
			page = eventService.findAll(pageable);

		} else {
			page = eventService.detailedSearch(pageable, eventName, city, locationName, fromPrice, toPrice, fromDate,
					toDate, categoryId);

		}

		eventDTOList = page.stream().map(event -> {
			EventDTO dto = EventConverter.convertToEventDTO(event);
			dto.setTotalSize(page.getTotalElements());
			return dto;
		}).collect(Collectors.toList());

		return new ResponseEntity<>(eventDTOList, HttpStatus.OK);
	}

	// Gets one page of events
	@GetMapping(value = "/getPage", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<List<EventDTO>> getLocations(Pageable pageable) {
		Page<Event> page = eventService.findAll(pageable);
		List<EventDTO> eventDTOList = page.stream().map(e -> {
			EventDTO dto = EventConverter.convertToEventDTO(e);
			dto.setTotalSize(page.getTotalElements());
			return dto;
		}).collect(Collectors.toList());
		return new ResponseEntity<>(eventDTOList, HttpStatus.OK);

	}

	// Gets event basic info
	@GetMapping(value = "/basicInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<List<EventBasicInfoDTO>> getEventBasicInfo() {

		List<EventBasicInfoDTO> eventDTOList = eventService.findAllBasicInfo();

		return new ResponseEntity<>(eventDTOList, HttpStatus.OK);

	}

	@GetMapping(value = "/{id}/eventDates", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<Set<Date>> getEventDates(@PathVariable("id") Long eventID) {

		Set<Date> eventDates = eventService.findEventDates(eventID);

		return new ResponseEntity<>(eventDates, HttpStatus.OK);

	}

	// Gets Event with given ID from system.
	@GetMapping(value = "/{eventID}")
	public @ResponseBody ResponseEntity<EventDTO> getEvent(@PathVariable("eventID") Long eventID)
			throws EventNotExistsException {

		EventDTO eventDTO = eventService.getEvent(eventID);
		return new ResponseEntity<>(eventDTO, HttpStatus.OK);
	}

	// Gets EventSection with given ID from system.
	@GetMapping(value = "/getEventSection/{eventSectionID}")
	public @ResponseBody ResponseEntity<EventSectionDTO> getEventSection(
			@PathVariable("eventSectionID") Long eventSectionID) throws EventSectionNotExistsException {

		Optional<EventSection> es = eventSectionService.findByIdOptional(eventSectionID);
		if (!es.isPresent()) {
			throw new EventSectionNotExistsException(eventSectionID);
		}

		EventSectionDTO dto = EventSectionConverter.convertToEventDTO(es.get());

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	// Adds new event to the system.
	// Hall, Location, Categories and EventSections must already exist in the
	// system.
	@PostMapping(value = "/add-event", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Long> addEvent(@RequestBody @Valid NewEventWithBackgroundDTO ne)
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		Event newEvent = eventService.addEvent(ne);
		return new ResponseEntity<>(newEvent.getId(), HttpStatus.OK);

	}

	// Changes Event status to blocked, blocked Event is logically deleted.
	// Blocked Event cannot be reversed.
	@PutMapping(value = "/block-event/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> blockEvent(@PathVariable("id") Long id)
			throws EventNotExistsException, SalesDateHasPassedException {

		boolean changedEventStatus = eventService.blockEvent(id);
		return new ResponseEntity<>(changedEventStatus, HttpStatus.OK);

	}

	// Updates Event name attribute.
	@PutMapping(value = "/update-event-name", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> updateEventName(@Valid @RequestBody UpdateEventNameDTO dto)
			throws EventNotExistsException, EventNameIsTakenException, EventIsBlockedException, EventIsOverException {

		boolean updateEventName = eventService.updateEventName(dto);
		if (updateEventName) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Updates Event description attribute
	@PutMapping(value = "/update-event-description", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> updateEventDescription(
			@Valid @RequestBody UpdateEventDescriptionDTO dto)
			throws EventNotExistsException, EventIsBlockedException, EventIsOverException {

		boolean updateEventDescription = eventService.updateEventDescription(dto);
		if (updateEventDescription) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Adds new attachment to Event attachments attribute, and saves it on server.
	@PutMapping(value = "/add-event-attachment", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> addEventAttachment(@Valid @RequestBody AddNewAttachmentDTO dto)
			throws EventNotExistsException, IOException, EventIsBlockedException {

		boolean addEventAttachment = eventService.addNewAttachment(dto);
		if (addEventAttachment) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Removes attachment with given name.
	@PutMapping(value = "/remove-event-attachment", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> removeEventAttachment(@Valid @RequestBody RemoveAttachmentDTO dto)
			throws EventNotExistsException, EventIsBlockedException {

		boolean removeAttachment = eventService.removeAttachment(dto);
		if (removeAttachment) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Changes Event control access. Only available before sales date!
	@PutMapping(value = "/change-event-controll-access/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> changeEventControllAccess(@PathVariable("id") Long id)
			throws EventNotExistsException, SalesDateHasPassedException, EventIsBlockedException, EventIsOverException {

		boolean changeEventControllAccess = eventService.changeEventControlAccess(id);
		if (changeEventControllAccess) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Adds and saves new Ticket background. Only available before sales date!
	@PutMapping(value = "/change-event-ticket-background", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> changeEventTicketBackground(
			@Valid @RequestBody AddNewAttachmentDTO dto)
			throws EventNotExistsException, IOException, SalesDateHasPassedException, EventIsBlockedException {

		boolean changeEventTicketBackground = eventService.changeEventTicketBackground(dto);
		if (changeEventTicketBackground) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Changes Event dates to new ones. Only available before sales date!
	@PutMapping(value = "/change-event-date", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> changeEventDate(@Valid @RequestBody ChangeEventDateDTO dto)
			throws EventNotExistsException, SalesDateHasPassedException, EventIsBlockedException, HallIsTakenException {

		boolean changeEventDate = eventService.changeEventDate(dto);
		if (changeEventDate) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
	}

	// Changes lastDayToPay attribute of Event. Only available before sales date.
	@PutMapping(value = "/change-last-day-to-pay", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> changeLastDayToPay(@Valid @RequestBody ChangeLastDayToPayDTO dto)
			throws EventNotExistsException, SalesDateHasPassedException, EventIsBlockedException {

		boolean changeLastDayToPay = eventService.changeLastDayToPay(dto);
		if (changeLastDayToPay) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Adds new Category (already existing) to Event.
	@PutMapping(value = "/add-new-category-to-event", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> addNewCategoryToEvent(@Valid @RequestBody ChangeEventCategoryDTO dto)
			throws EventNotExistsException, CategoryNotExistsException, EventIsBlockedException, EventIsOverException {

		boolean addNewCategoryToEvent = eventService.addNewCategoryToEvent(dto);
		if (addNewCategoryToEvent) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Removes Category from Event.
	@PutMapping(value = "/remove-category-from-event", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('ADMIN')")
	public @ResponseBody ResponseEntity<Boolean> removeCategoryFromEvent(@Valid @RequestBody ChangeEventCategoryDTO dto)
			throws EventNotExistsException, CategoryNotExistsException, EventIsBlockedException, EventIsOverException {

		boolean removeCategoryFromEvent = eventService.removeCategoryFromEvent(dto);
		if (removeCategoryFromEvent) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);

	}

	// Adds new event application and returns queue number.
	@PutMapping(value = "/add-event-application/{eventId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAuthority('REGISTERED')")
	public @ResponseBody ResponseEntity<Integer> addEventApplication(@PathVariable("eventId") Long eventId)
			throws EventNotExistsException, NoLoggedUserException, EventIsOverException, EventIsBlockedException,
			UserAlreadyHasEventApplication, ApplicationDateNotBegan {

		int queueNumber = eventApplicationService.addEventApplication(eventId);

		return new ResponseEntity<>(queueNumber, HttpStatus.OK);

	}

	// Gets all EventApplicationDTOs for given User.
	@GetMapping(value = "/get-event-application")
	@PreAuthorize("hasAuthority('REGISTERED')")
	public @ResponseBody ResponseEntity<List<EventApplicationDTO>> getEventApplication() throws NoLoggedUserException {

		List<EventApplication> eventApplications = eventApplicationService.findByUser();

		List<EventApplicationDTO> dtos = new ArrayList<>();

		for (EventApplication e : eventApplications) {
			Date[] dateIntervals = eventApplicationService.getDateIntervals(e.getEvent().getSalesDate(),
					e.getQueueNumber());
			dtos.add(EventApplicationConverter.convertToDTO(e, dateIntervals[0], dateIntervals[1]));
		}

		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	// Gets EventApplicationDTO for given Event and User.
	@GetMapping(value = "/get-event-application/{eventId}")
	public @ResponseBody ResponseEntity<EventApplicationDTO> checkEventApplication(
			@PathVariable("eventId") Long eventId)
			throws EventApplicationNotFoundForGivenUser, EventNotExistsException, NoLoggedUserException {

		EventApplication eventApplication = eventApplicationService.findByUserAndEvent(eventId);

		Event event = eventService.findOne(eventId);

		Date[] dateIntervals = eventApplicationService.getDateIntervals(event.getSalesDate(),
				eventApplication.getQueueNumber());

		EventApplicationDTO dto = EventApplicationConverter.convertToDTO(eventApplication, dateIntervals[0],
				dateIntervals[1]);

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

}
