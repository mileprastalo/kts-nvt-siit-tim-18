package com.tickettakeit.tickettakeit.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.paypal.base.rest.PayPalRESTException;
import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.ErrorResponseDTO;
import com.tickettakeit.tickettakeit.exceptions.AccountIsNotActiveException;
import com.tickettakeit.tickettakeit.exceptions.ApplicationDateNotBegan;
import com.tickettakeit.tickettakeit.exceptions.CategoryAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationHasPassed;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFound;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFoundForGivenUser;
import com.tickettakeit.tickettakeit.exceptions.EventDateBeforeSalesDateException;
import com.tickettakeit.tickettakeit.exceptions.EventIsBlockedException;
import com.tickettakeit.tickettakeit.exceptions.EventIsOverException;
import com.tickettakeit.tickettakeit.exceptions.EventNameIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionDoesNotBelongsToTheEventException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.GivenDateDoesntExistException;
import com.tickettakeit.tickettakeit.exceptions.HallAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallCreatingException;
import com.tickettakeit.tickettakeit.exceptions.HallDeleteException;
import com.tickettakeit.tickettakeit.exceptions.HallIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.HallIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallUpdateException;
import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.exceptions.InvalidReportTypeException;
import com.tickettakeit.tickettakeit.exceptions.LocationAlreadyExistsExceptions;
import com.tickettakeit.tickettakeit.exceptions.LocationCreatingException;
import com.tickettakeit.tickettakeit.exceptions.LocationDeleteException;
import com.tickettakeit.tickettakeit.exceptions.LocationIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.LocationUpdateException;
import com.tickettakeit.tickettakeit.exceptions.MissingAttributeException;
import com.tickettakeit.tickettakeit.exceptions.NoTicketsLeftException;
import com.tickettakeit.tickettakeit.exceptions.NotYourTimeYet;
import com.tickettakeit.tickettakeit.exceptions.ObjectCannotBeDeletedException;
import com.tickettakeit.tickettakeit.exceptions.PasswordConfirmationException;
import com.tickettakeit.tickettakeit.exceptions.SaleHasNotYetBeganException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsBeyondBoundariesException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.SectionAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionCreatingException;
import com.tickettakeit.tickettakeit.exceptions.SectionDeleteException;
import com.tickettakeit.tickettakeit.exceptions.SectionIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionUpdateException;
import com.tickettakeit.tickettakeit.exceptions.TicketIsBoughtException;
import com.tickettakeit.tickettakeit.exceptions.TicketNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserAlreadyHasEventApplication;
import com.tickettakeit.tickettakeit.exceptions.UserAlredyExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserDoesNotHaveTheTicketException;
import com.tickettakeit.tickettakeit.exceptions.UserNotExistsException;

@ControllerAdvice
public class ExceptionHandlerController {

	@ExceptionHandler(value = { CategoryNotExistsException.class, LocationNotExistsException.class,
			HallNotExistsException.class, SectionNotExistsException.class, UserNotExistsException.class,
			EventNotExistsException.class, EventSectionNotExistsException.class, GivenDateDoesntExistException.class,
			TicketNotExistsException.class, EventApplicationNotFound.class,
			EventApplicationNotFoundForGivenUser.class })
	public ResponseEntity<ErrorDetailsDTO> handleException(Exception exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO("", exception.getMessage()), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = { CategoryAlreadyExistsException.class, LocationAlreadyExistsExceptions.class,
			HallAlreadyExistsException.class, SectionAlreadyExistsException.class, UserAlredyExistsException.class,
			EventSectionDoesNotBelongsToTheEventException.class, UserDoesNotHaveTheTicketException.class,
			SeatIsBeyondBoundariesException.class, NoTicketsLeftException.class, UserAlreadyHasEventApplication.class,
			LocationIsBlocked.class, HallIsBlocked.class, SectionIsBlocked.class })
	public ResponseEntity<ErrorDetailsDTO> handleException2(Exception exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO("", exception.getMessage()), HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = { HallIsTakenException.class, EventNameIsTakenException.class, SeatIsTakenException.class,
			TicketIsBoughtException.class })
	public ResponseEntity<ErrorDetailsDTO> handleException3(Exception exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO("", exception.getMessage()), HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = { SalesDateHasPassedException.class, EventIsBlockedException.class,
			EventIsOverException.class, EventDateBeforeSalesDateException.class, SaleHasNotYetBeganException.class,
			EventApplicationHasPassed.class, NotYourTimeYet.class, ApplicationDateNotBegan.class })
	public ResponseEntity<ErrorDetailsDTO> handleException4(Exception exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO("", exception.getMessage()), HttpStatus.LOCKED);
	}

	@ExceptionHandler(value = { IOException.class })
	public ResponseEntity<ErrorDetailsDTO> handleException(IOException exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO("", exception.getMessage()), HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(value = { MissingAttributeException.class, PasswordConfirmationException.class,
			AccountIsNotActiveException.class })
	public ResponseEntity<ErrorDetailsDTO> handleException5(Exception exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO("", exception.getMessage()), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = ObjectCannotBeDeletedException.class)
	public ResponseEntity<ErrorDetailsDTO> handleException(ObjectCannotBeDeletedException exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO("", exception.getMessage()), HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = { SectionUpdateException.class, SectionCreatingException.class,
			LocationCreatingException.class, LocationUpdateException.class, HallUpdateException.class,
			HallCreatingException.class, HallDeleteException.class, LocationDeleteException.class,
			SectionDeleteException.class })
	public ResponseEntity<ErrorDetailsDTO> handleExceptionConflict(Exception exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO(exception.getMessage(), exception.getMessage()),
				HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = { InvalidDateException.class, InvalidReportTypeException.class })
	public ResponseEntity<ErrorDetailsDTO> handleInvalidDateException(Exception exception) {
		return new ResponseEntity<>(new ErrorDetailsDTO(exception.getMessage(), exception.getMessage()),
				HttpStatus.BAD_REQUEST);
	}

	// validation exceptions
	@ExceptionHandler(value = BindException.class)
	public ResponseEntity<ErrorResponseDTO> handleException(BindException ex) {

		// get all errors
		List<FieldError> errors = ex.getBindingResult().getFieldErrors();

		List<ErrorDetailsDTO> errorDetails = new ArrayList<>();
		for (FieldError fieldError : errors) {
			ErrorDetailsDTO error = new ErrorDetailsDTO();
			error.setFieldName(fieldError.getField());
			error.setMessage(fieldError.getDefaultMessage());
			errorDetails.add(error);
		}

		ErrorResponseDTO errorResponse = new ErrorResponseDTO();
		errorResponse.setErrors(errorDetails);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = PayPalRESTException.class)
	public ResponseEntity<ErrorDetailsDTO> handlePayPalException(PayPalRESTException exception) {
		String message = exception.getMessage().contains("PAYMENT_ALREADY_DONE")
				? "Payment has been done already for this cart."
				: exception.getMessage();
		return new ResponseEntity<>(new ErrorDetailsDTO("", message), HttpStatus.BAD_REQUEST);
	}

}
