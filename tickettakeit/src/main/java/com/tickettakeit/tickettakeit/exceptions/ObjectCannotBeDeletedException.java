package com.tickettakeit.tickettakeit.exceptions;

public class ObjectCannotBeDeletedException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ObjectCannotBeDeletedException(String message) {
		super(message);
	}
}
