package com.tickettakeit.tickettakeit.exceptions;

public class HallDeleteException extends Exception {
	public HallDeleteException() {
		super("Event exists for this hall");
	}

}
