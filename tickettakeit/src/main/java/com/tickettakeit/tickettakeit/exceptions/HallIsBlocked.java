package com.tickettakeit.tickettakeit.exceptions;

public class HallIsBlocked extends Exception {
	public HallIsBlocked(Long id) {
		super("Hall with ID: " + id + " is blocked.");
	}
}
