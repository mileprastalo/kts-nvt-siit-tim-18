package com.tickettakeit.tickettakeit.exceptions;

public class AccountIsNotActiveException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountIsNotActiveException() {
		super("You have to activate your account via activation email.");
	}
}
