package com.tickettakeit.tickettakeit.exceptions;

public class InvalidReportTypeException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8404815964398290096L;

	public InvalidReportTypeException() {
		super("Invalid report type. Posible types are daily, weekly and monthly.");
	}

}
