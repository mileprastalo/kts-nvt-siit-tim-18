package com.tickettakeit.tickettakeit.exceptions;

public class SectionAlreadyExistsException extends Exception {
	public SectionAlreadyExistsException(String section) {
		super("Section " + section + " already exists");
	}

}
