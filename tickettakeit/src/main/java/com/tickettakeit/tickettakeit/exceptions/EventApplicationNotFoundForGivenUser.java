package com.tickettakeit.tickettakeit.exceptions;

public class EventApplicationNotFoundForGivenUser extends Exception {
	public EventApplicationNotFoundForGivenUser(Long userId, Long eventId) {
		super("No event application found for user with ID: " + userId + " and event ID: " + eventId);
	}
}
