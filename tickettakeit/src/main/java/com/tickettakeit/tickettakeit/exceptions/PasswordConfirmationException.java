package com.tickettakeit.tickettakeit.exceptions;

public class PasswordConfirmationException extends Exception{
	public PasswordConfirmationException() {
		super("You must re-enter the password so that it matches with the original one.");
	}
}
