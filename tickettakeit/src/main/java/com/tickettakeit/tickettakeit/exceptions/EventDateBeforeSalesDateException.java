package com.tickettakeit.tickettakeit.exceptions;

public class EventDateBeforeSalesDateException extends Exception {
	public EventDateBeforeSalesDateException() {
		super("Event date is before sales date.");
	}
}
