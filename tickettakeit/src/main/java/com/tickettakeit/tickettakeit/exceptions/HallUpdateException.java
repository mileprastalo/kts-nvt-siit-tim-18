package com.tickettakeit.tickettakeit.exceptions;

public class HallUpdateException extends Exception {
	public HallUpdateException(String message) {
		super(message);
	}

}
