package com.tickettakeit.tickettakeit.exceptions;

public class CategoryNotExistsException  extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CategoryNotExistsException(Long categoryId) {
		super("Category with the id " + categoryId +" does not exist");
	}

}
