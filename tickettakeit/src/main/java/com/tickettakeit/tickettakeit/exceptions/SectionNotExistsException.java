package com.tickettakeit.tickettakeit.exceptions;

public class SectionNotExistsException extends Exception {
	public SectionNotExistsException(String sectionName) {
		super(sectionName+" does not exist");
	}

}
