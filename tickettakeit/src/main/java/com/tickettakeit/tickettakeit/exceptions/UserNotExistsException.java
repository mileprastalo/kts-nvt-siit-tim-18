package com.tickettakeit.tickettakeit.exceptions;

public class UserNotExistsException extends Exception {
	public UserNotExistsException(String username){
		super("User with username: " + username + " doesn't exists.");
	}
}
