package com.tickettakeit.tickettakeit.exceptions;

public class HallCreatingException extends Exception {
	public HallCreatingException(String message) {
		super(message);
	}

}
