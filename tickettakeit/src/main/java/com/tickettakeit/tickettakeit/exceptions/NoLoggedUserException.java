package com.tickettakeit.tickettakeit.exceptions;

public class NoLoggedUserException extends Exception {
	public NoLoggedUserException() {
		super("No logged user.");
	}
}
