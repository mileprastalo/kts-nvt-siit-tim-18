package com.tickettakeit.tickettakeit.exceptions;

public class LocationNotExistsException extends Exception {
	public LocationNotExistsException(Long id) {
		super("Location with id " + id.toString() + " does not exist");
	}
}
