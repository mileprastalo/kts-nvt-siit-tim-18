package com.tickettakeit.tickettakeit.exceptions;

public class SectionIsBlocked extends Exception {
	public SectionIsBlocked(Long id) {
		super("Section with ID: " + id + " is blocked");
	}
}
