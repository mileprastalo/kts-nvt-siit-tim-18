package com.tickettakeit.tickettakeit.exceptions;

import java.util.Date;

public class ApplicationDateNotBegan extends Exception {
	public ApplicationDateNotBegan(String name, Date date) {
		super("Application date for the Event: " + name + " begins at: " + date);
	}
}
