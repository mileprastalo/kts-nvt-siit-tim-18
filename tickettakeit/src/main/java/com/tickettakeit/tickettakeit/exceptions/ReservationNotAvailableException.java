package com.tickettakeit.tickettakeit.exceptions;

public class ReservationNotAvailableException extends Exception {
	public ReservationNotAvailableException() {
		super("Reservation is not available, last day to pay has passed.");
	}
}
