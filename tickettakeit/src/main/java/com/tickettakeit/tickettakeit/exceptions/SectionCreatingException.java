package com.tickettakeit.tickettakeit.exceptions;

public class SectionCreatingException extends Exception{
	public SectionCreatingException(String message) {
		super(message);
	}

}
