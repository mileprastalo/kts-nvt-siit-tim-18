package com.tickettakeit.tickettakeit.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.tickettakeit.tickettakeit.dto.NewEventDTO;

@Entity
public class Event {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	@Column(name = "description", unique = false, nullable = false)
	private String description;
	@ElementCollection
	private Set<Date> date;
	@Column(name = "salesDate", unique = false, nullable = false)
	private Date salesDate;
	@Column(name = "applicationDate", unique = false, nullable = true)
	private Date applicationDate;
	@Column(name = "status", unique = false, nullable = false)
	private EventStatus status;
	@Column(name = "lastDayToPay", unique = false, nullable = false)
	private int lastDayToPay;
	@Column(name = "controllAccess", unique = false, nullable = false)
	private boolean controllAccess;
	@Column(name = "ticketBackground", unique = false, nullable = true)
	private String ticketBackground;
	@ElementCollection
	private Set<String> attachments;
	@OneToOne
	private Hall hall;
	@ManyToMany
	private Set<Category> categories;
	@OneToMany
	private Set<EventSection> eventSections;
	@OneToOne
	private Location location;

	public Event() {
		super();
	}

	public Event(Long id, String name, String description, Set<Date> date, Date salesDate, Date applicationDate,
			EventStatus status, int lastDayToPay, boolean controllAccess, String ticketBackground,
			Set<String> attachments, Hall hall, Set<Category> categories, Set<EventSection> eventSections,
			Location location) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.date = date;
		this.salesDate = salesDate;
		this.applicationDate = applicationDate;
		this.status = status;
		this.lastDayToPay = lastDayToPay;
		this.controllAccess = controllAccess;
		this.ticketBackground = ticketBackground;
		this.attachments = attachments;
		this.hall = hall;
		this.categories = categories;
		this.eventSections = eventSections;
		this.location = location;
	}

	public Event(NewEventDTO n) {
		this.name = n.getName();
		this.description = n.getDescription();
		this.date = n.getDate().stream().map(s -> new Date(s)).collect(Collectors.toSet());
		this.salesDate = new Date(n.getSalesDate());
		this.status = EventStatus.values()[n.getStatus()];
		this.lastDayToPay = n.getLastDayToPay();
		this.controllAccess = n.isControllAccess();
		this.attachments = n.getAttachments().stream().collect(Collectors.toSet());
	}

	// For Event repository testing.
	public Event(String name, EventStatus status, int lastDayToPay, boolean controllAccess, Hall hall) {
		this.name = name;
		this.description = "Test";
		this.date = new HashSet<Date>();
		this.salesDate = new Date();
		this.status = status;
		this.lastDayToPay = lastDayToPay;
		this.controllAccess = controllAccess;
		this.ticketBackground = "";
		this.attachments = new HashSet<String>();
		this.hall = hall;
		this.categories = new HashSet<Category>();
		this.eventSections = new HashSet<EventSection>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Date> getDate() {
		return date;
	}

	public void setDate(Set<Date> date) {
		this.date = date;
	}

	public Date getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(Date salesDate) {
		this.salesDate = salesDate;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public EventStatus getStatus() {
		return status;
	}

	public void setStatus(EventStatus status) {
		this.status = status;
	}

	public int getLastDayToPay() {
		return lastDayToPay;
	}

	public void setLastDayToPay(int lastDayToPay) {
		this.lastDayToPay = lastDayToPay;
	}

	public boolean isControllAccess() {
		return controllAccess;
	}

	public void setControllAccess(boolean controllAccess) {
		this.controllAccess = controllAccess;
	}

	public Set<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<String> attachments) {
		this.attachments = attachments;
	}

	public Hall getHall() {
		return hall;
	}

	public void setHall(Hall hall) {
		this.hall = hall;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Set<EventSection> getEventSections() {
		return eventSections;
	}

	public void setEventSections(Set<EventSection> eventSections) {
		this.eventSections = eventSections;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getTicketBackground() {
		return ticketBackground;
	}

	public void setTicketBackground(String ticketBackground) {
		this.ticketBackground = ticketBackground;
	}

}
