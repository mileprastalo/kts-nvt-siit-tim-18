package com.tickettakeit.tickettakeit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Category {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "name", unique = true, nullable = false)
	private String name;
	@Column(name = "color", unique = false, nullable = false)
	private String color;
	@Column(name = "icon", unique = false, nullable = false)
	private String icon;

	public Category() {
		super();
	}

	public Category(String name, String color, String icon) {
		super();
		this.name = name;
		this.color = color;
		this.icon = icon;
	}

	// For testing of Event Service.
	public Category(Long id, String name, String color, String icon) {
		this.id = id;
		this.name = name;
		this.color = color;
		this.icon = icon;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
