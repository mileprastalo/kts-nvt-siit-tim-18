package com.tickettakeit.tickettakeit.model;

public enum EventStatus {
	NORMAL, BLOCKED, SOLD, OVER
}
