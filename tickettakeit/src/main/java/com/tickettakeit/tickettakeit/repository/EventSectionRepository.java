package com.tickettakeit.tickettakeit.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tickettakeit.tickettakeit.model.EventSection;

@Repository
public interface EventSectionRepository extends JpaRepository<EventSection, Long> {
	Optional<EventSection> findById(Long id);
}
