package com.tickettakeit.tickettakeit.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tickettakeit.tickettakeit.dto.EventBasicInfoDTO;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.Hall;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

	// find all events basic info
	@Query("SELECT NEW com.tickettakeit.tickettakeit.dto.EventBasicInfoDTO(e.id, e.name) FROM Event AS e")
	List<EventBasicInfoDTO> findAllBasicInfo();

	// find by event name or location or city
	@Query("SELECT DISTINCT e FROM Event e " + "WHERE lower(e.name) like lower(concat('%', :searchString,'%')) "
			+ "OR lower(e.location.city) like lower(concat('%', :searchString, '%'))"
			+ "OR lower(e.location.name) like lower(concat('%', :searchString,'%'))")
	Page<Event> generalSearch(Pageable pageable, String searchString);

	// find by event name, city, location, time interval, price and event categories
	@Query("SELECT DISTINCT e FROM Event e " + "JOIN e.eventSections es " + "JOIN e.categories c " + "JOIN e.date d "
			+ "WHERE es.price between :fromPrice and :toPrice " + "AND c.id in (:categoryIds) "
			+ "AND d between :fromDate and :toDate " + "AND lower(e.name) like lower(concat('%', :eventName,'%')) "
			+ "AND lower(e.location.city) like lower(concat('%', :city, '%')) "
			+ "AND lower(e.location.name) like lower(concat('%', :locationName,'%')) ")
	Page<Event> detailedSearch(Pageable pageable, String eventName, String city, String locationName, float fromPrice,
			float toPrice, Date fromDate, Date toDate, List<Long> categoryIds);

	// find by event name, city, location and price
	@Query("SELECT DISTINCT e FROM Event e " + "JOIN e.eventSections es "
			+ "WHERE es.price between :fromPrice and :toPrice "
			+ "AND lower(e.name) like lower(concat('%', :eventName,'%')) "
			+ "AND lower(e.location.city) like lower(concat('%', :city, '%')) "
			+ "AND lower(e.location.name) like lower(concat('%', :locationName,'%')) ")
	Page<Event> findByNameCityLocationPrice(Pageable pageable, String eventName, String city, String locationName,
			float fromPrice, float toPrice);

	// find by event name, city, location, price and date
	@Query("SELECT DISTINCT e FROM Event e " + "JOIN e.eventSections es " + "JOIN e.date d "
			+ "WHERE es.price between :fromPrice and :toPrice " + "AND d between :fromDate and :toDate "
			+ "AND lower(e.name) like lower(concat('%', :eventName,'%')) "
			+ "AND lower(e.location.city) like lower(concat('%', :city, '%')) "
			+ "AND lower(e.location.name) like lower(concat('%', :locationName,'%')) ")
	Page<Event> findByNameCityLocationPriceDate(Pageable pageable, String eventName, String city, String locationName,
			float fromPrice, float toPrice, Date fromDate, Date toDate);

	// find by event name, city, location, price and event categories
	@Query("SELECT DISTINCT e FROM Event e " + "JOIN e.eventSections es " + "JOIN e.categories c "
			+ "WHERE es.price between :fromPrice and :toPrice " + "AND c.id in (:categoryIds) "
			+ "AND lower(e.name) like lower(concat('%', :eventName,'%')) "
			+ "AND lower(e.location.city) like lower(concat('%', :city, '%')) "
			+ "AND lower(e.location.name) like lower(concat('%', :locationName,'%')) ")
	Page<Event> findByNameCityLocationPriceCategory(Pageable pageable, String eventName, String city,
			String locationName, float fromPrice, float toPrice, List<Long> categoryIds);

	@Query("SELECT DISTINCT e FROM Event e JOIN e.categories c WHERE c.id in (:ids)")
	Page<Event> findByCategory(Pageable pageable, List<Long> ids);

	@Query("SELECT DISTINCT e FROM Event e JOIN e.categories c WHERE c.name = :name")
	Page<Event> findByCategoryName(Pageable pageable, String name);

	@Query("SELECT DISTINCT e FROM Event e JOIN e.date d WHERE d between :from and :to")
	Page<Event> findByDate(Pageable pageable, Date from, Date to);

	@Query("SELECT DISTINCT e FROM Event e JOIN e.eventSections es WHERE es.price between :from and :to")
	Page<Event> findByPrice(Pageable pageable, float from, float to);

	List<Event> findByHall(Hall hall);

	Optional<Event> findById(Long id);
	
	Optional<Event> findByName(String name);
	
	@Query("SELECT DISTINCT event FROM Event event where event.location.id = :locationId")
	public Optional<List<Event>> selectEventsFromLocation(Long locationId);
	
	@Query("SELECT DISTINCT event FROM Event event where event.hall.id = :hallId")
	public Optional<List<Event>> selectEventsFromHall(Long hallId);

}
