package com.tickettakeit.tickettakeit.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tickettakeit.tickettakeit.model.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
	Optional<Location> findById(Long id);
	
	Optional<Location> findByName(String name);

	List<Location> findByNameAndCityAndAddress(String name, String city, String address);

	@Query("SELECT DISTINCT l.name FROM Location l where l.active=true")
	List<String> getAllLocationNames();
}
