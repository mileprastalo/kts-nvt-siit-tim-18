package com.tickettakeit.tickettakeit.converter;

import com.tickettakeit.tickettakeit.dto.EventSectionDTO;
import com.tickettakeit.tickettakeit.model.EventSection;

public class EventSectionConverter {
	// Converts Event to EventDTO
	public static EventSectionDTO convertToEventDTO(EventSection e) {
		EventSectionDTO dto = new EventSectionDTO();

		dto.setId(e.getId());
		dto.setName(e.getName());
		dto.setRows(e.getRows());
		dto.setColumns(e.getColumns());
		dto.setNumerated(e.isNumerated());
		dto.setNumberOfSeats(e.getNumberOfSeats());
		dto.setPrice(e.getPrice());
		dto.setLeft(e.getLeft());
		dto.setTop(e.getTop());
		return dto;
	}
	
	private EventSectionConverter() {
		
	}
}
