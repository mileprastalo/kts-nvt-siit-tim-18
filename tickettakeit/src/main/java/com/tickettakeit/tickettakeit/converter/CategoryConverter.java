package com.tickettakeit.tickettakeit.converter;

import com.tickettakeit.tickettakeit.dto.CategoryDTO;
import com.tickettakeit.tickettakeit.model.Category;

public class CategoryConverter {

	// Converts CategoryDTO to Category.
	public static Category convertFromDTO(CategoryDTO dto) {
		Category category = new Category();

		category.setId(dto.getId());
		category.setName(dto.getName());
		category.setColor(dto.getColor());
		category.setIcon(dto.getIcon());

		return category;

	}

	// Converts Category to CategoryDTO
	public static CategoryDTO convertToCategoryDTO(Category category) {
		CategoryDTO dto = new CategoryDTO();

		dto.setId(category.getId());
		dto.setName(category.getName());
		dto.setColor(category.getColor());
		dto.setIcon(category.getIcon());
		return dto;

	}
	private CategoryConverter() {
		
	}

}
