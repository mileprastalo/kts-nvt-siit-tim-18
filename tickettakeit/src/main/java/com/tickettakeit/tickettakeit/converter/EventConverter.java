package com.tickettakeit.tickettakeit.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import com.tickettakeit.tickettakeit.dto.CategoryDTO;
import com.tickettakeit.tickettakeit.dto.EventDTO;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;

public class EventConverter {

	// Converts EventDTO to Event.
	public static Event convertFromDTO(EventDTO dto, Hall hall, Set<Category> categories,
			Set<EventSection> eventSections, Location location) {
		Event e = new Event();

		e.setId(dto.getId());
		e.setName(dto.getName());
		e.setDescription(dto.getDescription());

		for (Date d : dto.getDate()) {
			e.getDate().add(d);
		}

		e.setSalesDate(new Date(dto.getSalesDate()));
		e.setApplicationDate(new Date(dto.getApplicationDate()));
		e.setStatus(EventStatus.values()[dto.getStatus()]);
		e.setLastDayToPay(dto.getLastDayToPay());
		e.setControllAccess(dto.isControllAccess());
		e.setAttachments(dto.getAttachments().stream().collect(Collectors.toSet()));
		e.setHall(hall);
		e.setCategories(categories);
		e.setEventSections(eventSections);
		e.setLocation(location);

		return e;

	}

	// Converts Event to EventDTO
	public static EventDTO convertToEventDTO(Event e) {
		EventDTO dto = new EventDTO();

		dto.setId(e.getId());
		dto.setName(e.getName());
		dto.setDescription(e.getDescription());
		dto.setDate(new ArrayList<>());
		for (Date d : e.getDate()) {
			dto.getDate().add(d);
		}
		dto.setSalesDate(e.getSalesDate().getTime());
		if (e.getApplicationDate() == null) {
			dto.setApplicationDate(null);
		} else {
			dto.setApplicationDate(e.getApplicationDate().getTime());
		}

		dto.setStatus(e.getStatus().ordinal());
		dto.setLastDayToPay(e.getLastDayToPay());
		dto.setControllAccess(e.isControllAccess());
		dto.setAttachments((ArrayList<String>) e.getAttachments().stream().collect(Collectors.toList()));
		dto.setHallID(e.getHall().getId());
		dto.setCategories((ArrayList<CategoryDTO>) e.getCategories().stream()
				.map(c -> CategoryConverter.convertToCategoryDTO(c)).collect(Collectors.toList()));
		dto.setEventSectionsIDs(
				(ArrayList<Long>) e.getEventSections().stream().map(es -> es.getId()).collect(Collectors.toList()));
		dto.setLocationID(e.getLocation().getId());
		dto.setLocationName(e.getLocation().getName());
		dto.setHallName(e.getHall().getName());
		dto.setBackground(e.getTicketBackground());

		return dto;
	}
	private EventConverter() {
		
	}
}
