package com.tickettakeit.tickettakeit.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.tickettakeit.tickettakeit.converter.TicketConverter;
import com.tickettakeit.tickettakeit.dto.BuyTicketDTO;
import com.tickettakeit.tickettakeit.dto.PayPalDTO;
import com.tickettakeit.tickettakeit.dto.SeatDTO;
import com.tickettakeit.tickettakeit.dto.TicketDTO;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationHasPassed;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFoundForGivenUser;
import com.tickettakeit.tickettakeit.exceptions.EventIsBlockedException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionDoesNotBelongsToTheEventException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.GivenDateDoesntExistException;
import com.tickettakeit.tickettakeit.exceptions.NoLoggedUserException;
import com.tickettakeit.tickettakeit.exceptions.NoTicketsLeftException;
import com.tickettakeit.tickettakeit.exceptions.NotYourTimeYet;
import com.tickettakeit.tickettakeit.exceptions.ReservationNotAvailableException;
import com.tickettakeit.tickettakeit.exceptions.SaleHasNotYetBeganException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsBeyondBoundariesException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.TicketIsBoughtException;
import com.tickettakeit.tickettakeit.exceptions.TicketNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserDoesNotHaveTheTicketException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventApplication;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.RegisteredUser;
import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.model.User;
import com.tickettakeit.tickettakeit.repository.TicketRepository;

import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

@Service
public class TicketService {
	@Autowired
	TicketRepository repository;
	@Autowired
	EventSectionService eventSectionService;
	@Autowired
	EventService eventService;
	@Autowired
	UserService userService;
	@Autowired
	EmailService emailService;
	@Autowired
	PayPalService paypalService;
	@Autowired
	EventApplicationService eventApplicationService;

	public Ticket findByID(Long id) {
		return repository.getOne(id);
	}

	public Ticket save(Ticket ticket) {
		return repository.save(ticket);
	}

	public Ticket findTicketByRowColumnEventSectionID(Long id, Integer row, Integer column) {
		return repository.findTicketByRowColumnEventSectionID(id, row, column);
	}

	public Page<Ticket> findTicketsByUser(Pageable pageable, Long userId, boolean bought) {
		return repository.findTicketsByUser(pageable, userId, bought);
	}

	public int countSoldTickets(Long id) {
		return repository.countSoldTickets(id);
	}

	public void cancelTicket(Long id) {
		repository.deleteById(id);
	}

	public List<SeatDTO> getTakenSeats(Long eventId, Long sectionId, String date) {
		List<SeatDTO> list = repository.getTakenSeats(eventId, sectionId);
		List<SeatDTO> filtered = new ArrayList<>();
		for (SeatDTO seatDTO : list) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String d = sdf.format(seatDTO.getTickedDate());
			if (d.equals(date)) {
				filtered.add(seatDTO);
			}
		}
		return filtered;
	}

	public TicketDTO getTicket(Long ticketID) throws TicketNotExistsException {
		Optional<Ticket> foundTicket = repository.findById(ticketID);

		if (!foundTicket.isPresent()) {
			throw new TicketNotExistsException(ticketID);
		}

		return TicketConverter.convertToDTO(foundTicket.get());
	}

	public String generateTicket(Ticket ticket) throws DocumentException, MalformedURLException, IOException {

		String directoryPath = "src/main/resources/static/tickets/";

		Document document = new Document();
		Rectangle rectangle = new Rectangle(600, 150);
		document.setPageSize(rectangle);

		PdfWriter.getInstance(document, new FileOutputStream(directoryPath + "ticket_" + ticket.getId() + ".pdf"));

		document.open();

		// Set background
		String imgName = ticket.getEvent().getTicketBackground();
		try {
			Image backgroundImg = Image.getInstance("src/main/resources/static/ticket_backgrounds/" + imgName);
			// Scale to new height and new width of image
			backgroundImg.setAbsolutePosition(0, 0);
			backgroundImg.scaleAbsolute(600, 150);
			document.add(backgroundImg);
		} catch (Exception e) {
		}

		// Qenerate QR code
		QRCode.from(ticket.getCode()).to(ImageType.JPG)
				.writeTo(new FileOutputStream(directoryPath + "qr_" + ticket.getId() + ".jpg"));

		Image qrImg = Image.getInstance(directoryPath + "qr_" + ticket.getId() + ".jpg");
		qrImg.scaleAbsolute(80, 80);
		qrImg.setAbsolutePosition(510, 10);
		document.add(qrImg);

		document.close();

		PdfReader pdfReader = new PdfReader(directoryPath + "ticket_" + ticket.getId() + ".pdf");
		String ticketPath = directoryPath + "ticketQr_" + ticket.getId() + ".pdf";
		PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(ticketPath));

		// Font eventNameFont = FontFactory.getFont(FontFactory.COURIER, 24,
		// BaseColor.YELLOW);
		PdfContentByte canvas = pdfStamper.getOverContent(1);
		Phrase eventNamePhrase = new Phrase(ticket.getEvent().getName());
		// eventNamePhrase.setFont(eventNameFont); // Why this doesn't work?

		SimpleDateFormat sdf = new SimpleDateFormat("dd. MM. yyyy. hh:mm");

		// Add text
		ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, eventNamePhrase, 30, 130, 0);
		ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase("Date: " + sdf.format(ticket.getForDay())),
				30, 110, 0);
		ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT,
				new Phrase("Location: " + ticket.getEvent().getLocation().getName() + ", "
						+ ticket.getEvent().getHall().getName() + ", " + ticket.getEventSection().getName()),
				30, 90, 0);
		ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase("Row: " + ticket.getRow()), 30, 70, 0);
		ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase("Column: " + ticket.getColumn()), 30, 50, 0);
		ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase("Ticket price: " + ticket.getPrice()), 30, 30,
				0);
		canvas.addImage(qrImg);
		pdfStamper.close();
		pdfReader.close();

		return ticketPath;

	}

	public void checkControllAccess(RegisteredUser loggedUser, Event foundEvent)
			throws EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Checks if User has EventApplication.
		EventApplication eventApplication = eventApplicationService.getByUserAndEvent(loggedUser, foundEvent);

		Date[] dateIntervals = eventApplicationService.getDateIntervals(foundEvent.getSalesDate(),
				eventApplication.getQueueNumber());

		if (new Date().before(dateIntervals[0])) {
			throw new NotYourTimeYet(dateIntervals[0], dateIntervals[1]);
		} else if (new Date().after(dateIntervals[1])) {
			throw new EventApplicationHasPassed(dateIntervals[0], dateIntervals[1]);
		}
	}

	public Ticket buyTicket(BuyTicketDTO bt) throws NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException,
			EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Checks if logged User exists.
		RegisteredUser loggedUser = getLoggedUser();
		if (loggedUser == null) {
			throw new NoLoggedUserException();
		}

		// Checks if Event with given ID exists.
		Optional<Event> foundEvent = eventService.findByID(bt.getEventID());
		if (!foundEvent.isPresent()) {
			throw new EventNotExistsException(bt.getEventID());
		}

		// Checks for control access.
		if (foundEvent.get().isControllAccess()) {
			checkControllAccess(loggedUser, foundEvent.get());
		}

		// Checks if EventSection with given ID exists.
		Optional<EventSection> foundES = eventSectionService.findByIdOptional(bt.getEventSectionID());
		if (!foundES.isPresent()) {
			throw new EventSectionNotExistsException(bt.getEventSectionID());
		}

		// Checks if EventSection belongs to the Event.
		if (!foundEvent.get().getEventSections().contains(foundES.get())) {
			throw new EventSectionDoesNotBelongsToTheEventException(foundES.get().getId(), foundEvent.get().getId());
		}

		// Checks if Event status is BLOCKED
		if (foundEvent.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(foundEvent.get().getId());
		}

		// Checks if given day belongs to Event dates.
		if (!foundEvent.get().getDate().contains(new Date(bt.getForDay()))) {
			throw new GivenDateDoesntExistException(new Date(bt.getForDay()), foundEvent.get().getId());
		}

		// Checks if sale has began.
		if (foundEvent.get().getSalesDate().after(new Date())) {
			throw new SaleHasNotYetBeganException();
		}

		// Checks if sale has passed.
		if (new Date().after(new Date(bt.getForDay() - foundEvent.get().getLastDayToPay() * (1000 * 60 * 60 * 24)))) {
			throw new SalesDateHasPassedException();
		}

		// Creates new Ticket, fills its info and saves it in the system.
		Ticket newTicket = new Ticket();
		// Checks if seats are enumerated.
		if (foundES.get().isNumerated()) {
			// Checks if the seat is taken.
			if (repository.findTicketByDateRowColumnEventSectionID(new Date(bt.getForDay()), bt.getEventSectionID(),
					bt.getRow(), bt.getColumn()) != null) {
				throw new SeatIsTakenException(bt.getRow(), bt.getColumn());
			}
			// Checks if seat is beyond boundaries.
			if (bt.getRow() > foundES.get().getRows() || bt.getColumn() > foundES.get().getColumns()) {
				throw new SeatIsBeyondBoundariesException(bt.getRow(), bt.getColumn());
			}
			newTicket.setRow(bt.getRow());
			newTicket.setColumn(bt.getColumn());
		} else {
			// Checks if there are free seats.
			if (countSoldTickets(bt.getEventSectionID()) == foundES.get().getNumberOfSeats()) {
				throw new NoTicketsLeftException();
			}
			newTicket.setRow(0);
			newTicket.setColumn(0);
		}

		newTicket.setDayOfPurchase(new Date());
		newTicket.setBought(true);
		newTicket.setPrice(foundES.get().getPrice());
		newTicket.setForDay(new Date(bt.getForDay()));
		newTicket.setEventSection(foundES.get());
		newTicket.setEvent(foundEvent.get());
		// Barcode will be auto generated.
		String barcode = loggedUser.getId() + " " + foundEvent.get().getId() + " "
				+ new Date(newTicket.getDayOfPurchase().getTime());
		newTicket.setCode(barcode);

		// Sets logged User.
		newTicket.setUser(loggedUser);
		loggedUser.getTickets().add(newTicket);

		// Saves new Ticket and user.
		Ticket boughtTicket = repository.save(newTicket);
		userService.save(loggedUser);

		return boughtTicket;
	}

	// creates reservations and paypal transaction for them
	public Map<String, Object> buyTicketsCreatePayment(List<BuyTicketDTO> buyTicketDtos)
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, PayPalRESTException,
			NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {

		List<Ticket> reservations = new ArrayList<>();

		// create reservations
		for (BuyTicketDTO dto : buyTicketDtos) {
			Ticket t = reserveTicket(dto);
			reservations.add(t);
		}

		// create paypal transaction
		Payment payment = paypalService.createPaymentObjectForTickets(reservations,
				"http://localhost:4200/user-profile?cancelIfFail=true",
				"http://localhost:4200/user-profile?paymentCanceled=true&deleteReservations=true");
		Map<String, Object> response = paypalService.createPayPalPayment(payment);

		List<Long> ids = reservations.stream().map(Ticket::getId).collect(Collectors.toList());
		response.put("reservationIds", ids);

		return response;

	}

	// User reserves Ticket.
	public Ticket reserveTicket(BuyTicketDTO bt) throws NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException,
			EventIsBlockedException, SalesDateHasPassedException, GivenDateDoesntExistException,
			SaleHasNotYetBeganException, EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException,
			NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {

		// Checks if logged User exists.
		RegisteredUser loggedUser = getLoggedUser();
		if (loggedUser == null) {
			throw new NoLoggedUserException();
		}

		// Checks if Event with given ID exists.
		Optional<Event> foundEvent = eventService.findByID(bt.getEventID());
		if (!foundEvent.isPresent()) {
			throw new EventNotExistsException(bt.getEventID());
		}

		// Checks for control access.
		if (foundEvent.get().isControllAccess()) {
			checkControllAccess(loggedUser, foundEvent.get());
		}
		// Checks if EventSection with given ID exists.
		Optional<EventSection> foundES = eventSectionService.findByIdOptional(bt.getEventSectionID());
		if (!foundES.isPresent()) {
			throw new EventSectionNotExistsException(bt.getEventSectionID());
		}

		// Checks if EventSection belongs to the Event.
		if (!foundEvent.get().getEventSections().contains(foundES.get())) {
			throw new EventSectionDoesNotBelongsToTheEventException(foundES.get().getId(), foundEvent.get().getId());
		}

		// Checks if Event status is BLOCKED
		if (foundEvent.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(foundEvent.get().getId());
		}

		// Checks if given day belongs to Event dates.
		if (!foundEvent.get().getDate().contains(new Date(bt.getForDay()))) {
			throw new GivenDateDoesntExistException(new Date(bt.getForDay()), foundEvent.get().getId());
		}

		// Checks if sale has began.
		if (foundEvent.get().getSalesDate().after(new Date())) {
			throw new SaleHasNotYetBeganException();
		}

		// Checks if sale has passed.
		if (new Date().after(new Date(bt.getForDay() - foundEvent.get().getLastDayToPay() * (1000 * 60 * 60 * 24)))) {
			throw new SalesDateHasPassedException();
		}

		// Creates new Ticket, fills its info and saves it in the system.
		Ticket newTicket = new Ticket();
		if (foundES.get().isNumerated()) {
			// Checks if the seat is taken.
			if (repository.findTicketByDateRowColumnEventSectionID(new Date(bt.getForDay()), bt.getEventSectionID(),
					bt.getRow(), bt.getColumn()) != null) {
				throw new SeatIsTakenException(bt.getRow(), bt.getColumn());
			}
			// Checks if seat is beyond boundaries.
			if (bt.getRow() > foundES.get().getRows() || bt.getColumn() > foundES.get().getColumns()) {
				throw new SeatIsBeyondBoundariesException(bt.getRow(), bt.getColumn());
			}
			newTicket.setRow(bt.getRow());
			newTicket.setColumn(bt.getColumn());
		} else {
			// Checks if there are free seats.
			if (countSoldTickets(bt.getEventSectionID()) == foundES.get().getNumberOfSeats()) {
				throw new NoTicketsLeftException();
			}
			newTicket.setRow(0);
			newTicket.setColumn(0);
		}

		// This is considered as the day of reservation.
		newTicket.setDayOfPurchase(new Date());
		newTicket.setBought(false);
		newTicket.setPrice(foundES.get().getPrice());
		newTicket.setForDay(new Date(bt.getForDay()));
		newTicket.setEventSection(foundES.get());
		newTicket.setEvent(foundEvent.get());
		// Barcode is auto generated.
		String barcode = loggedUser.getId() + " " + foundEvent.get().getId() + " "
				+ new Date(newTicket.getDayOfPurchase().getTime());
		newTicket.setCode(barcode);

		newTicket.setUser(loggedUser);
		loggedUser.getTickets().add(newTicket);

		Ticket reservedTicket = repository.save(newTicket);
		userService.save(loggedUser);

		return reservedTicket;
	}

	// ****************************** BUY RESERVED *****************************

	// User buys Ticket he had previously reserved -> paypal create payment
	public Map<String, Object> buyReservedTicketCreatePayment(List<Long> ticketIds) throws NoLoggedUserException,
			TicketNotExistsException, EventIsBlockedException, SalesDateHasPassedException, SaleHasNotYetBeganException,
			UserDoesNotHaveTheTicketException, PayPalRESTException {
		// Checks if logged User exists.
		User loggedUser = getLoggedUser();
		if (loggedUser == null) {
			throw new NoLoggedUserException();
		}

		// get and validate reserved tickets
		List<Ticket> ticketsList = new ArrayList<Ticket>();
		for (Long ticketId : ticketIds) {
			Ticket ticket = validateReservedTicket(ticketId);
			ticketsList.add(ticket);
		}

		Payment payment = paypalService.createPaymentObjectForTickets(ticketsList,
				"http://localhost:4200/user-profile?cancelIfFail=false",
				"http://localhost:4200/user-profile?paymentCanceled=true&deleteReservations=false");

		return paypalService.createPayPalPayment(payment);
	}

	// User buys Ticket he had previously reserved -> paypal execute payment
	public Map<String, Object> buyReservedTicketCompletePayment(PayPalDTO paypalDto) throws NoLoggedUserException,
			TicketNotExistsException, PayPalRESTException, EventIsBlockedException, SaleHasNotYetBeganException,
			SalesDateHasPassedException, UserDoesNotHaveTheTicketException, DocumentException, IOException {

		// Checks if logged User exists.
		User loggedUser = getLoggedUser();
		if (loggedUser == null) {
			throw new NoLoggedUserException();
		}

		// get and validate reserved tickets
		List<Ticket> tickets = new ArrayList<Ticket>();
		for (Long ticketId : paypalDto.getReservationIds()) {
			Ticket ticket = validateReservedTicket(ticketId);
			tickets.add(ticket);
		}

		// paypal complete payment
		Map<String, Object> response = paypalService.completePayment(paypalDto);

		// save tickets
		for (Ticket ticket : tickets) {
			ticket.setBought(true);
			ticket.setDayOfPurchase(new Date());
			repository.save(ticket);
		}

		// generate tickets and send email
		List<String> ticketPaths = new ArrayList<>();
		for (Ticket ticket : tickets) {
			String path = generateTicket(ticket);
			ticketPaths.add(path);
		}

		emailService.sendTicket(tickets.get(0), ticketPaths);

		return response;

	}

	// Cancels reserved ticket.
	// User can cancel ticket whenever he likes.
	// Ticket is auto canceled if User has not bought it before last day to pay.
	public boolean cancelTicketReservation(Long ticketID)
			throws NoLoggedUserException, TicketNotExistsException, TicketIsBoughtException {
		// Checks if logged User exists.
		User loggedUser = getLoggedUser();
		if (loggedUser == null) {
			throw new NoLoggedUserException();
		}

		// Checks if Ticket with given ID exists.
		Optional<Ticket> foundTicket = repository.findById(ticketID);
		if (!foundTicket.isPresent()) {
			throw new TicketNotExistsException(ticketID);
		}
		// Can't cancel bought Ticket.
		if (foundTicket.get().isBought()) {
			throw new TicketIsBoughtException();
		}

		loggedUser.getTickets().remove(foundTicket.get());

		cancelTicket(ticketID);
		userService.save(loggedUser);

		return true;
	}

	// Get logged User.
	protected RegisteredUser getLoggedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String username = authentication.getName();
			return (RegisteredUser) userService.findOneByUsername(username).get();
		}
		return null;
	}

	// ****************************** VALIDATIONS *****************************

	public Ticket validateReservedTicket(Long ticketID) throws TicketNotExistsException, EventIsBlockedException,
			SaleHasNotYetBeganException, SalesDateHasPassedException, UserDoesNotHaveTheTicketException {

		// Checks if Ticket with given ID exists.
		Optional<Ticket> foundTicket = repository.findById(ticketID);
		if (!foundTicket.isPresent()) {
			throw new TicketNotExistsException(ticketID);
		}

		// Checks if Event status is BLOCKED
		if (foundTicket.get().getEvent().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(foundTicket.get().getEvent().getId());
		}

		// Checks if sale has began.
		if (foundTicket.get().getEvent().getSalesDate().after(new Date())) {
			throw new SaleHasNotYetBeganException();
		}

		// Checks if sale has passed.
		if (new Date().after(new Date(foundTicket.get().getForDay().getTime()
				- foundTicket.get().getEvent().getLastDayToPay() * (1000 * 60 * 60 * 24)))) {
			throw new SalesDateHasPassedException();
		}

		// Checks if Logged User has the Ticket
		if (!getLoggedUser().getTickets().contains(foundTicket.get())) {
			throw new UserDoesNotHaveTheTicketException(getLoggedUser().getId(), foundTicket.get().getId());
		}

		return foundTicket.get();

	}
}
