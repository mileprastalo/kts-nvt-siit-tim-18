package com.tickettakeit.tickettakeit.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.itextpdf.text.pdf.codec.Base64;
import com.tickettakeit.tickettakeit.converter.LocationConverter;
import com.tickettakeit.tickettakeit.dto.LocationDTO;
import com.tickettakeit.tickettakeit.exceptions.LocationAlreadyExistsExceptions;
import com.tickettakeit.tickettakeit.exceptions.LocationCreatingException;
import com.tickettakeit.tickettakeit.exceptions.LocationDeleteException;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.LocationUpdateException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.repository.LocationRepository;

@Service
public class LocationService {

	@Autowired
	private LocationRepository repository;

	@Autowired
	private EventService eventService;

	public Optional<Location> findOne(Long id) {
		return repository.findById(id);
	}

	public LocationDTO findByName(String name) throws LocationNotExistsException {
		Optional<Location> location = repository.findByName(name);
		if (!location.isPresent()) {
			throw new LocationNotExistsException(0L);
		}
		return LocationConverter.toDto(location.get());
	}

	public Optional<Location> findById(Long id) {
		return repository.findById(id);
	}

	public List<Location> findAll() {
		return repository.findAll();
	}

	public Page<Location> findAll(Pageable page) {
		return repository.findAll(page);
	}

	public List<String> getAllLocationNames() {
		return repository.getAllLocationNames();
	}

	public Location save(Location location) {
		return repository.save(location);
	}

	public List<Location> saveAll(List<Location> location) {
		return repository.saveAll(location);
	}

	public void remove(Long id) {
		repository.deleteById(id);
	}

	public Location createLocation(LocationDTO locationdto)
			throws LocationAlreadyExistsExceptions, LocationCreatingException, IOException {
		if (repository
				.findByNameAndCityAndAddress(locationdto.getName(), locationdto.getCity(), locationdto.getAddress())
				.size() > 0) {
			throw new LocationAlreadyExistsExceptions(locationdto.getName());
		}


		Location location = null;
		locationdto.setActive(true);
		if (locationdto.getImage() != null) {
			byte[] imageByte = Base64.decode((locationdto.getImage().split(","))[1]);
			String directory = "/images";
			File f = new File(directory);
			f.mkdirs();
			try (FileOutputStream fstr = new FileOutputStream(directory + "/" + locationdto.getName() + ".jpg")) {
				fstr.write(imageByte);
			}
			locationdto.setImage("/images/" + locationdto.getName() + ".jpg");

		}
		try {
			location = save(LocationConverter.fromDto(locationdto));
		} catch (Exception e) {
			throw new LocationCreatingException(locationdto.getName());
		}
		return location;
	}

	public Location updateLocation(LocationDTO locationdto)
			throws LocationNotExistsException, LocationUpdateException, IOException {
		Location l = findOne(locationdto.getId())
				.orElseThrow(() -> new LocationNotExistsException(locationdto.getId()));
		if ((locationdto.getImage() != null && (!locationdto.getImage().equals("")))
				&& (!locationdto.getImage().equals(l.getImage()))) {
			byte[] imageByte = Base64.decode((locationdto.getImage().split(","))[1]);
			String directory = "/images";
			File f = new File(directory);
			f.mkdirs();
			try (FileOutputStream fstr = new FileOutputStream(directory + "/" + locationdto.getName() + ".jpg")) {
				fstr.write(imageByte);
			}
			locationdto.setImage("/images/" + locationdto.getName() + ".jpg");

		}
		Location loc = LocationConverter.fromDto(locationdto);

		try {
			loc = save(loc);
		} catch (Exception e) {
			throw new LocationUpdateException(locationdto.getId().toString());
		}
		return loc;
	}

	public Location getOneLocation(Long id) throws LocationNotExistsException {
		return findOne(id).orElseThrow(() -> new LocationNotExistsException(id));
	}

	public Location removeLocation(Long id) throws LocationDeleteException, LocationNotExistsException {
		List<Event> events = eventService.selectEventsFromLocation(id).orElseGet(() -> null);
		if (events != null) {
			Date today = new Date();
			for (Event event : events) {
				Set<Date> dates = event.getDate();
				for (Date d : dates) {
					if (d.after(today)) {
						throw new LocationDeleteException();
					}
				}
			}
		}
		Location location = findOne(id).orElseThrow(() -> new LocationNotExistsException(id));
		location.setActive(false);
		return save(location);
	}
}
