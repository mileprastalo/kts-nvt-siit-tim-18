package com.tickettakeit.tickettakeit.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.tickettakeit.tickettakeit.exceptions.ApplicationDateNotBegan;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFound;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFoundForGivenUser;
import com.tickettakeit.tickettakeit.exceptions.EventIsBlockedException;
import com.tickettakeit.tickettakeit.exceptions.EventIsOverException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.NoLoggedUserException;
import com.tickettakeit.tickettakeit.exceptions.UserAlreadyHasEventApplication;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventApplication;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.RegisteredUser;
import com.tickettakeit.tickettakeit.repository.EventApplicationRepository;

@Service
public class EventApplicationService {

	@Autowired
	EventApplicationRepository repository;
	@Autowired
	EventService eventService;
	@Autowired
	UserService userService;

	public EventApplication findByID(Long id) throws EventApplicationNotFound {

		Optional<EventApplication> eventApplication = repository.findById(id);
		if (!eventApplication.isPresent()) {
			throw new EventApplicationNotFound(id);
		}

		return eventApplication.get();
	}

	public EventApplication findByUserAndEvent(Long eventId)
			throws EventApplicationNotFoundForGivenUser, EventNotExistsException, NoLoggedUserException {
		RegisteredUser loggedUser = getLoggedUser();
		Optional<Event> event = eventService.findByID(eventId);
		if (!event.isPresent()) {
			throw new EventNotExistsException(eventId);
		}
		Optional<EventApplication> eventApplication = repository.findByUserAndEvent(loggedUser, event.get());
		if (!eventApplication.isPresent()) {
			throw new EventApplicationNotFoundForGivenUser(loggedUser.getId(), event.get().getId());
		}

		return eventApplication.get();
	}

	public EventApplication getByUserAndEvent(RegisteredUser user, Event event)
			throws EventApplicationNotFoundForGivenUser {
		Optional<EventApplication> eventApplication = repository.findByUserAndEvent(user, event);
		if (!eventApplication.isPresent()) {
			throw new EventApplicationNotFoundForGivenUser(user.getId(), event.getId());
		}

		return eventApplication.get();
	}

	public EventApplication save(EventApplication eventApplication) {
		return repository.save(eventApplication);
	}

	public List<EventApplication> findByUser() throws NoLoggedUserException {
		RegisteredUser loggedUser = getLoggedUser();

		return repository.findByUser(loggedUser);
	}

	public int addEventApplication(Long eventId) throws EventNotExistsException, NoLoggedUserException,
			EventIsOverException, EventIsBlockedException, UserAlreadyHasEventApplication, ApplicationDateNotBegan {
		Optional<Event> event = eventService.findByID(eventId);
		if (!event.isPresent()) {
			throw new EventNotExistsException(eventId);
		}

		RegisteredUser user = getLoggedUser();

		// Checks if User already has EventApplication.
		Optional<EventApplication> foundEventApplication = repository.findByUserAndEvent(user, event.get());
		if (foundEventApplication.isPresent()) {
			throw new UserAlreadyHasEventApplication(eventId);
		}

		// Checks application date.
		if (new Date().before(event.get().getApplicationDate())) {
			throw new ApplicationDateNotBegan(event.get().getName(), event.get().getApplicationDate());
		}

		// Checks if Event is OVER.
		if (event.get().getStatus().equals(EventStatus.OVER)) {
			throw new EventIsOverException(eventId);
		}

		// Checks if Event is BLOCKED.
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(eventId);
		}

		int currentCount = repository.countByEvent(event.get());

		EventApplication eventApplication = new EventApplication(user, event.get(), currentCount + 1);

		eventApplication = repository.save(eventApplication);

		return eventApplication.getQueueNumber();
	}

	// Returns date intervals for control access [0] downInterval, [1] upInterval.
	public Date[] getDateIntervals(Date salesDate, int queueNumber) {
		// How much time User has to buy Ticket.
		int minutesForPurchase = 5;
		// How many Users can buy Ticket in the same interval.
		int numberOfUsers = 2;

		int downInterval;
		int upInterval;

		// Rounds number down.
		downInterval = (queueNumber / numberOfUsers) * minutesForPurchase;
		// Adds interval up.
		upInterval = downInterval + minutesForPurchase;

		// Adds time till start of the event application queue interval.
		Date downDate = new Date(salesDate.getTime() + (1000 * 60 * downInterval));
		// Adds time till end of the event application queue interval.
		Date upDate = new Date(salesDate.getTime() + (1000 * 60 * upInterval));

		return new Date[] { downDate, upDate };
	}

	// Get logged User.
	protected RegisteredUser getLoggedUser() throws NoLoggedUserException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String username = authentication.getName();
			return (RegisteredUser) userService.findOneByUsername(username).get();
		}
		throw new NoLoggedUserException();
	}
}
