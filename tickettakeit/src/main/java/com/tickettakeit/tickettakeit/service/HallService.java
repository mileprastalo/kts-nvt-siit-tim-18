package com.tickettakeit.tickettakeit.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tickettakeit.tickettakeit.converter.HallConverter;
import com.tickettakeit.tickettakeit.converter.SectionConverter;
import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.exceptions.HallAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallCreatingException;
import com.tickettakeit.tickettakeit.exceptions.HallDeleteException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallUpdateException;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.repository.HallRepository;

@Service
public class HallService {

	@Autowired
	HallRepository repository;
	@Autowired
	@Lazy
	LocationService locationService;
	@Autowired
	@Lazy
	SectionService sectionService;
	@Autowired
	@Lazy
	EventService eventService;

	public Hall findOne(Long id) {
		return repository.getOne(id);
	}

	public Optional<Hall> findById(Long id) {
		return repository.findById(id);
	}

	public List<Hall> findAll() {
		return repository.findAll();
	}

	public List<Hall> findAll(Pageable page) {
		Page<Hall> p = repository.findAll(page);
		return p.toList();
	}

	public Hall save(Hall hall) {
		return repository.save(hall);
	}

	public List<Hall> saveAll(List<Hall> hall) {
		return repository.saveAll(hall);
	}

	public void remove(Long id) {
		repository.deleteById(id);
	}

	public List<Hall> findHallsFromLocation(Long locationId) throws LocationNotExistsException {
		Optional<List<Hall>> optional = repository.findHallsFromLocation(locationId);
		return optional.orElseThrow(() -> new LocationNotExistsException(locationId));
	}

	public Hall createHall(Long locationID, HallDTO hallDTO)
			throws LocationNotExistsException, HallAlreadyExistsException, HallCreatingException {
		Location location = locationService.findOne(locationID)
				.orElseThrow(() -> new LocationNotExistsException(locationID));
		boolean exists = (location.getHalls().stream().filter(hall -> hall.getName().equals(hallDTO.getName()))
				.collect(Collectors.toList())).size() > 0;

		if (exists) {
			throw new HallAlreadyExistsException(hallDTO.getName());
		}
		Hall h;
		try {
			Hall h2 = HallConverter.fromDto(hallDTO);
			h = save(h2);
			location.getHalls().add(h);
			locationService.save(location);
		} catch (Exception e) {

			throw new HallCreatingException(hallDTO.getName());
		}
		return h;
	}

	public Hall findHallByLocationAndId(Long locationId, Long hallId) throws HallNotExistsException {
		Optional<Hall> optional = repository.findHallsFromLocationAndHall(locationId, hallId);
		return optional.orElseThrow(() -> new HallNotExistsException(hallId));
	}

	public Hall updateHall(Long locationID, Long hallID, HallDTO hallDTO)
			throws HallNotExistsException, HallUpdateException {
		Hall hall = repository.findHallsFromLocationAndHall(locationID, hallID)
				.orElseThrow(() -> new HallNotExistsException(hallID));
		hall.setName(hallDTO.getName());
		hall.setSections(hallDTO.getSections().stream().map(section -> SectionConverter.fromDto(section))
				.collect(Collectors.toSet()));
		hall.setActive(hallDTO.getActive());
		Hall h;
		try {
			h = repository.save(hall);
		} catch (Exception e) {
			throw new HallUpdateException(hall.getName());
		}
		return h;
	}

	public Hall deleteHall(Long locationID,Long hallID)
			throws HallNotExistsException, HallDeleteException {
		List<Event> events = null;
		try{
			events = eventService.selectEventsFromHall(hallID).orElseGet(null);
		}
		catch (Exception e) {
			
		}
		if (events != null) {
			Date today = new Date();
			for (Event event : events) {
				Set<Date> dates = event.getDate();
				for (Date d : dates) {
					if (d.after(today)) {
						throw new HallDeleteException();
					}
				}
			}
		}

		Hall hall = findHallByLocationAndId(locationID, hallID);
		hall.getSections().forEach((section) -> {
			section.setActive(false);
			sectionService.save(section);
		});
		hall.setActive(false);
		return repository.save(hall);

	}

}
