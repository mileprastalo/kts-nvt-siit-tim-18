package com.tickettakeit.tickettakeit.service;

import static java.time.DayOfWeek.MONDAY;
import static java.time.temporal.TemporalAdjusters.next;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tickettakeit.tickettakeit.dto.ReportDTO;
import com.tickettakeit.tickettakeit.dto.ReportDataDTO;
import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.model.REPORT_TYPE;
import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.repository.EventRepository;
import com.tickettakeit.tickettakeit.repository.LocationRepository;
import com.tickettakeit.tickettakeit.repository.TicketRepository;
import com.tickettakeit.tickettakeit.util.DateValidation;

@Service
public class ReportService {

	@Autowired
	TicketRepository ticketRepository;

	@Autowired
	LocationRepository locationRepository;

	@Autowired
	EventRepository eventRepository;

	public ArrayList<ReportDataDTO> createReports(ReportDTO reportDto) throws InvalidDateException {

		DateValidation.checkInThePast(reportDto.getFromDate(), reportDto.getToDate());

		if (reportDto.getReportType().equals(REPORT_TYPE.DAILY)) { // daily report

			return dailyReport(reportDto.getLocationId(), reportDto.getEventId(), reportDto.getEventDate(),
					reportDto.getFromDate(), reportDto.getToDate());

		} else if (reportDto.getReportType().equals(REPORT_TYPE.WEEKLY)) { // weekly report

			return weeklyReport(reportDto.getLocationId(), reportDto.getEventId(), reportDto.getEventDate(),
					reportDto.getFromDate(), reportDto.getToDate());

		} else { // monthly report

			return monthlyReport(reportDto.getLocationId(), reportDto.getEventId(), reportDto.getEventDate(),
					reportDto.getFromDate(), reportDto.getToDate());
		}

	}

	public ArrayList<ReportDataDTO> dailyReport(Long locationId, Long eventId, Date eventDate, Date fromDate,
			Date toDate) {

		ArrayList<ReportDataDTO> reportDTOList = new ArrayList<>();

		DateTimeComparator dateTimeComparator = DateTimeComparator.getDateOnlyInstance();
		Calendar c = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();

		do {
			c.setTime(fromDate);
			c2.setTime(fromDate);
			c2.add(Calendar.DATE, 1);
			reportDTOList.add(createReportData(locationId, eventId, eventDate, fromDate, c2.getTime()));

			c.add(Calendar.DATE, 1);
			fromDate = c.getTime();
		} while (dateTimeComparator.compare(fromDate, toDate) != 0);

		return reportDTOList;

	}

	public ArrayList<ReportDataDTO> weeklyReport(Long locationId, Long eventId, Date eventDate, Date fromDate,
			Date toDate) {

		ArrayList<ReportDataDTO> reportDTOList = new ArrayList<>();
		DateTimeComparator dateTimeComparator = DateTimeComparator.getDateOnlyInstance();
		Date nextMonday;

		do {
			// get next monday
			LocalDate fromDateLocal = Instant.ofEpochMilli(fromDate.getTime()).atZone(ZoneId.systemDefault())
					.toLocalDate();

			LocalDate nextMondayLocal = fromDateLocal.with(next(MONDAY));

			nextMonday = java.util.Date.from(nextMondayLocal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

			if (nextMonday.after(toDate)) {

				nextMonday = toDate;
			}

			reportDTOList.add(createReportData(locationId, eventId, eventDate, fromDate, nextMonday));

			fromDate = nextMonday;

		} while (dateTimeComparator.compare(nextMonday, toDate) != 0);

		return reportDTOList;
	}

	public ArrayList<ReportDataDTO> monthlyReport(Long locationId, Long eventId, Date eventDate, Date fromDate,
			Date toDate) {

		ArrayList<ReportDataDTO> reportDTOList = new ArrayList<>();

		Date nextMonth;
		do {

			fromDate.setDate(1);
			Calendar c = Calendar.getInstance();
			c.setTime(fromDate);
			c.add(Calendar.MONTH, 1);
			nextMonth = c.getTime();

			reportDTOList.add(createReportData(locationId, eventId, eventDate, fromDate, nextMonth));

			fromDate = nextMonth;
		} while (fromDate.before(toDate));

		return reportDTOList;
	}

	public ReportDataDTO createReportData(Long locationId, Long eventId, Date eventDate, Date fromDate, Date toDate) {

		List<Ticket> tickets;

		if (locationId != null) {
			tickets = ticketRepository.soldTicketsForLocation(locationId, fromDate, toDate);
		} else if (eventDate == null) {
			tickets = ticketRepository.soldTicketsForEvent(eventId, fromDate, toDate);
		} else {
			tickets = ticketRepository.soldTicketsForEventDay(eventId, eventDate, fromDate, toDate);
		}

		return new ReportDataDTO(tickets.stream().mapToDouble(x -> x.getPrice()).sum(), tickets.size());
	}
}
