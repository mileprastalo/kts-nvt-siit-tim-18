package com.tickettakeit.tickettakeit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tickettakeit.tickettakeit.model.Authority;
import com.tickettakeit.tickettakeit.repository.AuthorityRepository;

@Service
public class AuthorityService {

	@Autowired
	AuthorityRepository repository;
	
	public Authority findByID(Long id) {
		return repository.getOne(id);
	}
	

}
