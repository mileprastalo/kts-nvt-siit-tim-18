package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateEventDescriptionDTO {
	@NotNull
	private Long id;
	@NotNull
	@Size(min = 1, max = 100)
	private String description;

	public UpdateEventDescriptionDTO(@NotNull Long id, @NotNull String description) {
		this.id = id;
		this.description = description;
	}

	public UpdateEventDescriptionDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
