package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateEventNameDTO {
	@NotNull
	private Long id;
	@NotNull
	@Size(min = 2, max = 20)
	private String name;

	public UpdateEventNameDTO(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public UpdateEventNameDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
