package com.tickettakeit.tickettakeit.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;


public class HallDTO {
	private Long id;
	@NotEmpty
	private String name;
	private List<SectionDTO> sections;
	private boolean active;

	public HallDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SectionDTO> getSections() {
		return sections;
	}

	public void setSections(List<SectionDTO> sections) {
		this.sections = sections;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	

}
