package com.tickettakeit.tickettakeit.dto;

import java.util.ArrayList;
import java.util.Date;

import com.tickettakeit.tickettakeit.model.REPORT_TYPE;

public class ReportDTO {
	
	private Long locationId;
	private Long eventId;
	private Date eventDate;
	private Date fromDate;
	private Date toDate;
	private REPORT_TYPE reportType; // daily|weekly|monthly
	private ArrayList<ReportDataDTO> reportData;
	
	public ReportDTO() {
		super();
	}

	public ReportDTO(Long locationId, Long eventId, Date eventDate, Date fromDate, Date toDate, REPORT_TYPE reportType, ArrayList<ReportDataDTO> reportData) {
		super();
		this.locationId = locationId;
		this.eventId = eventId;
		this.eventDate = eventDate;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.reportType = reportType;
		this.reportData = reportData;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public REPORT_TYPE getReportType() {
		return reportType;
	}

	public void setReportType(REPORT_TYPE reportType) {
		this.reportType = reportType;
	}

	public ArrayList<ReportDataDTO> getReportData() {
		return reportData;
	}

	public void setReportData(ArrayList<ReportDataDTO> reportData) {
		this.reportData = reportData;
	}
	
}
