package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tickettakeit.tickettakeit.model.Ticket;

public class TicketDTO {
	@NotNull
	private Long id;
	@NotNull
	private String code;
	@NotNull
	private boolean bought;
	@NotNull
	@Size(min = 0)
	private int row;
	@NotNull
	@Size(min = 0)
	private int column;
	@NotNull
	@Size(min = 0)
	private double price;
	@NotNull
	private Long forDay;
	@NotNull
	private Long eventSectionID;
	@NotNull
	private Long eventID;

	public TicketDTO() {

	}

	public TicketDTO(Ticket t) {
		this.id = t.getId();
		this.code = t.getCode();
		this.bought = t.isBought();
		this.row = t.getRow();
		this.column = t.getColumn();
		this.price = t.getPrice();
		this.forDay = t.getForDay().getTime();
		this.eventSectionID = t.getEventSection().getId();
		this.eventID = t.getEvent().getId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isBought() {
		return bought;
	}

	public void setBought(boolean bought) {
		this.bought = bought;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Long getForDay() {
		return forDay;
	}

	public void setForDay(Long forDay) {
		this.forDay = forDay;
	}

	public Long getEventSectionID() {
		return eventSectionID;
	}

	public void setEventSectionID(Long eventSectionID) {
		this.eventSectionID = eventSectionID;
	}

	public Long getEventID() {
		return eventID;
	}

	public void setEventID(Long eventID) {
		this.eventID = eventID;
	}

}
