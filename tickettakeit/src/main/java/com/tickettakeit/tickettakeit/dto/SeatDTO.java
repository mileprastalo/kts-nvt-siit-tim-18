package com.tickettakeit.tickettakeit.dto;

import java.util.Date;

public class SeatDTO {
	private int row;
	private int column;
	private Date tickedDate;

	public SeatDTO() {
	}

	public SeatDTO(int row, int column, Date tickedDate) {
		super();
		this.row = row;
		this.column = column;
		this.tickedDate = tickedDate;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public Date getTickedDate() {
		return tickedDate;
	}

	public void setTickedDate(Date tickedDate) {
		this.tickedDate = tickedDate;
	}

}
