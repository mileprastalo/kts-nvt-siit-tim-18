package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CategoryDTO {
	
	private Long id;
	@NotEmpty
	@Pattern(regexp = "[a-z A-Z]*", message = "Category name must contain only letters")
	@Size(min=3, max=20)
	private String name;
	@NotEmpty
	private String color;
	@NotEmpty
	private String icon;
	
	public CategoryDTO() {
		super();
	}

	public CategoryDTO(Long id, String name, String color, String icon) {
		super();
		this.id = id;
		this.name = name;
		this.color = color;
		this.icon = icon;
	}
	
	@Override
	public String toString() {
		return "CategoryDTO [id=" + id + ", name=" + name + ", color=" + color + ", icon=" + icon + "]";
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
		
}
