package com.tickettakeit.tickettakeit.dto;

public class EventApplicationDTO {
	private Long id;
	private Long userId;
	private Long eventId;
	private int queueNumber;
	private Long downInterval;
	private long upInterval;
	private String eventName;

	public EventApplicationDTO(Long id, Long userId, Long eventId, int queueNumber, Long downInterval,
			long upInterval, String eventName) {
		this.id = id;
		this.userId = userId;
		this.eventId = eventId;
		this.queueNumber = queueNumber;
		this.downInterval = downInterval;
		this.upInterval = upInterval;
		this.eventName = eventName;
	}

	public EventApplicationDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public int getQueueNumber() {
		return queueNumber;
	}

	public void setQueueNumber(int queueNumber) {
		this.queueNumber = queueNumber;
	}

	public Long getDownInterval() {
		return downInterval;
	}

	public void setDownInterval(Long downInterval) {
		this.downInterval = downInterval;
	}

	public long getUpInterval() {
		return upInterval;
	}

	public void setUpInterval(long upInterval) {
		this.upInterval = upInterval;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

}
