package com.tickettakeit.tickettakeit.dto;

import java.util.ArrayList;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

@Validated
public class NewEventDTO {
	@NotNull
	@Size(min = 2, max = 50)
	private String name;
	@NotNull
	@Size(min = 1, max = 200)
	private String description;
	@NotNull
	@Size(min = 1)
	private ArrayList<Long> date;
	@NotNull
	private Long salesDate;
	@NotNull
	private int status;
	@NotNull
	private int lastDayToPay;
	@NotNull
	private boolean controllAccess;
	@NotNull
	//@Size(min = 1)
	private ArrayList<String> attachments;
	@NotNull
	private Long hallID;
	@NotNull
	private ArrayList<Long> categoriesIDs;
	@NotNull
	@Size(min = 1)
	private Map<Long, Float> eventSectionsIDs;
	@NotNull
	private Long locationID;

	public NewEventDTO() {
	}

	public NewEventDTO(@NotNull @Size(min = 2, max = 20) String name,
			@NotNull @Size(min = 1, max = 100) String description, @NotNull @Size(min = 1) ArrayList<Long> date,
			@NotNull Long salesDate, @NotNull int status, @NotNull int lastDayToPay, @NotNull boolean controllAccess,
			@NotNull @Size(min = 1) ArrayList<String> attachments, @NotNull Long hallID,
			@NotNull ArrayList<Long> categoriesIDs, @NotNull @Size(min = 1) Map<Long, Float> eventSectionsIDs,
			@NotNull Long locationID) {
		super();
		this.name = name;
		this.description = description;
		this.date = date;
		this.salesDate = salesDate;
		this.status = status;
		this.lastDayToPay = lastDayToPay;
		this.controllAccess = controllAccess;
		this.attachments = attachments;
		this.hallID = hallID;
		this.categoriesIDs = categoriesIDs;
		this.eventSectionsIDs = eventSectionsIDs;
		this.locationID = locationID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Long> getDate() {
		return date;
	}

	public void setDate(ArrayList<Long> date) {
		this.date = date;
	}

	public Long getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(Long salesDate) {
		this.salesDate = salesDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getLastDayToPay() {
		return lastDayToPay;
	}

	public void setLastDayToPay(int lastDayToPay) {
		this.lastDayToPay = lastDayToPay;
	}

	public boolean isControllAccess() {
		return controllAccess;
	}

	public void setControllAccess(boolean controllAccess) {
		this.controllAccess = controllAccess;
	}

	public ArrayList<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}

	public Long getHallID() {
		return hallID;
	}

	public void setHallID(Long hallID) {
		this.hallID = hallID;
	}

	public ArrayList<Long> getCategoriesIDs() {
		return categoriesIDs;
	}

	public void setCategoriesIDs(ArrayList<Long> categoriesIDs) {
		this.categoriesIDs = categoriesIDs;
	}

	public Map<Long, Float> getEventSectionsIDs() {
		return eventSectionsIDs;
	}

	public void setEventSectionsIDs(Map<Long, Float> eventSectionsIDs) {
		this.eventSectionsIDs = eventSectionsIDs;
	}

	public Long getLocationID() {
		return locationID;
	}

	public void setLocationID(Long locationID) {
		this.locationID = locationID;
	}

}
