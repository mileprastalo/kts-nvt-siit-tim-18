package com.tickettakeit.tickettakeit.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@DataJpaTest
public class EventRepositoryUnitTest {

	private static PageRequest pageRequest = PageRequest.of(0, 4);

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private EventRepository eventRepository;

	// TODO Find a better way to do this
	private ArrayList<Long> categoryIds = new ArrayList<>();

	@Before
	public void setUp() {

		// Set up event categories
		Category category1 = new Category("Category 1", "", "");
		Category category2 = new Category("Category 2", "", "");
		Category category3 = new Category("Category 3", "", "");

		categoryIds.add(entityManager.persistAndGetId(category1, Long.class));
		categoryIds.add(entityManager.persistAndGetId(category2, Long.class));
		categoryIds.add(entityManager.persistAndGetId(category3, Long.class));

		// Set up event sections
		EventSection es1 = new EventSection("east", 10, 10, true, 0, 100);
		EventSection es2 = new EventSection("west", 10, 10, true, 0, 150);
		EventSection es3 = new EventSection("south", 10, 10, true, 0, 200);
		EventSection es4 = new EventSection("north", 10, 10, true, 0, 250);

		Set<EventSection> sections1 = new HashSet<>();
		sections1.add(es1);

		Set<EventSection> sections2 = new HashSet<>();
		sections2.add(es2);

		Set<EventSection> sections3 = new HashSet<>();
		sections3.add(es3);
		sections3.add(es4);

		entityManager.persist(es1);
		entityManager.persist(es2);
		entityManager.persist(es3);
		entityManager.persist(es4);

		// Set up locations
		Location location1 = new Location("Location 1", "City 1", "Address 1", 100, 100, "", null, true);
		Location location2 = new Location("Location 2", "City 1", "Address 2", 100, 100, "", null, true);

		entityManager.persist(location1);
		entityManager.persist(location2);

		// Set up halls
		Hall hall1 = new Hall("Hall 1");
		Hall hall2 = new Hall("Hall 2");
		entityManager.persist(hall1);
		entityManager.persist(hall2);

		// Set up events
		Event event1 = new Event("Event 1", EventStatus.NORMAL, 0, false, hall1);
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime());
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 21).getTime());
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 22).getTime());
		event1.getCategories().add(category1);
		event1.setLocation(location1);

		Event event2 = new Event("Event 2", EventStatus.NORMAL, 0, false, hall1);
		event2.getDate().add(new GregorianCalendar(2020, Calendar.APRIL, 20).getTime());
		event2.getCategories().add(category1);
		event2.getCategories().add(category2);
		event2.setLocation(location2);

		Event event3 = new Event("Event 3", EventStatus.NORMAL, 0, false, hall1);
		event3.getCategories().add(category1);
		event3.setLocation(location2);

		Event event4 = new Event("Event 4", EventStatus.NORMAL, 0, false, hall1);

		event1.setEventSections(sections1);
		event2.setEventSections(sections2);
		event3.setEventSections(sections3);

		entityManager.persist(event1);
		entityManager.persist(event2);
		entityManager.persist(event3);
		entityManager.persist(event4);

		entityManager.flush();

	}

	// Checks if both Events with given Hall are found.
	@Test
	public void whenFindByHall_thenReturnEvents() {
		// given
		Hall hall = new Hall("Hall 1");
		Event event1 = new Event("Event 1", EventStatus.NORMAL, 0, false, hall);
		Event event2 = new Event("Event 2", EventStatus.NORMAL, 0, false, hall);
		entityManager.persist(hall);
		entityManager.persist(event1);
		entityManager.persist(event2);
		entityManager.flush();

		// when
		ArrayList<Event> foundEvents = (ArrayList<Event>) eventRepository.findByHall(hall);

		// then
		assertEquals(2, foundEvents.size());

	}

	// Checks if exactly 1 Event out of 2 with given Hall is found.
	@Test
	public void whenFindByHall_thenReturnEvents2() {
		// given
		Hall hall1 = new Hall("Hall 1");
		Hall hall2 = new Hall("Hall 2");
		Event event1 = new Event("Event 1", EventStatus.NORMAL, 0, false, hall1);
		Event event2 = new Event("Event 2", EventStatus.NORMAL, 0, false, hall2);
		entityManager.persist(hall1);
		entityManager.persist(hall2);
		entityManager.persist(event1);
		entityManager.persist(event2);
		entityManager.flush();

		// when
		ArrayList<Event> foundEvents = (ArrayList<Event>) eventRepository.findByHall(hall2);

		// then
		assertEquals(1, foundEvents.size());

	}

	// ********* findByPrice *************
	@Test
	public void findByPrice_findMultiple() {

		Page<Event> foundEvents = eventRepository.findByPrice(pageRequest, 100, 200);

		assertThat(foundEvents).hasSize(3).anyMatch(event -> event.getName().equals("Event 1"))
				.anyMatch(event -> event.getName().equals("Event 2"))
				.anyMatch(event -> event.getName().equals("Event 3"));

	}

	@Test
	public void findByPrice_findOne() {

		Page<Event> foundEvents = eventRepository.findByPrice(pageRequest, 201, 260);

		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getName().equals("Event 3"));
	}

	@Test
	public void findByPrice_noResults() {

		Page<Event> foundEvents = eventRepository.findByPrice(pageRequest, 10, 99);

		assertThat(foundEvents).hasSize(0);
	}

	// ********* findByDate *************

	@Test
	public void findByDate_findMultiple() {

		Page<Event> foundEvents = eventRepository.findByDate(pageRequest,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.APRIL, 20).getTime());

		assertThat(foundEvents).hasSize(2).anyMatch(event -> event.getName().equals("Event 1"))
				.anyMatch(event -> event.getName().equals("Event 2"));
	}

	@Test
	public void findByDate_findOne() {

		Page<Event> foundEvents = eventRepository.findByDate(pageRequest,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 21).getTime(),
				new GregorianCalendar(2020, Calendar.APRIL, 19).getTime());

		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getName().equals("Event 1"));
	}

	@Test
	public void findByDate_noResults() {

		Page<Event> foundEvents = eventRepository.findByDate(pageRequest,
				new GregorianCalendar(2020, Calendar.APRIL, 22).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime());

		assertThat(foundEvents).hasSize(0);

	}

	// ****** findByCategoryName *******
	@Test
	public void findByCategoryName_findMultiple() {

		Page<Event> foundEvents = eventRepository.findByCategoryName(pageRequest, "Category 1");

		assertThat(foundEvents).hasSize(3).anyMatch(event -> event.getName().equals("Event 1"))
				.anyMatch(event -> event.getName().equals("Event 2"))
				.anyMatch(event -> event.getName().equals("Event 3"));
	}

	@Test
	public void findByCategoryName_findOne() {

		Page<Event> foundEvents = eventRepository.findByCategoryName(pageRequest, "Category 2");

		System.out.println(
				"\n\n\n Category id is: " + foundEvents.toList().get(0).getCategories().iterator().next().getId());
		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getName().equals("Event 2"));

	}

	@Test
	public void findByCategoryName_noResults() {

		Page<Event> foundEvents = eventRepository.findByCategoryName(pageRequest, "Category 3");

		assertThat(foundEvents).hasSize(0);
	}

	// ***** findByCategory ********
	@Test
	public void findByCategory_findMultiple() {

		Page<Event> foundEvents = eventRepository.findByCategory(pageRequest, categoryIds);

		assertThat(foundEvents).hasSize(3).anyMatch(event -> event.getName().equals("Event 1"))
				.anyMatch(event -> event.getName().equals("Event 2"))
				.anyMatch(event -> event.getName().equals("Event 3"));
	}

	@Test
	public void findByCategory_findOne() {
		ArrayList<Long> ids = new ArrayList<Long>();
		ids.add(categoryIds.get(0));

		Page<Event> foundEvents = eventRepository.findByCategory(pageRequest, ids);

		assertThat(foundEvents).allMatch(event -> event.getCategories().stream().map(Category::getId)
				.collect(Collectors.toSet()).contains(ids.get(0)));

	}

	@Test
	public void findByCategory_noResults() {

		Page<Event> foundEvents = eventRepository.findByCategory(pageRequest, new ArrayList<Long>());

		assertThat(foundEvents).hasSize(0);
	}

	// *********** findByNameCityLocationPriceCategory *****************

	@Test
	public void findByNameCityLocationPriceCategory_matchAll() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPriceCategory(pageRequest, "Event 1", "City 1",
				"Location 1", 0, 2000, categoryIds);

		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getName().equals("Event 1"))
				.anyMatch(event -> event.getLocation().getName().equals("Location 1"));

	}

	@Test
	public void findByNameCityLocationPriceCategory_matchEventName() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPriceCategory(pageRequest, "Event 2", "", "", 0,
				2000, categoryIds);

		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getName().equals("Event 2"));

	}

	@Test
	public void findByNameCityLocationPriceCategory_matchCityName() {

		Page<Event> foundEvents2 = eventRepository.findByNameCityLocationPriceCategory(pageRequest, "", "City 1", "", 0,
				2000, categoryIds);

		assertThat(foundEvents2).hasSize(3);

	}

	@Test
	public void findByNameCityLocationPriceCategory_noResults() {
		Page<Event> foundEvents2 = eventRepository.findByNameCityLocationPriceCategory(pageRequest, "", "City 1", "", 0,
				2000, new ArrayList<Long>());

		assertThat(foundEvents2).hasSize(0);
	}

	// ***** findByNameCityLocationPriceDate *****
	@Test
	public void findByNameCityLocationPriceDate_matchAll() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPriceDate(pageRequest, "Event 1", "City 1",
				"Location 1", 0, 2000, new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime());

		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getName().equals("Event 1"))
				.anyMatch(event -> event.getLocation().getName().equals("Location 1"));
	}

	@Test
	public void findByNameCityLocationPriceDate_matchSubstring() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPriceDate(pageRequest, "Event ", "City ", "", 0,
				2000, new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime());

		assertThat(foundEvents).hasSize(2).anyMatch(event -> event.getName().equals("Event 1"))
				.anyMatch(event -> event.getName().equals("Event 2"));
	}

	@Test
	public void findByNameCityLocationPriceDate_emptyStrings() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPriceDate(pageRequest, "", "", "", 0, 2000,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime());

		assertThat(foundEvents).hasSize(2).anyMatch(event -> event.getName().equals("Event 1"))
				.anyMatch(event -> event.getName().equals("Event 2"));
	}

	@Test
	public void findByNameCityLocationPriceDate_noResults() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPriceDate(pageRequest, "", "", "", 0, 2000,
				new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime());

		assertThat(foundEvents).hasSize(0);

	}

	// ***** findByNameCityLocationPrice *****
	@Test
	public void findByNameCityLocationPrice_matchAll() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPrice(pageRequest, "Event 1", "City 1",
				"Location 1", 0, 2000);

		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getName().equals("Event 1"));
	}

	@Test
	public void findByNameCityLocationPrice_matchSubstring() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPrice(pageRequest, "Event ", "City ",
				"Location ", 0, 2000);

		assertThat(foundEvents).hasSize(3);
	}

	@Test
	public void findByNameCityLocationPrice_emptyStrings() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPrice(pageRequest, "", "", "", 0, 2000);

		assertThat(foundEvents).hasSize(3);

	}

	@Test
	public void findByNameCityLocationPrice_noResults() {

		Page<Event> foundEvents = eventRepository.findByNameCityLocationPrice(pageRequest, "", "", "", 0, 20);

		assertThat(foundEvents).hasSize(0);

	}

	// ***** detailedSearch *****

	@Test
	public void detailedSearch_matchAll() {

		Page<Event> foundEvents = eventRepository.detailedSearch(pageRequest, "Event 1", "City 1", "Location 1", 0,
				2000, new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime(), categoryIds);

		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getName().equals("Event 1"));
	}

	@Test
	public void detailedSearch_matchSubstrings() {

		Page<Event> foundEvents = eventRepository.detailedSearch(pageRequest, "Event ", "City ", "Location", 0, 2000,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime(), categoryIds);

		assertThat(foundEvents).hasSize(2);
	}

	@Test
	public void detailedSearch_noMatchingEventName() {

		Page<Event> foundEvents = eventRepository.detailedSearch(pageRequest, "ahodghro", "City 1", "Location 1", 0,
				2000, new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime(), categoryIds);

		assertThat(foundEvents).hasSize(0);

	}

	@Test
	public void detailedSearch_noMatchingCity() {

		Page<Event> foundEvents = eventRepository.detailedSearch(pageRequest, "Event 1", "adfhoaw", "Location 1", 0,
				2000, new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime(), categoryIds);

		assertThat(foundEvents).hasSize(0);

	}

	@Test
	public void detailedSearch_noMatchingPrice() {

		Page<Event> foundEvents = eventRepository.detailedSearch(pageRequest, "Event 1", "City 1", "Location 1", 0, 10,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime(), categoryIds);

		assertThat(foundEvents).hasSize(0);
	}

	@Test
	public void detailedSearch_noMatchingDate() {

		Page<Event> foundEvents = eventRepository.detailedSearch(pageRequest, "Event 4", "City 1", "Location 1", 0,
				2000, new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime(), categoryIds);

		assertThat(foundEvents).hasSize(0);

	}

	@Test
	public void detailedSearch_noMatchingCategories() {

		Page<Event> foundEvents = eventRepository.detailedSearch(pageRequest, "Event 4", "City 1", "Location 1", 0,
				2000, new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime(), new ArrayList<Long>());

		assertThat(foundEvents).hasSize(0);

	}

	// ***** generalSearch *****
	@Test
	public void generalSearch_matchEventName() {

		Page<Event> foundEvents = eventRepository.generalSearch(pageRequest, "Event");
		assertThat(foundEvents).hasSize(3);
	}

	@Test
	public void generalSearch_matchLocationName() {

		Page<Event> foundEvents = eventRepository.generalSearch(pageRequest, "Location 1");
		assertThat(foundEvents).hasSize(1).anyMatch(event -> event.getLocation().getName().equals("Location 1"));
	}

	@Test
	public void generalSearch_matchCity() {

		Page<Event> foundEvents = eventRepository.generalSearch(pageRequest, "City");
		assertThat(foundEvents).hasSize(3);
	}

	@Test
	public void generalSearch_matchAny() {

		Page<Event> foundEvents = eventRepository.generalSearch(pageRequest, "2");
		assertThat(foundEvents).hasSize(2).anyMatch(event -> event.getName().equals("Event 2"))
				.anyMatch(event -> event.getName().equals("Event 3"));
	}

	@Test
	public void generalSearch_matchEmptyString() {

		Page<Event> foundEvents = eventRepository.generalSearch(pageRequest, "");
		assertThat(foundEvents).hasSize(3);
	}

	@Test
	public void generalSearch_noResults() {
		Page<Event> foundEvents = eventRepository.generalSearch(pageRequest, "afghatehsrthrt");
		assertThat(foundEvents).hasSize(0);
	}

	@Test
	public void selectEventsFromLocation() {
		Location l = new Location("Location", "NS", "Address", 50, 50, "", new HashSet<Hall>(), true);
		Long locationId = entityManager.persistAndGetId(l, Long.class);
		l.setId(locationId);
		Hall hall = new Hall("Hall");
		entityManager.persist(hall);
		Event e = new Event("Event", EventStatus.NORMAL, 2, false, hall);
		e.setLocation(l);
		Long eventId = entityManager.persistAndGetId(e, Long.class);
		Optional<List<Event>> events = eventRepository.selectEventsFromLocation(locationId);
		assertEquals(1, events.get().size());
		assertEquals(eventId, events.get().get(0).getId());
	}

	@Test
	public void selectEventsFromHall() {
		Location l = new Location("Location", "NS", "Address", 50, 50, "", new HashSet<Hall>(), true);
		Long locationId = entityManager.persistAndGetId(l, Long.class);
		l.setId(locationId);
		Hall hall = new Hall("Hall");
		Long hallId = entityManager.persistAndGetId(hall, Long.class);
		Event e = new Event("Event", EventStatus.NORMAL, 2, false, hall);
		e.setLocation(l);
		Long eventId = entityManager.persistAndGetId(e, Long.class);
		Optional<List<Event>> events = eventRepository.selectEventsFromHall(hallId);
		assertEquals(1, events.get().size());
		assertEquals(eventId, events.get().get(0).getId());
	}

}
