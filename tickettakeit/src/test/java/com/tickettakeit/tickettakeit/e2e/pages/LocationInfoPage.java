package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LocationInfoPage {

	private WebDriver driver;

	@FindBy(id = "newHallButton")
	private WebElement newHallButton;
	@FindBy(id = "updateButton")
	private WebElement updateButton;

	@FindBy(id = "locationName")
	private WebElement locationName;
	@FindBy(xpath = "//*[@id='hallTable']/tbody/tr/td[1]")
	private WebElement hallName;

	@FindBy(xpath = "//*[@id='hallTable']/tbody/tr[1]/td[4]/button")
	private WebElement deactivateBt;

	@FindBy(xpath = "//*[@id='hallTable']/tbody/tr[1]/td[3]")
	private WebElement status;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getNewHallButton() {
		return newHallButton;
	}

	public void setNewHallButton(WebElement newHallButton) {
		this.newHallButton = newHallButton;
	}

	public WebElement getUpdateButton() {
		return updateButton;
	}

	public void setUpdateButton(WebElement updateButton) {
		this.updateButton = updateButton;
	}

	public WebElement getLocationName() {
		return locationName;
	}

	public void setLocationName(WebElement locationName) {
		this.locationName = locationName;
	}

	public WebElement getHallName() {
		return hallName;
	}

	public void setHallName(WebElement hallName) {
		this.hallName = hallName;
	}

	public WebElement getDeactivateBt() {
		return deactivateBt;
	}

	public void setDeactivateBt(WebElement deactivateBt) {
		this.deactivateBt = deactivateBt;
	}

	public WebElement getStatus() {
		return status;
	}

	public void setStatus(WebElement status) {
		this.status = status;
	}

	public void ensureUpdateButtonPresend() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(updateButton));
	}

	public void ensureCreateHallButtonPresend() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(newHallButton));
	}

	public void ensureHallIsPresend() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(hallName));
	}

	public void ensureDeactivateHallIsPresend() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(deactivateBt));
	}

}
