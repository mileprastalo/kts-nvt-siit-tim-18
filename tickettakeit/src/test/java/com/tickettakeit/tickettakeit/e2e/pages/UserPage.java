package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserPage {

	private WebDriver driver;
	
	@FindBy(xpath = "//*[@id=\"mat-tab-label-0-1\"]")
	private WebElement reservedTicketsTab;
	@FindBy(xpath = "//*[@id=\"mat-tab-label-0-2\"]")
	private WebElement boughtTicketsTab;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getReservedTicketsTab() {
		return reservedTicketsTab;
	}

	public WebElement getBoughtTicketsTab() {
		return boughtTicketsTab;
	}

	public void ensureReservedTicketsTabIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(reservedTicketsTab));
	}
	
	public void ensureBoughtTicketsTabIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(boughtTicketsTab));
	}
}
