package com.tickettakeit.tickettakeit.e2e;

import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.NEW_CATEGORY_NAME;
import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.NEW_ICON_PATH;
import static com.tickettakeit.tickettakeit.e2e.constants.URLConstants.LOGIN_PAGE_URL;
import static com.tickettakeit.tickettakeit.e2e.constants.UserConstants.ADMIN_PASSWORD;
import static com.tickettakeit.tickettakeit.e2e.constants.UserConstants.ADMIN_USERNAME;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.tickettakeit.tickettakeit.e2e.pages.AddEventPage;
import com.tickettakeit.tickettakeit.e2e.pages.AdminPage;
import com.tickettakeit.tickettakeit.e2e.pages.CategoriesListPage;
import com.tickettakeit.tickettakeit.e2e.pages.CreateUpdateCategoryPage;
import com.tickettakeit.tickettakeit.e2e.pages.LoginPage;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AddEventE2E {
	private AdminPage adminPage;
	private AddEventPage addEventPage;
	private LoginPage loginPage;
	private WebDriver browser;
	private JavascriptExecutor jsExecutor;

	private Calendar eventDateCalendar1;
	private Calendar eventDateCalendar2;
	private Calendar salesDateCalendar;
	private SimpleDateFormat sdf;

	@BeforeEach
	public void setupSelenium() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		jsExecutor = (JavascriptExecutor) browser;
		browser.navigate().to(LOGIN_PAGE_URL);

		loginPage = PageFactory.initElements(browser, LoginPage.class);
		loginPage.setDriver(browser);
		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName(ADMIN_USERNAME);
		loginPage.setPassword(ADMIN_PASSWORD);
		loginPage.getLoginButton().click();
		adminPage = PageFactory.initElements(browser, AdminPage.class);
		adminPage.setDriver(browser);
		addEventPage = PageFactory.initElements(browser, AddEventPage.class);
		addEventPage.setDriver(browser);

		adminPage.ensureAddEventTabIsDisplayed();
		adminPage.getAddEventTab().click();

	}

	@AfterEach
	public void closeBrowser() {
		addEventPage.getDriver().quit();
	}

	@Test
	@Order(2)
	public void addEvent_success() throws InterruptedException {

		// Sets up data
		eventDateCalendar1 = GregorianCalendar.getInstance();
		eventDateCalendar1.clear();
		eventDateCalendar1.set(2020, 10, 10);
		eventDateCalendar2 = GregorianCalendar.getInstance();
		eventDateCalendar2.clear();
		eventDateCalendar2.set(2020, 10, 11);
		sdf = new SimpleDateFormat("MM/dd/yyyy");

		String eventDateStr1 = sdf.format(eventDateCalendar1.getTime());
		String eventDateStr2 = sdf.format(eventDateCalendar2.getTime());

		salesDateCalendar = GregorianCalendar.getInstance();
		salesDateCalendar.clear();
		salesDateCalendar.set(2020, 8, 10);

		String salesDate = sdf.format(salesDateCalendar.getTime());

		// Test
		addEventPage.ensureAddDateBtnIsDisplayed();

		addEventPage.setName("Event Test 2");
		addEventPage.setDescription("Event Test 2 description.");

		addEventPage.setAddDatePicker(eventDateStr1);
		addEventPage.getAddDateBtn().click();
		addEventPage.setAddDatePicker(eventDateStr2);
		addEventPage.getAddDateBtn().click();

		addEventPage.setSalesDatePicker(salesDate);
		addEventPage.setLastDayToPay("1");

		addEventPage.getControllAccessSelect().click();
		addEventPage.getDriver().findElement(By.id("controllAccessFalse")).click();

		addEventPage.ensureFileInputBtnIsDisplayed();

		String filename = "C:\\Users\\NikolaS\\Pictures\\Saved Pictures\\Red-Panda-Parker-001-Birmingham-Zoo-2-27-18-1024x801.jpg";
		File file = new File(filename);
		String path = file.getAbsolutePath();

		addEventPage.setFileInput(path);

		filename = "C:\\Users\\NikolaS\\Pictures\\Saved Pictures\\zoos-animal-parks-netherlands.jpg";
		file = new File(filename);
		path = file.getAbsolutePath();

		addEventPage.setAttachmentInput(path);

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getAddCategoryBtn());

		addEventPage.getCategorySelect().click();
		addEventPage.getDriver().findElement(By.id("Music Festival")).click();
		addEventPage.getAddCategoryBtn().click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getLocationSelect());

		addEventPage.getLocationSelect().click();
		addEventPage.getDriver().findElement(By.id("Beogradska arena")).click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getHallSelect());

		addEventPage.ensureHallIsDisplayed();
		addEventPage.getHallSelect().click();
		addEventPage.getDriver().findElement(By.id("Hall 3")).click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getSectionSelect());

		addEventPage.ensureSectionIsDisplayed();
		addEventPage.getSectionSelect().click();
		addEventPage.getDriver().findElement(By.id("Section 2")).click();

		addEventPage.setSectionPrice("200");

		addEventPage.getAddSectionBtn().click();

		addEventPage.ensureAddEventBtnIsDisplayed();

		// addEventPage.ensureSnackBarIsInvisible();

		Thread.sleep(5000);

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getAddEventBtn());

		addEventPage.getAddEventBtn().click();

		addEventPage.ensureSuccessSnackBarIsDisplayed();
		assertEquals("Event Event Test 2 successfully added.", addEventPage.getSuccessSnackBar().getText());

	}

	@Test
	@Order(3)
	public void addEvent_NameIsTaken() throws InterruptedException {

		// Sets up data
		eventDateCalendar1 = GregorianCalendar.getInstance();
		eventDateCalendar1.clear();
		eventDateCalendar1.set(2020, 10, 10);
		eventDateCalendar2 = GregorianCalendar.getInstance();
		eventDateCalendar2.clear();
		eventDateCalendar2.set(2020, 10, 11);
		sdf = new SimpleDateFormat("MM/dd/yyyy");

		String eventDateStr1 = sdf.format(eventDateCalendar1.getTime());
		String eventDateStr2 = sdf.format(eventDateCalendar2.getTime());

		salesDateCalendar = GregorianCalendar.getInstance();
		salesDateCalendar.clear();
		salesDateCalendar.set(2020, 8, 10);

		String salesDate = sdf.format(salesDateCalendar.getTime());

		// Test
		addEventPage.ensureAddDateBtnIsDisplayed();

		addEventPage.setName("Event Test 2");
		addEventPage.setDescription("Event Test 2 description.");

		addEventPage.setAddDatePicker(eventDateStr1);
		addEventPage.getAddDateBtn().click();
		addEventPage.setAddDatePicker(eventDateStr2);
		addEventPage.getAddDateBtn().click();

		addEventPage.setSalesDatePicker(salesDate);
		addEventPage.setLastDayToPay("1");

		addEventPage.getControllAccessSelect().click();
		addEventPage.getDriver().findElement(By.id("controllAccessFalse")).click();

		addEventPage.ensureFileInputBtnIsDisplayed();

		String filename = "C:\\Users\\NikolaS\\Pictures\\Saved Pictures\\Red-Panda-Parker-001-Birmingham-Zoo-2-27-18-1024x801.jpg";
		File file = new File(filename);
		String path = file.getAbsolutePath();

		addEventPage.setFileInput(path);

		filename = "C:\\Users\\NikolaS\\Pictures\\Saved Pictures\\zoos-animal-parks-netherlands.jpg";
		file = new File(filename);
		path = file.getAbsolutePath();

		addEventPage.setAttachmentInput(path);

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getAddCategoryBtn());

		addEventPage.getCategorySelect().click();
		addEventPage.getDriver().findElement(By.id("Music Festival")).click();
		addEventPage.getAddCategoryBtn().click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getLocationSelect());

		addEventPage.getLocationSelect().click();
		addEventPage.getDriver().findElement(By.id("Beogradska arena")).click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getHallSelect());

		addEventPage.ensureHallIsDisplayed();
		addEventPage.getHallSelect().click();
		addEventPage.getDriver().findElement(By.id("Hall 3")).click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getSectionSelect());

		addEventPage.ensureSectionIsDisplayed();
		addEventPage.getSectionSelect().click();
		addEventPage.getDriver().findElement(By.id("Section 2")).click();

		addEventPage.setSectionPrice("200");

		addEventPage.getAddSectionBtn().click();

		addEventPage.ensureAddEventBtnIsDisplayed();

		// addEventPage.ensureSnackBarIsInvisible();

		Thread.sleep(5000);

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getAddEventBtn());

		addEventPage.getAddEventBtn().click();

		addEventPage.ensureErrorSnackBarIsDisplayed();
		assertEquals("Event name is taken.", addEventPage.getErrorSnackBar().getText());

	}

	@Test
	@Order(4)
	public void addEvent_HallIsTaken() throws InterruptedException {

		// Sets up data
		eventDateCalendar1 = GregorianCalendar.getInstance();
		eventDateCalendar1.clear();
		eventDateCalendar1.set(2020, 10, 10);
		eventDateCalendar2 = GregorianCalendar.getInstance();
		eventDateCalendar2.clear();
		eventDateCalendar2.set(2020, 10, 11);
		sdf = new SimpleDateFormat("MM/dd/yyyy");

		String eventDateStr1 = sdf.format(eventDateCalendar1.getTime());
		String eventDateStr2 = sdf.format(eventDateCalendar2.getTime());

		salesDateCalendar = GregorianCalendar.getInstance();
		salesDateCalendar.clear();
		salesDateCalendar.set(2020, 8, 10);

		String salesDate = sdf.format(salesDateCalendar.getTime());

		// Test
		addEventPage.ensureAddDateBtnIsDisplayed();

		addEventPage.setName("Event Test 3");
		addEventPage.setDescription("Event Test 2 description.");

		addEventPage.setAddDatePicker(eventDateStr1);
		addEventPage.getAddDateBtn().click();
		addEventPage.setAddDatePicker(eventDateStr2);
		addEventPage.getAddDateBtn().click();

		addEventPage.setSalesDatePicker(salesDate);
		addEventPage.setLastDayToPay("1");

		addEventPage.getControllAccessSelect().click();
		addEventPage.getDriver().findElement(By.id("controllAccessFalse")).click();

		addEventPage.ensureFileInputBtnIsDisplayed();

		String filename = "C:\\Users\\NikolaS\\Pictures\\Saved Pictures\\Red-Panda-Parker-001-Birmingham-Zoo-2-27-18-1024x801.jpg";
		File file = new File(filename);
		String path = file.getAbsolutePath();

		addEventPage.setFileInput(path);

		filename = "C:\\Users\\NikolaS\\Pictures\\Saved Pictures\\zoos-animal-parks-netherlands.jpg";
		file = new File(filename);
		path = file.getAbsolutePath();

		addEventPage.setAttachmentInput(path);

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getAddCategoryBtn());

		addEventPage.getCategorySelect().click();
		addEventPage.getDriver().findElement(By.id("Music Festival")).click();
		addEventPage.getAddCategoryBtn().click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getLocationSelect());

		addEventPage.getLocationSelect().click();
		addEventPage.getDriver().findElement(By.id("Beogradska arena")).click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getHallSelect());

		addEventPage.ensureHallIsDisplayed();
		addEventPage.getHallSelect().click();
		addEventPage.getDriver().findElement(By.id("Hall 3")).click();

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getSectionSelect());

		addEventPage.ensureSectionIsDisplayed();
		addEventPage.getSectionSelect().click();
		addEventPage.getDriver().findElement(By.id("Section 2")).click();

		addEventPage.setSectionPrice("200");

		addEventPage.getAddSectionBtn().click();

		addEventPage.ensureAddEventBtnIsDisplayed();

		// addEventPage.ensureSnackBarIsInvisible();

		Thread.sleep(5000);

		((JavascriptExecutor) addEventPage.getDriver()).executeScript("arguments[0].scrollIntoView();",
				addEventPage.getAddEventBtn());

		addEventPage.getAddEventBtn().click();

		addEventPage.ensureErrorSnackBarIsDisplayed();
		assertEquals("Hall with ID: 300is taken for the date: 11.11.2020", addEventPage.getErrorSnackBar().getText());

	}

}
