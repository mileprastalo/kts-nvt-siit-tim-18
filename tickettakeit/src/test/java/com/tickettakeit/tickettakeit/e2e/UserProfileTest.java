package com.tickettakeit.tickettakeit.e2e;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import com.tickettakeit.tickettakeit.e2e.pages.LoginPage;
import com.tickettakeit.tickettakeit.e2e.pages.UserInfoPage;
import com.tickettakeit.tickettakeit.e2e.pages.UserPage;
import com.tickettakeit.tickettakeit.e2e.pages.UserTicketsPage;

public class UserProfileTest {

	UserInfoPage userInfoPage;
	UserTicketsPage userTicketsPage;
	UserPage userPage;
	LoginPage loginPage;
	WebDriver browser;
	
	//at least 2 reserved ticket and 1 bought
	private String username = "user";
	private String password = "user";
	private String paypalEmail = "sb-qowca942245@personal.example.com";
	private String paypalPassword = "11111111";

	@Before
	public void setupSelenium() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		browser.navigate().to("http://localhost:4200/login");
		//browser.manage().window().setSize(new Dimension(1540, 1090));

		// login
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		loginPage.setDriver(browser);
		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName(username);
		loginPage.setPassword(password);
		loginPage.getLoginButton().click();
		loginPage.ensureSnackBarIsDisplayed();
		loginPage.ensureSnackBarInvisible();

		userPage = PageFactory.initElements(browser, UserPage.class);
		userPage.setDriver(browser);
		userInfoPage = PageFactory.initElements(browser, UserInfoPage.class);
		userInfoPage.setDriver(browser);
		userTicketsPage = PageFactory.initElements(browser, UserTicketsPage.class);
		userTicketsPage.setDriver(browser);
	}

	@Test
	public void editProfile() {

		userInfoPage.ensureEditProfileButtonIsDisplayed();

		// Current password field empty
		userInfoPage.getEditProfileButton().click();
		String text = userInfoPage.ensureErrorMessageIsDisplayed("6");
		assertEquals("Current password is required for changing profile information.", text);
		userInfoPage.ensureSnackBarInvisible();

		// successfully
		userInfoPage.setPassword(password);
		userInfoPage.setFirstName("Mirjana");
		userInfoPage.getEditProfileButton().click();
		userInfoPage.ensureSnackBarIsDisplayed("Successfuly updated.");
		text = userInfoPage.getSnackBar().getText();
		assertEquals("Successfuly updated.", text);
		userInfoPage.ensureSnackBarInvisible();

		// wrong data
		userInfoPage.setFirstName("M@rj%ana");
		userInfoPage.getEditProfileButton().click();
		text = userInfoPage.ensureErrorMessageIsDisplayed("2");
		assertEquals("First Name is required and can contain only letters.", text);
		userInfoPage.setFirstName("Mirjana");

		// change password
		userInfoPage.setNewPassword("new password");
		userInfoPage.setRepeatedPassword("wrong repeated");
		userInfoPage.getEditProfileButton().click();
		userInfoPage.ensureSnackBarIsDisplayed("Password is wrong repeated!");
		text = userInfoPage.getSnackBar().getText();
		assertEquals("Password is wrong repeated!", text);
		userInfoPage.ensureSnackBarInvisible();
	}

	@Test
	public void userTickets_Reserved() {

		userPage.ensureReservedTicketsTabIsDisplayed();
		userPage.getReservedTicketsTab().click();
		
		//cancelTicket
		userPage.ensureReservedTicketsTabIsDisplayed();
		userPage.getReservedTicketsTab().click();
		userTicketsPage.ensureTicketCardIsDisplayed();
		userTicketsPage.ensureCancelTicketButtonIsDisplayed();
		userTicketsPage.getCancelTicketBtn().click();
		userTicketsPage.ensureCancelDialogIsDisplayed();
		userTicketsPage.ensureConfirmCancelTicketButtonIsDisplayed();
		userTicketsPage.getCancelTicketConfirmationBtn().click();
		userInfoPage.ensureSnackBarIsDisplayed("Ticket canceled");
		String text = userInfoPage.getSnackBar().getText();
		assertEquals("Ticket canceled", text);
		userInfoPage.ensureSnackBarInvisible();

		// go to event details
		userTicketsPage.ensureEventDetailsButtonIsDisplayed();
		userTicketsPage.getEventDetailsBtn().click();
		assertEquals(true, browser.getCurrentUrl().contains("http://localhost:4200/show-event/"));

	}

	@Test
	public void userTickets_BuyReservedTicket() {

		userPage.ensureReservedTicketsTabIsDisplayed();
		userPage.getReservedTicketsTab().click();
		userTicketsPage.ensureTicketCardIsDisplayed();
		userPage.ensureReservedTicketsTabIsDisplayed();
		userPage.getReservedTicketsTab().click();
		userTicketsPage.ensureTicketCardIsDisplayed();
		userTicketsPage.ensureBuyTicketButtonIsDisplayed();
		userTicketsPage.getBuyTicketBtn().click();
		userTicketsPage.ensurePayPalNextButtonIsDisplayed();
		userTicketsPage.setPayPalEmail(paypalEmail);
		userTicketsPage.getPayPalNextButton().click();
		userTicketsPage.ensurePayPalLoginButtonIsDisplayed();
		userTicketsPage.setPayPalPassword(paypalPassword);
		userTicketsPage.getPayPalLoginButton().click();
		userTicketsPage.ensurePayPalButtonIsDisplayed();
		userTicketsPage.getPayPalSubmitButton().click();
		userTicketsPage.ensureSnackBarIsDisplayed("Your purchase was successful");
		String text = userInfoPage.getSnackBar().getText();
		assertEquals("Your purchase was successful", text);
	}

	@Test
	public void userTickets_Bought() {

		userPage.ensureBoughtTicketsTabIsDisplayed();
		userPage.getBoughtTicketsTab().click();
		userTicketsPage.ensureBoughtTicketCardIsDisplayed();
		userTicketsPage.ensureEventDetailsButtonIsDisplayed();

		// go to event details
		userTicketsPage.getEventDetailsBtn().click();
		assertEquals(true, browser.getCurrentUrl().contains("http://localhost:4200/show-event/"));
	}

	@After
	public void closeSelenium() {
		// Shutdown the browser
		 browser.quit();
	}

}
