package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddEventPage {
	private WebDriver driver;
	private JavascriptExecutor jsExecutor;
	private WebDriverWait wait;

	@FindBy(id = "name")
	private WebElement name;

	@FindBy(id = "description")
	private WebElement description;

	@FindBy(id = "addDatePicker")
	private WebElement addDatePicker;

	@FindBy(id = "addDateBtn")
	private WebElement addDateBtn;

	@FindBy(id = "salesDatePicker")
	private WebElement salesDatePicker;

	@FindBy(id = "lastDayToPay")
	private WebElement lastDayToPay;

	@FindBy(id = "controllAccessSelect")
	private WebElement controllAccessSelect;

	@FindBy(id = "applicationDatePicker")
	private WebElement applicationDatePicker;

	@FindBy(id = "fileInput")
	private WebElement fileInput;

	@FindBy(id = "fileInputBtn")
	private WebElement fileInputBtn;

	@FindBy(id = "attachmentInput")
	private WebElement attachmentInput;

	@FindBy(id = "attachmentInputBtn")
	private WebElement attachmentInputBtn;

	@FindBy(id = "categorySelect")
	private WebElement categorySelect;

	@FindBy(id = "addCategoryBtn")
	private WebElement addCategoryBtn;

	@FindBy(id = "locationSelect")
	private WebElement locationSelect;

	@FindBy(id = "hallSelect")
	private WebElement hallSelect;

	@FindBy(id = "sectionSelect")
	private WebElement sectionSelect;

	@FindBy(id = "sectionPrice")
	private WebElement sectionPrice;

	@FindBy(id = "addSectionBtn")
	private WebElement addSectionBtn;

	@FindBy(id = "addEventBtn")
	private WebElement addEventBtn;

	@FindBy(className = "cdk-overlay-container")
	private WebElement snackBar;

	@FindBy(className = "success-snack-bar")
	private WebElement successSnackBar;

	@FindBy(className = "add-event-error-snack-bar")
	private WebElement errorSnackBar;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 20);
		this.jsExecutor = (JavascriptExecutor) this.driver;
	}

	public WebElement getName() {
		return name;
	}

	public void setName(String name) {
		this.name.clear();
		this.name.sendKeys(name);
	}

	public WebElement getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description.clear();
		this.description.sendKeys(description);
	}

	public WebElement getAddDatePicker() {
		return addDatePicker;
	}

	public void setAddDatePicker(String addDatePicker) {
		this.addDatePicker.clear();
		this.addDatePicker.sendKeys(addDatePicker);
	}

	public WebElement getAddDateBtn() {
		return addDateBtn;
	}

	public void setAddDateBtn(WebElement addDateBtn) {
		this.addDateBtn = addDateBtn;
	}

	public WebElement getSalesDatePicker() {
		return salesDatePicker;
	}

	public void setSalesDatePicker(String salesDatePicker) {
		this.salesDatePicker.clear();
		this.salesDatePicker.sendKeys(salesDatePicker);
	}

	public WebElement getLastDayToPay() {
		return lastDayToPay;
	}

	public void setLastDayToPay(String lastDayToPay) {
		this.lastDayToPay.clear();
		this.lastDayToPay.sendKeys(lastDayToPay);
	}

	public WebElement getControllAccessSelect() {
		return controllAccessSelect;
	}

	public void setControllAccessSelect(String controllAccessSelect) {
		this.controllAccessSelect.clear();
		this.controllAccessSelect.sendKeys(controllAccessSelect);
	}

	public WebElement getApplicationDatePicker() {
		return applicationDatePicker;
	}

	public void setApplicationDatePicker(String applicationDatePicker) {
		this.applicationDatePicker.clear();
		this.applicationDatePicker.sendKeys(applicationDatePicker);
	}

	public WebElement getFileInput() {
		return fileInput;
	}

	public void setFileInput(String fileInput) {
		this.fileInput.sendKeys(fileInput);
	}

	public WebElement getFileInputBtn() {
		return fileInputBtn;
	}

	public void setFileInputBtn(WebElement fileInputBtn) {
		this.fileInputBtn = fileInputBtn;
	}

	public WebElement getAttachmentInput() {
		return attachmentInput;
	}

	public void setAttachmentInput(String attachmentInput) {
		this.attachmentInput.sendKeys(attachmentInput);
	}

	public WebElement getAttachmentInputBtn() {
		return attachmentInputBtn;
	}

	public void setAttachmentInputBtn(WebElement attachmentInputBtn) {
		this.attachmentInputBtn = attachmentInputBtn;
	}

	public WebElement getCategorySelect() {
		return categorySelect;
	}

	public void setCategorySelect(String categorySelect) {
		this.categorySelect.clear();
		this.categorySelect.sendKeys(categorySelect);
	}

	public WebElement getAddCategoryBtn() {
		return addCategoryBtn;
	}

	public void setAddCategoryBtn(WebElement addCategoryBtn) {
		this.addCategoryBtn = addCategoryBtn;
	}

	public WebElement getLocationSelect() {
		return locationSelect;
	}

	public void setLocationSelect(String locationSelect) {
		this.locationSelect.clear();
		this.locationSelect.sendKeys(locationSelect);
	}

	public WebElement getHallSelect() {
		return hallSelect;
	}

	public void setHallSelect(String hallSelect) {
		this.hallSelect.clear();
		this.hallSelect.sendKeys(hallSelect);
	}

	public WebElement getSectionSelect() {
		return sectionSelect;
	}

	public void setSectionSelect(String sectionSelect) {
		this.sectionSelect.clear();
		this.sectionSelect.sendKeys(sectionSelect);
	}

	public WebElement getSectionPrice() {
		return sectionPrice;
	}

	public void setSectionPrice(String sectionPrice) {
		this.sectionPrice.clear();
		this.sectionPrice.sendKeys(sectionPrice);
	}

	public WebElement getAddSectionBtn() {
		return addSectionBtn;
	}

	public void setAddSectionBtn(WebElement addSectionBtn) {
		this.addSectionBtn = addSectionBtn;
	}

	public WebElement getAddEventBtn() {
		return addEventBtn;
	}

	public void setAddEventBtn(WebElement addEventBtn) {
		this.addEventBtn = addEventBtn;
	}

	public WebElement getSnackBar() {
		return snackBar;
	}

	public void setSnackBar(WebElement snackBar) {
		this.snackBar = snackBar;
	}

	public WebElement getSuccessSnackBar() {
		return successSnackBar;
	}

	public void setSuccessSnackBar(WebElement snackBar) {
		this.successSnackBar = snackBar;
	}

	public WebElement getErrorSnackBar() {
		return errorSnackBar;
	}

	public void setErrorSnackBar(WebElement errorSnackBar) {
		this.errorSnackBar = errorSnackBar;
	}
	// =============== WAIT ======================

	public void ensureAddDateBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(addDateBtn));
	}

	public void ensureFileInputBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(fileInputBtn));
	}

	public void ensureAttachmentInputBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(attachmentInputBtn));
	}

	public void ensureAddCategoryBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(addCategoryBtn));
	}

	public void ensureAddSectionBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(addSectionBtn));
	}

	public void ensureAddEventBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(addEventBtn));
	}

	public void ensureSnackBarIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("cdk-overlay-container")));
	}

	public void ensureHallIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(hallSelect));
	}

	public void ensureSectionIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(sectionSelect));
	}

	public void ensureSnackBarIsInvisible() {
		wait.until(ExpectedConditions.invisibilityOf(snackBar));
	}

	public void ensureSuccessSnackBarIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("success-snack-bar")));
	}

	public void ensureErrorSnackBarIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("add-event-error-snack-bar")));
	}
}
