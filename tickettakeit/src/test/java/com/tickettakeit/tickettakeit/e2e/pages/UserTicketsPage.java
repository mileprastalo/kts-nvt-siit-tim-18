package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserTicketsPage {

	private WebDriver driver;

	@FindBy(id = "400CARD")
	private WebElement ticketCard;
	
	@FindBy(xpath = "//*[@id=\"mat-tab-content-0-2\"]/div/app-user-ticket/div/div[2]/mat-card[1]")
	private WebElement boughtTicketCard;

	@FindBy(id = "eventDetails")
	private WebElement eventDetailsBtn;
	@FindBy(id = "600buy")
	private WebElement buyTicketBtn;
	
	@FindBy(id = "400cancel")
	private WebElement cancelTicketBtn;
	@FindBy(id = "confirmation-dialog")
	private WebElement cancelDialog;
	@FindBy(id = "confirmation-confirm-btn")
	private WebElement cancelTicketConfimationBtn;
	
	@FindBy(className = "cdk-overlay-container")
	private WebElement snackBar;
	
	@FindBy(id = "email")
	private WebElement paypalEmail;
	@FindBy(id = "password")
	private WebElement paypalPassword;
	@FindBy(id = "btnNext")
	private WebElement paypalNextButton;
	@FindBy(id = "btnLogin")
	private WebElement paypalLoginBtn;
	@FindBy(id = "payment-submit-btn")
	private WebElement paypalSubmitButton;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getEventDetailsBtn() {
		return eventDetailsBtn;
	}

	public WebElement getBuyTicketBtn() {
		return buyTicketBtn;
	}

	public WebElement getCancelTicketBtn() {
		return cancelTicketBtn;
	}
	
	public WebElement getCancelTicketConfirmationBtn() {
		return cancelTicketConfimationBtn;
	}
	
	public WebElement getSnackBar() {
		return snackBar;
	}
	
	public void setPayPalEmail(String value) {
		this.paypalEmail.clear();
		this.paypalEmail.sendKeys(value);
	}
	
	public void setPayPalPassword(String value) {
		this.paypalPassword.clear();
		this.paypalPassword.sendKeys(value);
	}
	
	public WebElement getPayPalNextButton() {
		return this.paypalNextButton;
	}
	
	public WebElement getPayPalLoginButton() {
		return this.paypalLoginBtn;
	}

	
	public WebElement getPayPalSubmitButton() {
		return this.paypalSubmitButton;
	}

	public void ensureTicketCardIsDisplayed() {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOf(ticketCard));
	}
	
	public void ensureBoughtTicketCardIsDisplayed() {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOf(boughtTicketCard));
	}

	public void ensureEventDetailsButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(eventDetailsBtn));
	}

	public void ensureBuyTicketButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(buyTicketBtn));
	}

	public void ensureCancelTicketButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(cancelTicketBtn));
	}
	public void ensureCancelDialogIsDisplayed() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(cancelDialog));
	}
	public void ensureConfirmCancelTicketButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(cancelTicketConfimationBtn));
	}
	
	public void ensureSnackBarIsDisplayed(String text) {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.textToBePresentInElement(snackBar, text));
	}

	public void ensureSnackBarInvisible() {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.invisibilityOf(this.snackBar));
	}
	
	public void ensurePayPalButtonIsDisplayed() {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.elementToBeClickable(paypalSubmitButton));
	}
	
	public void ensurePayPalNextButtonIsDisplayed() {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.elementToBeClickable(paypalNextButton));
	}
	public void ensurePayPalLoginButtonIsDisplayed() {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.elementToBeClickable(paypalLoginBtn));
	}
}
