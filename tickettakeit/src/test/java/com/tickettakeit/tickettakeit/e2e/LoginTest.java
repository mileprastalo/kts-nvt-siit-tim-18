package com.tickettakeit.tickettakeit.e2e;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import com.tickettakeit.tickettakeit.e2e.pages.LoginPage;

public class LoginTest {

	LoginPage loginPage;
	WebDriver browser;

	private final String username = "user";
	private final String password = "user";
	private final String adminUsername = "admin";
	private final String adminPassword = "user";
	private final String nonExistingUsername = "xxxx";

	@Before
	public void setupSelenium() {

		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		browser.navigate().to("http://localhost:4200/login");
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		loginPage.setDriver(browser);
	}

	@Test
	public void loginTest_User() {

		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName(username);
		loginPage.setPassword(password);
		loginPage.getLoginButton().click();
		loginPage.ensureSnackBarIsDisplayed("Logged In successfully.");
		loginPage.ensureSnackBarInvisible();
		assertEquals("http://localhost:4200/user-profile", browser.getCurrentUrl());
	}

	@Test
	public void loginTest_Admin() {

		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName(adminUsername);
		loginPage.setPassword(adminPassword);
		loginPage.getLoginButton().click();
		loginPage.ensureSnackBarIsDisplayed("Logged In successfully.");
		loginPage.ensureSnackBarInvisible();
		assertEquals("http://localhost:4200/admin-profile", browser.getCurrentUrl());
	}

	@Test
	public void loginTest_WrongData() {

		loginPage.ensureTableRowIsPresend();

		// empty data
		loginPage.getLoginButton().click();
		String text = loginPage.ensureErrorMessageIsDisplayed("0");
		assertEquals("Username can not be blank.", text);
		text = loginPage.ensureErrorMessageIsDisplayed("1");
		assertEquals("Password can not be blank.", text);

		// NonExiting username
		loginPage.setUserName(nonExistingUsername);
		loginPage.setPassword(password);
		loginPage.getLoginButton().click();
		loginPage.ensureSnackBarIsDisplayed();
		text = loginPage.getSnackBar().getText();
		assertEquals("User with username: " + nonExistingUsername + " doesn't exists.", text);
		assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
	}

	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}
}
