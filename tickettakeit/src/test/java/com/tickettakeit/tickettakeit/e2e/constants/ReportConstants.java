package com.tickettakeit.tickettakeit.e2e.constants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ReportConstants {

	private static final Calendar fromDateDailyCalendar;
	private static final Calendar toDateDailyCalendar;
	private static final Calendar fromDateMonthlyCalendar;
	private static final Calendar toDateMonthlyCalendar;
	private static final SimpleDateFormat sdf;

	static {
		fromDateDailyCalendar = GregorianCalendar.getInstance();
		fromDateDailyCalendar.clear();
		fromDateDailyCalendar.set(2019, 11, 22);
		toDateDailyCalendar = GregorianCalendar.getInstance();
		toDateDailyCalendar.clear();
		toDateDailyCalendar.set(2019, 11, 29);

		fromDateMonthlyCalendar = GregorianCalendar.getInstance();
		fromDateMonthlyCalendar.clear();
		fromDateMonthlyCalendar.set(2019, 9, 1);
		toDateMonthlyCalendar = GregorianCalendar.getInstance();
		toDateMonthlyCalendar.clear();
		toDateMonthlyCalendar.set(2020, 1, 1);
 
		sdf = new SimpleDateFormat("MM/dd/yyyy"); 
	}

	public static final String LOCATION_NAME = "Spens";
	public static final String EVENT_NAME = "Lara Fabian";
	public static final String INVALID_EVENT_NAME = "Invalid name";
	public static final String FROM_DATE_DAILY = sdf.format(fromDateDailyCalendar.getTime());
	public static final String TO_DATE_DAILY = sdf.format(toDateDailyCalendar.getTime());
	public static final String FROM_DATE_MONTHLY = sdf.format(fromDateMonthlyCalendar.getTime());
	public static final String TO_DATE_MONTHLY = sdf.format(toDateMonthlyCalendar.getTime());
	
	public static final String DAILY_REPORT_EARNINGS_IMAGE = ".\\src\\test\\resources\\chart-snapshots\\daily-report-earnings.jpg";
	public static final String DAILY_REPORT_SOLD_TICKETS_IMAGE = ".\\src\\test\\resources\\chart-snapshots\\daily-report-sold-tickets.jpg";
	public static final String MONTHLY_REPORT_EARNINGS_IMAGE = ".\\src\\test\\resources\\chart-snapshots\\monthly-report-earnings.jpg";
	public static final String MONTHLY_REPORT_SOLD_TICKETS_IMAGE = ".\\src\\test\\resources\\chart-snapshots\\monthly-report-sold-tickets.jpg";
	public static final String SNAPSHOT_PATH = ".\\src\\test\\resources";
	public static final String CHART_SCREENSHOT_PATH = "";

}
