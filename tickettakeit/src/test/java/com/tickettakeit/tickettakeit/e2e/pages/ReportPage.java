package com.tickettakeit.tickettakeit.e2e.pages;

import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.CHART_SCREENSHOT_PATH;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.SNAPSHOT_PATH;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.testautomationguru.ocular.Ocular;
import com.testautomationguru.ocular.comparator.OcularResult;

public class ReportPage {

	private WebDriver driver;

	private WebDriverWait wait;

	@FindBy(className = "report-box")
	private WebElement reportBox; 

	@FindBy(id = "location-slide")
	private WebElement locationSlideBtn;

	@FindBy(id = "event-slide")
	private WebElement eventSlideBtn;

	@FindBy(id = "location-input")
	private WebElement locationInput;

	@FindBy(id = "event-input")
	private WebElement eventInput;

	@FindBy(id = "from-date-input")
	private WebElement fromDateInput;

	@FindBy(id = "to-date-input")
	private WebElement toDateInput;

	@FindBy(className = "report-type-radio-button")
	private List<WebElement> reportTypeRadioBtns;

	@FindBy(id = "report-submit-btn")
	private WebElement reportSubmitBtn;

	@FindBy(id = "earnings-chart")
	private WebElement earningsChart;

	@FindBy(id = "earnings-chart-div")
	private WebElement earningsChartDiv;

	@FindBy(id = "sold-tickets-chart")
	private WebElement soldTicketsChart;

	@FindBy(className = "error-snack-bar")
	private WebElement errorSnackBar;

	// ******************* GET AND SET METHODS *************************

	public WebElement getReportBox() {
		return reportBox;
	}

	public void setReportBox(WebElement reportBox) {
		this.reportBox = reportBox;
	}

	public WebElement getLocationSlideBtn() {
		return locationSlideBtn;
	}

	public void setLocationSlideBtn(WebElement locationSlideBtn) {
		this.locationSlideBtn = locationSlideBtn;
	}

	public WebElement getEventSlideBtn() {
		return eventSlideBtn;
	}

	public void setEventSlideBtn(WebElement eventSlideBtn) {
		this.eventSlideBtn = eventSlideBtn;
	}

	public WebElement getLocationInput() {
		return locationInput;
	}

	public void setLocationInput(String locationName) {
		this.locationInput.clear(); 
		this.locationInput.sendKeys(locationName);
	}

	public WebElement getEventInput() {
		return eventInput;
	}

	public void setEventInput(String eventName) {
		this.eventInput.clear();
		this.eventInput.sendKeys(eventName);
	}

	public WebElement getFromDateInput() {
		return fromDateInput;
	}

	public void setFromDateInput(String fromDate) {
		this.fromDateInput.clear();
		this.fromDateInput.sendKeys(fromDate);
	}

	public WebElement getToDateInput() {
		return toDateInput;
	}

	public void setToDateInput(String toDate) {
		this.toDateInput.clear();
		this.toDateInput.sendKeys(toDate);
	}

	public List<WebElement> getReportTypeRadioBtns() {
		return reportTypeRadioBtns;
	}

	public void setReportTypeRadioBtns(List<WebElement> reportTypeRadioBtns) {
		this.reportTypeRadioBtns = reportTypeRadioBtns;
	}

	public WebElement getReportSubmitBtn() {
		return reportSubmitBtn;
	}

	public void setReportSubmitBtn(WebElement reportSubmitBtn) {
		this.reportSubmitBtn = reportSubmitBtn;
	}

	public WebElement getEarningsChart() {
		return earningsChart;
	}

	public void setEarningsChart(WebElement earningsChart) {
		this.earningsChart = earningsChart;
	}

	public WebElement getSoldTicketsChart() {
		return soldTicketsChart;
	}

	public void setSoldTicketsChart(WebElement soldTicketsChart) {
		this.soldTicketsChart = soldTicketsChart;
	}

	public String getEarningsChartLabels() {
		return this.earningsChart.getAttribute("ng-reflect-bar-chart-labels");
	}

	public String getSoldTicketsChartLabels() {
		return this.soldTicketsChart.getAttribute("ng-reflect-bar-chart-labels");
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 20);
	}

	public WebElement getErrorSnackBar() {
		return errorSnackBar;
	}

	public void setErrorSnackBar(WebElement errorSnackBar) {
		this.errorSnackBar = errorSnackBar;
	}

	// ********************** WAITS *****************************
	public void ensureReportBoxIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(reportBox));
	}

	public void ensureLocationSlideBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(locationSlideBtn));
	}

	public void ensureEventSlideBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(eventSlideBtn));
	}

	public void ensureLocationInputIsReady() {
		wait.until(ExpectedConditions.elementToBeClickable(locationInput));
	}

	public void ensureEventInputIsReady() {
		wait.until(ExpectedConditions.elementToBeClickable(eventInput));
	}

	public void ensureReportSubmitBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(reportSubmitBtn));
	}

	public void ensureChartsAreDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(earningsChart));
		wait.until(ExpectedConditions.visibilityOf(soldTicketsChart));
	}

	public void ensureErrorSnackBarIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(errorSnackBar));
	}
	
	
	// ********************** COMPARE CHARTS *****************************
	
	public void takeChartScreenshot(String filename) throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(".\\src\\test\\resources\\"+ filename + ".jpg"));
	}

	public boolean compareCharts(String imageRelativePath, String... screenshotPath) throws InterruptedException, IOException {
		
		// convert relative to absolute path
		String imageAbsolutePath = new File(imageRelativePath).getCanonicalPath(); 
		Path path = Paths.get(imageAbsolutePath);
		
		// scroll to make chart visible
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
		//takeChartScreenshot(screenshotPath[0]);
		
		Ocular.config().resultPath(Paths.get(new File(SNAPSHOT_PATH).getCanonicalPath()))
				       .snapshotPath(Paths.get(new File(SNAPSHOT_PATH).getCanonicalPath()))
				       .globalSimilarity(99)
				       .saveSnapshot(false);

		OcularResult result = Ocular.snapshot()
				                     .from(path)
				                     .sample()
				                     .using(driver)
				                     .compare();

		return result.isEqualsImages(); 

	}
}
