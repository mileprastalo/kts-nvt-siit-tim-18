package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.itextpdf.text.DocumentException;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.tickettakeit.tickettakeit.dto.BuyTicketDTO;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationHasPassed;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFoundForGivenUser;
import com.tickettakeit.tickettakeit.exceptions.EventIsBlockedException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionDoesNotBelongsToTheEventException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.GivenDateDoesntExistException;
import com.tickettakeit.tickettakeit.exceptions.NoLoggedUserException;
import com.tickettakeit.tickettakeit.exceptions.NoTicketsLeftException;
import com.tickettakeit.tickettakeit.exceptions.NotYourTimeYet;
import com.tickettakeit.tickettakeit.exceptions.ReservationNotAvailableException;
import com.tickettakeit.tickettakeit.exceptions.SaleHasNotYetBeganException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsBeyondBoundariesException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.TicketIsBoughtException;
import com.tickettakeit.tickettakeit.exceptions.TicketNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserDoesNotHaveTheTicketException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.RegisteredUser;
import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.repository.TicketRepository;

@Transactional
@Rollback
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TicketServiceUnitTest {

	@SpyBean
	TicketService ticketService;

	@MockBean
	TicketRepository ticketRepository;

	@MockBean
	EventSectionService eventSectionService;

	@MockBean
	EventService eventService;

	@MockBean
	UserService userService;

	@MockBean
	EmailService emailService;
	
	@MockBean
	PayPalService paypalService;

	// Data
	private RegisteredUser loggedUser;
	private EventSection eventSection;
	private Event event;
	private Ticket ticket;
	private List<Long> ticketIds;
	private BuyTicketDTO btDTO;
	private Date today;
	private ArgumentCaptor<Ticket> argument;

	@Before
	public void setUp() throws MalformedURLException, DocumentException, IOException, PayPalRESTException {
		today = new Date();

		loggedUser = new RegisteredUser(10L, "user", "user", "user@mail.com", "414241", "User", "Useric");
		Mockito.doReturn(loggedUser).when(ticketService).getLoggedUser();

		eventSection = new EventSection(10L, "Event Section 1", 10, 10, true, 100, 100);
		Mockito.when(eventSectionService.findByIdOptional(10L)).thenReturn(Optional.of(eventSection));

		event = new Event();
		event.setId(10L);
		event.setEventSections(new HashSet<EventSection>());
		event.getEventSections().add(eventSection);
		event.setStatus(EventStatus.NORMAL);
		event.setSalesDate(new Date(today.getTime() - 5 * (1000 * 60 * 60 * 24)));
		event.setDate(new HashSet<Date>());
		event.getDate().add(new Date(today.getTime() + 10 * (1000 * 60 * 60 * 24)));
		event.getDate().add(new Date(today.getTime() + 11 * (1000 * 60 * 60 * 24)));
		event.getDate().add(new Date(today.getTime() + 12 * (1000 * 60 * 60 * 24)));
		Mockito.when(eventService.findByID(10L)).thenReturn(Optional.of(event));

		btDTO = new BuyTicketDTO(5, 5, today.getTime() + 11 * (1000 * 60 * 60 * 24), eventSection.getId(), 10L);

		// Mocks generateTicket method.
		Mockito.doReturn("Generated Ticket.").when(ticketService).generateTicket(Mockito.any());

		// Mocks emailService sendTicket method.
		Mockito.doNothing().when(emailService).sendTicket(Mockito.any(Ticket.class), ArgumentMatchers.<String>anyList());
		argument = ArgumentCaptor.forClass(Ticket.class);
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("status", "success");
		response.put("redirect_url", "some_url");
		Payment payment = new Payment();
		Mockito.doReturn(response).when(paypalService).createPayPalPayment(Mockito.any(Payment.class));
		Mockito.doReturn(payment).when(paypalService).createPaymentObjectForTickets( ArgumentMatchers.<Ticket>anyList(), Mockito.anyString(), Mockito.anyString());

		// Sets reserved Ticket for BuyReservedTicket
		ticket = new Ticket();
		ticket.setId(20L);
		ticket.setCode("10 20 ");
		ticket.setDayOfPurchase(new Date(today.getTime() - 2 * (1000 * 60 * 60 * 24)));
		ticket.setBought(false);
		ticket.setRow(6);
		ticket.setColumn(6);
		ticket.setPrice(eventSection.getPrice());
		ticket.setForDay(new Date(today.getTime() + 12 * (1000 * 60 * 60 * 24)));
		ticket.setEventSection(eventSection);
		ticket.setEvent(event);
		ticket.setUser(loggedUser);
		
		ticketIds = new ArrayList<>();
		ticketIds.add(ticket.getId());
		loggedUser.getTickets().add(ticket);
		Mockito.when(ticketRepository.findById(ticket.getId())).thenReturn(Optional.of(ticket));
	}

	// ===============================BuyTicket================================

	@Test
	public void whenSuccessfullyBuyTicket_thenReturnBoughtTicket() throws MalformedURLException, DocumentException,
			IOException, NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException, EventIsBlockedException,
			SaleHasNotYetBeganException, SalesDateHasPassedException, GivenDateDoesntExistException,
			EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		
		Ticket boughtTicket = ticketService.buyTicket(btDTO);

		// Checks if method has passed.
		verify(ticketRepository, times(1)).save(argument.capture());

		// Gets sent Ticket to database.
		boughtTicket = argument.getValue();

		assertEquals("Assert barcode: ", "10 10 " + boughtTicket.getDayOfPurchase(), boughtTicket.getCode());
		assertEquals("Assert Ticket bought for day: ", today.getTime() + 11 * (1000 * 60 * 60 * 24),
				boughtTicket.getForDay().getTime());
		assertTrue("Assert Ticket is bought: ", boughtTicket.isBought());
		assertEquals("Assert row number: ", 5, boughtTicket.getRow());
		assertEquals("Assert column number: ", 5, boughtTicket.getColumn());
		assertEquals(eventSection.getPrice(), boughtTicket.getPrice(), 1);
		assertEquals("Assert EventSection id: ", eventSection.getId(), boughtTicket.getEventSection().getId());
		assertEquals("Assert Event id: ", event.getId(), boughtTicket.getEvent().getId());
		assertEquals("Assert User id: ", loggedUser.getId(), boughtTicket.getUser().getId());
	}

	@Test(expected = NoLoggedUserException.class)
	public void whenBuyTicketLoggedUserNotFound_thenThrowNoLoggedUserException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets logged user to null.
		Mockito.doReturn(null).when(ticketService).getLoggedUser();

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = EventSectionNotExistsException.class)
	public void whenBuyTicketEventSectionNotFound_thenThrowEventSectionNotExistsException()
			throws MalformedURLException, DocumentException, IOException, NoLoggedUserException,
			EventSectionNotExistsException, EventNotExistsException, SeatIsTakenException,
			SeatIsBeyondBoundariesException, NoTicketsLeftException, EventIsBlockedException,
			SaleHasNotYetBeganException, SalesDateHasPassedException, GivenDateDoesntExistException,
			EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets EventSection that doesn't exist in database.
		btDTO.setEventSectionID(11L);

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = EventNotExistsException.class)
	public void whenBuyTicketEventNotFound_thenThrowEventNotExistsException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets Event that doesn't exist in database.
		btDTO.setEventID(11L);

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = EventSectionDoesNotBelongsToTheEventException.class)
	public void whenBuyTicketEventSectionDoesNotBelongToTheEvent_thenThrowEventSectionDoesNotBelongsToTheEventException()
			throws MalformedURLException, DocumentException, IOException, NoLoggedUserException,
			EventSectionNotExistsException, EventNotExistsException, SeatIsTakenException,
			SeatIsBeyondBoundariesException, NoTicketsLeftException, EventIsBlockedException,
			SaleHasNotYetBeganException, SalesDateHasPassedException, GivenDateDoesntExistException,
			EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets EventSection that doesn't exist in database.
		EventSection eventSection2 = new EventSection(11L, "Event Section 1", 10, 10, true, 100, 100);
		btDTO.setEventSectionID(11L);
		Mockito.when(eventSectionService.findByIdOptional(11L)).thenReturn(Optional.of(eventSection2));

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = EventIsBlockedException.class)
	public void whenBuyTicketEventIsBlocked_thenThrowEventIsBlockedException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets Event status to BLOCKED.
		event.setStatus(EventStatus.BLOCKED);

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = GivenDateDoesntExistException.class)
	public void whenBuyTicketEventDateDoesNotBelongToEvent_thenThrowGivenDateDoesntExistException()
			throws MalformedURLException, DocumentException, IOException, NoLoggedUserException,
			EventSectionNotExistsException, EventNotExistsException, SeatIsTakenException,
			SeatIsBeyondBoundariesException, NoTicketsLeftException, EventIsBlockedException,
			SaleHasNotYetBeganException, SalesDateHasPassedException, GivenDateDoesntExistException,
			EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets Event Date that doesn't exist in the Event.
		btDTO.setForDay(today.getTime() + 9 * (1000 * 60 * 60 * 24));

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = SaleHasNotYetBeganException.class)
	public void whenBuyTicketBeforeTheSaleBegan_thenThrowSaleHasNotYetBeganException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets Event sales date after today.
		event.setSalesDate(new Date(today.getTime() + 1 * (1000 * 60 * 60 * 24)));

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = SalesDateHasPassedException.class)
	public void whenBuyTicketAfterSaleHasPassed_thenThrowSalesDateHasPassedException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets Event last day to pay before today.
		event.setLastDayToPay(20);

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = SeatIsTakenException.class)
	public void whenBuyTicketSeatIsTaken_thenThrowSeatIsTakenException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Mocks second Ticket with same seat.
		Ticket ticket2 = new Ticket();
		ticket2.setId(11L);
		ticket2.setRow(5);
		ticket2.setColumn(5);
		ticket2.setEventSection(eventSection);	
		Mockito.when(ticketRepository.findTicketByDateRowColumnEventSectionID(new Date(btDTO.getForDay()), eventSection.getId(), 5, 5)).thenReturn(ticket2);

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = SeatIsBeyondBoundariesException.class)
	public void whenBuyTicketRowOutOfBoundaries_thenThrowSeatIsBeyondBoundariesException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets Ticket row out of boundaries(10).
		btDTO.setRow(11);

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = SeatIsBeyondBoundariesException.class)
	public void whenBuyTicketColumnOutOfBoundaries_thenThrowSeatIsBeyondBoundariesException()
			throws MalformedURLException, DocumentException, IOException, NoLoggedUserException,
			EventSectionNotExistsException, EventNotExistsException, SeatIsTakenException,
			SeatIsBeyondBoundariesException, NoTicketsLeftException, EventIsBlockedException,
			SaleHasNotYetBeganException, SalesDateHasPassedException, GivenDateDoesntExistException,
			EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets Ticket column out of boundaries(10).
		btDTO.setColumn(11);

		ticketService.buyTicket(btDTO);
	}

	@Test(expected = NoTicketsLeftException.class)
	public void whenBuyTicketAllTicketsSold_thenThrowNoTicketsLeftExceptionException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, EventSectionNotExistsException,
			EventNotExistsException, SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException,
			EventIsBlockedException, SaleHasNotYetBeganException, SalesDateHasPassedException,
			GivenDateDoesntExistException, EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {
		// Sets EventSection enumerated to false and number of seats to 100.
		eventSection.setNumerated(false);
		eventSection.setNumberOfSeats(100);

		Mockito.doReturn(100).when(ticketService).countSoldTickets(10L);

		ticketService.buyTicket(btDTO);
	}

	// ======================ReserveTicket=================================

	@Test
	public void whenSuccessfullyReserveTicket_thenReturnReservedTicket()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		Ticket reservedTicket = ticketService.reserveTicket(btDTO);

		// Checks if method has passed.
		verify(ticketRepository, times(1)).save(argument.capture());

		// Gets sent Ticket to database.
		reservedTicket = argument.getValue();

		assertEquals("Assert barcode: ", "10 10 " + reservedTicket.getDayOfPurchase(), reservedTicket.getCode());
		assertEquals("Assert Ticket bought for day: ", today.getTime() + 11 * (1000 * 60 * 60 * 24),
				reservedTicket.getForDay().getTime());
		assertFalse("Assert Ticket is bought: ", reservedTicket.isBought());
		assertEquals("Assert row number: ", 5, reservedTicket.getRow());
		assertEquals("Assert column number: ", 5, reservedTicket.getColumn());
		assertEquals(eventSection.getPrice(), reservedTicket.getPrice(), 1);
		assertEquals("Assert EventSection id: ", eventSection.getId(), reservedTicket.getEventSection().getId());
		assertEquals("Assert Event id: ", event.getId(), reservedTicket.getEvent().getId());
		assertEquals("Assert User id: ", loggedUser.getId(), reservedTicket.getUser().getId());
	}

	@Test(expected = NoLoggedUserException.class)
	public void whenReserveTicketLoggedUserNotFound_thenThrowNoLoggedUserException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets logged user to null.
		Mockito.doReturn(null).when(ticketService).getLoggedUser();

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = EventSectionNotExistsException.class)
	public void whenReserveTicketEventSectionNotFound_thenThrowEventSectionNotExistsException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets EventSection that doesn't exist in database.
		btDTO.setEventSectionID(11L);

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = EventNotExistsException.class)
	public void whenReserveTicketEventNotFound_thenThrowEventNotExistsException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets Event that doesn't exist in database.
		btDTO.setEventID(11L);

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = EventSectionDoesNotBelongsToTheEventException.class)
	public void whenReserveTicketEventSectionDoesNotBelongToTheEvent_thenThrowEventSectionDoesNotBelongsToTheEventException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets EventSection that doesn't exist in database.
		EventSection eventSection2 = new EventSection(11L, "Event Section 1", 10, 10, true, 100, 100);
		btDTO.setEventSectionID(11L);
		Mockito.when(eventSectionService.findByIdOptional(11L)).thenReturn(Optional.of(eventSection2));

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = EventIsBlockedException.class)
	public void whenReserveTicketEventIsBlocked_thenThrowEventIsBlockedException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets Event status to BLOCKED.
		event.setStatus(EventStatus.BLOCKED);

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = GivenDateDoesntExistException.class)
	public void whenReserveTicketEventDateDoesNotBelongToEvent_thenThrowGivenDateDoesntExistException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets Event Date that doesn't exist in the Event.
		btDTO.setForDay(today.getTime() + 9 * (1000 * 60 * 60 * 24));

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = SaleHasNotYetBeganException.class)
	public void whenReserveTicketBeforeTheSaleBegan_thenThrowSaleHasNotYetBeganException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets Event sales date after today.
		event.setSalesDate(new Date(today.getTime() + 1 * (1000 * 60 * 60 * 24)));

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = SalesDateHasPassedException.class)
	public void whenReserveTicketAfterSaleHasPassed_thenThrowSalesDateHasPassedException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets Event last day to pay before today.
		event.setLastDayToPay(20);

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = SeatIsTakenException.class)
	public void whenReserveTicketSeatIsTaken_thenThrowSeatIsTakenException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Mocks second Ticket with same seat.
		Ticket ticket2 = new Ticket();
		ticket2.setId(11L);
		ticket2.setRow(5);
		ticket2.setColumn(5);
		ticket2.setEventSection(eventSection);
		Mockito.when(ticketRepository.findTicketByDateRowColumnEventSectionID(new Date(btDTO.getForDay()), eventSection.getId(), 5, 5)).thenReturn(ticket2);

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = SeatIsBeyondBoundariesException.class)
	public void whenReserveTicketRowOutOfBoundaries_thenThrowSeatIsBeyondBoundariesException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets Ticket row out of boundaries(10).
		btDTO.setRow(11);

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = SeatIsBeyondBoundariesException.class)
	public void whenReserveTicketColumnOutOfBoundaries_thenThrowSeatIsBeyondBoundariesException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets Ticket column out of boundaries(10).
		btDTO.setColumn(11);

		ticketService.reserveTicket(btDTO);
	}

	@Test(expected = NoTicketsLeftException.class)
	public void whenReserveTicketAllTicketsSold_thenThrowNoTicketsLeftExceptionException()
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {
		// Sets EventSection enumerated to false and number of seats to 100.
		eventSection.setNumerated(false);
		eventSection.setNumberOfSeats(100);

		Mockito.doReturn(100).when(ticketService).countSoldTickets(10L);

		ticketService.reserveTicket(btDTO);
	}

	// ========================BuyReservedTicket================================

	@Test
	public void whenSuccessfullyBuyReservedTicket_thenReturnTrue() throws MalformedURLException, DocumentException,
			IOException, NoLoggedUserException, TicketNotExistsException, EventIsBlockedException,
			SalesDateHasPassedException, SaleHasNotYetBeganException, UserDoesNotHaveTheTicketException, PayPalRESTException {
		
		Map<String, Object> reserved = ticketService.buyReservedTicketCreatePayment(ticketIds);

		assertEquals("success",reserved.get("status"));
	}

	@Test(expected = NoLoggedUserException.class)
	public void whenBuyReservedTicketLoggedUserNotFound_thenThrowNoLoggedUserException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, TicketNotExistsException, EventIsBlockedException,
			SalesDateHasPassedException, SaleHasNotYetBeganException, UserDoesNotHaveTheTicketException, PayPalRESTException {
		// Sets logged user to null.
		Mockito.doReturn(null).when(ticketService).getLoggedUser();
		
		ticketService.buyReservedTicketCreatePayment(ticketIds);
	}

	@Test(expected = TicketNotExistsException.class)
	public void whenBuyReservedTicketMissingTicket_thenThrowTicketNotExistsException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, TicketNotExistsException, EventIsBlockedException,
			SalesDateHasPassedException, SaleHasNotYetBeganException, UserDoesNotHaveTheTicketException, PayPalRESTException {
		// Sets Ticket id to non existing one.
		List<Long> ids = new ArrayList<>();
		ids.add(1111L);
		
		ticketService.buyReservedTicketCreatePayment(ids);
	}

	@Test(expected = EventIsBlockedException.class)
	public void whenBuyReservedTicketEventIsBlocked_thenThrowEventIsBlockedException() throws MalformedURLException,
			DocumentException, IOException, NoLoggedUserException, TicketNotExistsException, EventIsBlockedException,
			SalesDateHasPassedException, SaleHasNotYetBeganException, UserDoesNotHaveTheTicketException, PayPalRESTException {
		// Sets Event status to BLOCKED.
		event.setStatus(EventStatus.BLOCKED);

		ticketService.buyReservedTicketCreatePayment(ticketIds);
	}

	@Test(expected = SaleHasNotYetBeganException.class)
	public void whenBuyReservedTicketBeforeTheSaleBegan_thenThrowSaleHasNotYetBeganException()
			throws MalformedURLException, DocumentException, IOException, NoLoggedUserException,
			TicketNotExistsException, EventIsBlockedException, SalesDateHasPassedException, SaleHasNotYetBeganException,
			UserDoesNotHaveTheTicketException, PayPalRESTException {
		// Sets Event sales date after today.
		event.setSalesDate(new Date(today.getTime() + 1 * (1000 * 60 * 60 * 24)));

		ticketService.buyReservedTicketCreatePayment(ticketIds);
	}

	@Test(expected = SalesDateHasPassedException.class)
	public void whenBuyReservedTicketAfterSaleHasPassed_thenThrowSalesDateHasPassedException()
			throws MalformedURLException, DocumentException, IOException, NoLoggedUserException,
			TicketNotExistsException, EventIsBlockedException, SalesDateHasPassedException, SaleHasNotYetBeganException,
			UserDoesNotHaveTheTicketException, PayPalRESTException {
		// Sets Event last day to pay before today.
		event.setLastDayToPay(20);

		ticketService.buyReservedTicketCreatePayment(ticketIds);
	}

	@Test(expected = UserDoesNotHaveTheTicketException.class)
	public void whenBuyReservedTicketUserMissingTicket_thenThrowUserDoesNotHaveTheTicketException()
			throws MalformedURLException, DocumentException, IOException, NoLoggedUserException,
			TicketNotExistsException, EventIsBlockedException, SalesDateHasPassedException, SaleHasNotYetBeganException,
			UserDoesNotHaveTheTicketException, PayPalRESTException {
		// Removes User's Ticket.
		loggedUser.getTickets().remove(ticket);

		ticketService.buyReservedTicketCreatePayment(ticketIds);
	}

	// ========================CancerReservedTicket================================

	@Test
	public void whenCancelReservedTicket_thenReturnTrue()
			throws NoLoggedUserException, TicketNotExistsException, TicketIsBoughtException {

		// Logged user has reserved ticket
		loggedUser.getTickets().add(ticket);
		Mockito.doReturn(loggedUser).when(ticketService).getLoggedUser();
		Mockito.doReturn(ticket).when(ticketService).findByID(ticket.getId());
		boolean success = ticketService.cancelTicketReservation(ticket.getId());
		assertTrue(success);
		assertEquals(false, loggedUser.getTickets().contains(ticket));
	}
	
	@Test(expected = TicketNotExistsException.class)
	public void whenCancelNonExistingReservedTicket_thenThrowTicketNotExistsExeption()
			throws NoLoggedUserException, TicketNotExistsException, TicketIsBoughtException {

		Mockito.doReturn(Optional.ofNullable(null)).when(ticketRepository).findById(ticket.getId());
		ticketService.cancelTicketReservation(ticket.getId());
	}
	
	@Test(expected = NoLoggedUserException.class)
	public void whenCancelReservedTicketLoggedUserNotFound_thenThrowNoLoggedUserException()
			throws NoLoggedUserException, TicketNotExistsException, TicketIsBoughtException {

		Mockito.doReturn(null).when(ticketService).getLoggedUser();
		ticketService.cancelTicketReservation(ticket.getId());
	}
	
	@Test(expected = TicketIsBoughtException.class)
	public void whenCancelReservedTicket_TicketIsBought_thenThrowTicketIsBoughtException()
			throws NoLoggedUserException, TicketNotExistsException, TicketIsBoughtException {

		ticket.setBought(true);
		Mockito.doReturn(ticket).when(ticketService).findByID(ticket.getId());
		ticketService.cancelTicketReservation(ticket.getId());
		//return
		ticket.setBought(false);
	}
}
