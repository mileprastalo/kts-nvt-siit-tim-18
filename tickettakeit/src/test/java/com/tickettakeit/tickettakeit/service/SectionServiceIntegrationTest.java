package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.SectionDTO;
import com.tickettakeit.tickettakeit.exceptions.SectionAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionCreatingException;
import com.tickettakeit.tickettakeit.exceptions.SectionDeleteException;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionUpdateException;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Section;
import com.tickettakeit.tickettakeit.repository.SectionRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test-mile.properties")
@Transactional
@Rollback(value = true)public class SectionServiceIntegrationTest {
	@Autowired
	private SectionRepository repository;
	@Autowired
	private SectionService service;
	@Autowired
	private HallService hallService;

	@Test
	public void whencreateSection_thenCreateSection() throws SectionAlreadyExistsException, SectionCreatingException {
		SectionDTO dto = new SectionDTO(null, "Section", 10, 10, false, 100, true);
		Section section = service.createSection(10l, dto);
		assertEquals("Section", section.getName());
	}

	@Test(expected = SectionAlreadyExistsException.class)
	public void whenCreateSection_SectionExists() throws SectionAlreadyExistsException, SectionCreatingException {
		SectionDTO dto = new SectionDTO(null, "east", 10, 10, false, 100, true);
		Section section = service.createSection(10l, dto);
		assertEquals("east", section.getName());
	}

	@Test
	public void whenUpdateSection_thenUpdate() throws SectionNotExistsException, SectionUpdateException {
		SectionDTO dto = new SectionDTO(null, "Section", 10, 10, false, 100, true);
		Section section = service.updateSection(10l, dto);
		assertEquals("Section", repository.findById(10l).get().getName());
	}

	@Test(expected = SectionNotExistsException.class)
	public void whenUpdateSection_thenNotExist() throws SectionNotExistsException, SectionUpdateException {
		SectionDTO dto = new SectionDTO(null, "Section", 10, 10, false, 100, true);
		Section section = service.updateSection(5l, dto);
		assertEquals("Section", repository.findById(10l).get().getName());
	}
	
	@Test(expected = SectionNotExistsException.class)
	public void whenDeleteSection_thenNotExist() throws SectionNotExistsException, SectionUpdateException, SectionDeleteException {
		Section section = service.remove(55l);
	}
	@Test(expected = SectionDeleteException.class)
	public void whenDeleteSection_thenHasEvent() throws SectionNotExistsException, SectionUpdateException, SectionDeleteException {
		Section section = service.remove(22l);
	}
	@Test
	public void whenDeleteSection_thendelete() throws SectionNotExistsException, SectionUpdateException, SectionDeleteException {
		Section section = service.remove(10l);
		assertFalse(section.getActive());
	}

}
