package com.tickettakeit.tickettakeit.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.ReportDTO;
import com.tickettakeit.tickettakeit.dto.ReportDataDTO;
import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.model.REPORT_TYPE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:ana-test.properties")
public class ReportServiceIntegrationTest {

	@Autowired
	private ReportService reportService;

	private final Date fromDate = new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime();
	private final Date toDate = new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime();

	// ***** createReportData *****
	@Test
	@Transactional
	public void createReportData_ForLocation_Success() {

		ReportDataDTO dto = reportService.createReportData(100L, null, null, fromDate, toDate);

		assertNotNull(dto);
		assertEquals(1, dto.getSoldTickets());
		assertEquals(110, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForLocation_NoTickets() {

		ReportDataDTO dto = reportService.createReportData(103L, null, null, fromDate, toDate);

		assertNotNull(dto);
		assertEquals(0, dto.getSoldTickets());
		assertEquals(0, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForEvent_Success() {

		ReportDataDTO dto = reportService.createReportData(null, 100L, null, fromDate, toDate);

		assertNotNull(dto);
		assertEquals(2, dto.getSoldTickets());
		assertEquals(220, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForEvent_NoTickets() {

		ReportDataDTO dto = reportService.createReportData(null, 103L, null, fromDate, toDate);

		assertNotNull(dto);
		assertEquals(0, dto.getSoldTickets());
		assertEquals(0, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForEventDay_Success() {

		ReportDataDTO dto = reportService.createReportData(null, 100L,
				new GregorianCalendar(2020, Calendar.DECEMBER, 6, 20, 30).getTime(), fromDate, toDate);

		assertNotNull(dto);
		assertEquals(1, dto.getSoldTickets());
		assertEquals(100, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForEventDay_NoTickets() {

		ReportDataDTO dto = reportService.createReportData(null, 100L,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 22).getTime(), fromDate, toDate);

		assertNotNull(dto);
		assertEquals(0, dto.getSoldTickets());
		assertEquals(0, dto.getEarnings(), 0);

	}

	// **** createDailyReport *****
	@Test
	@Transactional
	public void createDailyReport_Success() {

		ArrayList<ReportDataDTO> dtos = reportService.dailyReport(null, 100L, null,
				new GregorianCalendar(2019, Calendar.OCTOBER, 10).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 17).getTime());

		assertNotNull(dtos);
		assertEquals(7, dtos.size());
		assertEquals(1, dtos.get(0).getSoldTickets());
		assertEquals(100, dtos.get(0).getEarnings(), 0);
		dtos.remove(0);
		assertThat(dtos).hasSize(6).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	@Test
	@Transactional
	public void createDailyReport_NoTickets() {

		ArrayList<ReportDataDTO> dtos = reportService.dailyReport(null, 100L, null,
				new GregorianCalendar(2019, Calendar.MARCH, 20).getTime(),
				new GregorianCalendar(2019, Calendar.MARCH, 27).getTime());

		assertNotNull(dtos);
		assertThat(dtos).hasSize(7).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// **** createWeeklyReport *****
	@Test
	@Transactional
	public void createWeeklyReport_Success() {

		ArrayList<ReportDataDTO> dtos = reportService.weeklyReport(null, 100L, null,
				new GregorianCalendar(2019, Calendar.OCTOBER, 10).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 17).getTime());

		assertNotNull(dtos);
		assertEquals(2, dtos.size());
		assertEquals(1, dtos.get(0).getSoldTickets());
		assertEquals(100, dtos.get(0).getEarnings(), 0);
		dtos.remove(0);
		assertThat(dtos).hasSize(1).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	@Test
	@Transactional
	public void createWeeklyReport_NoTickets() {

		ArrayList<ReportDataDTO> dtos = reportService.weeklyReport(null, 100L, null,
				new GregorianCalendar(2019, Calendar.MARCH, 20).getTime(),
				new GregorianCalendar(2019, Calendar.MARCH, 27).getTime());

		assertNotNull(dtos);
		assertThat(dtos).hasSize(2).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// **** createMonthlyReport *****
	@Test
	@Transactional
	public void createMonthlyReport_Success_OneMonth() {

		ArrayList<ReportDataDTO> dtos = reportService.monthlyReport(null, 100L, null,
				new GregorianCalendar(2019, Calendar.OCTOBER, 1).getTime(),
				new GregorianCalendar(2019, Calendar.NOVEMBER, 1).getTime());

		assertNotNull(dtos);
		assertEquals(1, dtos.size());
		assertEquals(2, dtos.get(0).getSoldTickets());
		assertEquals(220, dtos.get(0).getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createMonthlyReport_Success_ThreeMonths() {

		ArrayList<ReportDataDTO> dtos = reportService.monthlyReport(null, 100L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 1).getTime(),
				new GregorianCalendar(2019, Calendar.DECEMBER, 1).getTime());

		assertNotNull(dtos);
		assertEquals(3, dtos.size());
		assertEquals(0, dtos.get(0).getSoldTickets());
		assertEquals(0, dtos.get(0).getEarnings(), 0);
		assertEquals(2, dtos.get(1).getSoldTickets());
		assertEquals(220, dtos.get(1).getEarnings(), 0);
		assertEquals(0, dtos.get(2).getSoldTickets());
		assertEquals(0, dtos.get(2).getEarnings(), 0);

	}

	// TODO Granicni slucajevi sa datumima

	@Test
	@Transactional
	public void createMonthlyReport_NoTickets() {

		ArrayList<ReportDataDTO> dtos = reportService.monthlyReport(null, 100L, null,
				new GregorianCalendar(2019, Calendar.FEBRUARY, 1).getTime(),
				new GregorianCalendar(2019, Calendar.MARCH, 1).getTime());

		assertNotNull(dtos);
		assertThat(dtos).hasSize(1).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// ***** createReports *****

	// daily report
	@Test
	@Transactional
	public void createReports_Daily_Success() throws InvalidDateException {

		ReportDTO reportDto = new ReportDTO(null, 100L, null,
				new GregorianCalendar(2019, Calendar.OCTOBER, 10).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 17).getTime(), REPORT_TYPE.DAILY, null);

		ArrayList<ReportDataDTO> result = reportService.createReports(reportDto);

		assertNotNull(result);
		assertEquals(7, result.size());
		assertEquals(1, result.get(0).getSoldTickets());
		assertEquals(100, result.get(0).getEarnings(), 0);
		result.remove(0);
		assertThat(result).hasSize(6).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// weekly report
	@Test
	@Transactional
	public void createReports_Weekly_Success() throws InvalidDateException {

		ReportDTO reportDto = new ReportDTO(null, 100L, null,
				new GregorianCalendar(2019, Calendar.OCTOBER, 10).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 17).getTime(), REPORT_TYPE.WEEKLY, null);

		ArrayList<ReportDataDTO> result = reportService.createReports(reportDto);

		assertNotNull(result);
		assertEquals(2, result.size());
		assertEquals(1, result.get(0).getSoldTickets());
		assertEquals(100, result.get(0).getEarnings(), 0);
		result.remove(0);
		assertThat(result).hasSize(1).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// monthly report
	@Test
	@Transactional
	public void createReports_Monthly_Success() throws InvalidDateException {
		ReportDTO reportDto = new ReportDTO(null, 100L, null,
				new GregorianCalendar(2019, Calendar.OCTOBER, 1).getTime(),
				new GregorianCalendar(2019, Calendar.NOVEMBER, 1).getTime(), REPORT_TYPE.MONTHLY, null);

		ArrayList<ReportDataDTO> result = reportService.createReports(reportDto);

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(2, result.get(0).getSoldTickets());
		assertEquals(220, result.get(0).getEarnings(), 0);

	}
	
	// invalid dates -> fromDate after toDate
	@Test(expected = InvalidDateException.class)
	@Transactional
	public void createReports_fromDateAfterToDate() throws InvalidDateException {

		ReportDTO reportDto = new ReportDTO(null, 100L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 27).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(), REPORT_TYPE.DAILY, null);

		reportService.createReports(reportDto);

	}

}
