package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.converter.HallConverter;
import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.dto.SectionDTO;
import com.tickettakeit.tickettakeit.exceptions.HallAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallCreatingException;
import com.tickettakeit.tickettakeit.exceptions.HallDeleteException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallUpdateException;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.repository.HallRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test-mile.properties")
@Transactional
@Rollback(value = true)
public class HallServiceIntegrationTest {
	@Autowired
	HallService service;
	@Autowired
	LocationService locationService;
	@Autowired
	HallRepository repository;


	@Test
	public void whencreateHall_thenCreateHall()
			throws LocationNotExistsException, HallAlreadyExistsException, HallCreatingException {
		HallDTO dto = new HallDTO();
		dto.setId(null);
		dto.setName("Hall name");
		dto.setSections(new ArrayList<SectionDTO>());
		Hall hall = service.createHall(10l, dto);
		assertEquals(hall.getName(), "Hall name");
	}

	@Test(expected = LocationNotExistsException.class)
	public void whencreateHall_thenLocationNotExist()
			throws LocationNotExistsException, HallAlreadyExistsException, HallCreatingException {
		HallDTO dto = new HallDTO();
		dto.setId(null);
		dto.setName("Hall name");
		dto.setSections(new ArrayList<SectionDTO>());
		Hall hall = service.createHall(211l, dto);
	}

	@Test(expected = HallAlreadyExistsException.class)
	public void whencreateHall_thenHallAlreadyExist()
			throws LocationNotExistsException, HallAlreadyExistsException, HallCreatingException {
		HallDTO dto = new HallDTO();
		dto.setId(null);
		dto.setName("Hall1");
		dto.setSections(new ArrayList<SectionDTO>());
		Hall hall = service.createHall(10l, dto);
	}

	@Test
	public void whenUpdateHall_thenUpdateHall() throws HallNotExistsException, HallUpdateException {
		HallDTO dto = new HallDTO();
		dto.setName("Hall2");
		dto.setSections(new ArrayList<SectionDTO>());
		Hall hall = service.updateHall(20l, 20l, dto);
		assertEquals("Hall2", hall.getName());
	}
	
	@Test(expected = HallNotExistsException.class)
	public void whenUpdateHall_thenHallNotExists() throws HallNotExistsException, HallUpdateException {
		HallDTO dto = new HallDTO();
		dto.setName("Hall2");
		dto.setSections(new ArrayList<SectionDTO>());
		Hall hall = service.updateHall(20l, 55l, dto);
		assertEquals("Hall2", hall.getName());
	}
	
	@Test
	public void whenDeleteHall_deleteHall() throws HallNotExistsException, LocationNotExistsException, HallDeleteException {
		Hall hall = service.deleteHall(21l, 21l);
		assertFalse(hall.getActive());
	}
	@Test(expected = HallNotExistsException.class)
	public void whenDeleteHall_thenHallNotExist() throws HallNotExistsException, LocationNotExistsException, HallDeleteException {
		Hall hall = service.deleteHall(20l, 21l);
		assertFalse(hall.getActive());
	}
	
	@Test(expected = HallDeleteException.class)
	public void whenDeleteHall_thenHasEvent() throws HallNotExistsException, LocationNotExistsException, HallDeleteException {
		Hall hall = service.deleteHall(22l, 22l);
		assertFalse(hall.getActive());
	}

}
