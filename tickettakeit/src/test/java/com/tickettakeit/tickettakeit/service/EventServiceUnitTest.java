package com.tickettakeit.tickettakeit.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.dto.EventDTO;
import com.tickettakeit.tickettakeit.dto.NewEventWithBackgroundDTO;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventDateBeforeSalesDateException;
import com.tickettakeit.tickettakeit.exceptions.EventNameIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.HallIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.LocationIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SectionIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.model.Section;
import com.tickettakeit.tickettakeit.repository.EventRepository;
import com.tickettakeit.tickettakeit.service.CategoryService;
import com.tickettakeit.tickettakeit.service.EventSectionService;
import com.tickettakeit.tickettakeit.service.EventService;
import com.tickettakeit.tickettakeit.service.HallService;
import com.tickettakeit.tickettakeit.service.LocationService;

@Transactional
@Rollback
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@SpringBootTest
public class EventServiceUnitTest {

	@SpyBean
	private EventService eventService;

	@MockBean
	private HallService hallService;

	@MockBean
	private LocationService locationService;

	@MockBean
	private CategoryService catService;

	@MockBean
	private EventSectionService eventSectionService;

	@MockBean
	private SectionService sectionService;

	@MockBean
	private EventRepository eventRepository;

	// Data
	private Event savedEvent;
	private Hall hall;
	private Category cat1;
	private Category cat2;
	private Section es1;
	private Section es2;
	private Location location;
	private NewEventWithBackgroundDTO newEventDTO;
	private HashSet<Hall> halls;
	private ArrayList<Long> date;
	private ArrayList<String> attachments;
	private ArrayList<Long> categories;
	private HashMap<Long, Float> sections;
	private ArgumentCaptor<Event> argument;
	private Date today;

	@Before
	public void setUp() {

		cat1 = new Category(2L, "Cat 1", "White", "Icon 1");
		Mockito.when(catService.findByIdOptional(cat1.getId())).thenReturn(Optional.of(cat1));
		cat2 = new Category(3L, "Cat 2", "Blue", "Icon 2");
		Mockito.when(catService.findByIdOptional(cat2.getId())).thenReturn(Optional.of(cat2));

		es1 = new Section(4L, "ES 1", 8, 5, true, 40, true);
		Mockito.when(sectionService.findById(es1.getId())).thenReturn(Optional.of(es1));
		es2 = new Section(5L, "ES 2", 0, 0, false, 50, true);
		Mockito.when(sectionService.findById(es2.getId())).thenReturn(Optional.of(es2));

		hall = new Hall("Hall 1");
		hall.setId(1L);
		Mockito.when(hallService.findById(hall.getId())).thenReturn(Optional.of(hall));
		hall.getSections().add(es1);
		hall.getSections().add(es2);

		halls = new HashSet<Hall>();
		halls.add(hall);
		location = new Location(6L, "Location 1", "Novi Sad", "Neka Ulica 123", 23.4241, 14.2323, "Img 1", halls, true);
		Mockito.when(locationService.findById(location.getId())).thenReturn(Optional.of(location));

		today = new Date();
		Date salesDate = new Date(today.getTime() + 5 * (1000 * 60 * 60 * 24));
		date = new ArrayList<Long>();
		// Sets date 10 and 11 days from now.
		date.add(today.getTime() + 10 * (1000 * 60 * 60 * 24));
		date.add((today.getTime() + 11 * (1000 * 60 * 60 * 24)));
		attachments = new ArrayList<String>();
		// attachments.add("A1");
		// attachments.add("A2");

		categories = new ArrayList<Long>();
		categories.add(2L);
		categories.add(3L);
		sections = new HashMap<>();
		sections.put(4L, new Float(100));
		sections.put(5L, new Float(100));

		newEventDTO = new NewEventWithBackgroundDTO("Event 1", "Test Desc", date, new Long(salesDate.getTime()), null,
				EventStatus.NORMAL.ordinal(), 2, false, attachments, hall.getId(), categories, sections,
				location.getId(), null, "Event 1");
		argument = ArgumentCaptor.forClass(Event.class);
	}

	@Test(expected = EventNotExistsException.class)
	public void whenGetEventWrongId_thenEventNotExistsException() throws EventNotExistsException {
		// Database is empty
		eventService.getEvent(100L);
	}

	@Test()
	public void whenGetEventById_thenReturnEventOptional() throws EventNotExistsException {
		// Mock eventRepository findById
		Event getEvent = new Event(new Long(1), "Event 1", "Event Desc", new HashSet<Date>(), new Date(), null,
				EventStatus.NORMAL, 5, false, "", new HashSet<String>(), new Hall(), new HashSet<Category>(),
				new HashSet<EventSection>(), new Location());
		Mockito.when(eventRepository.findById(1L)).thenReturn(Optional.of(getEvent));

		EventDTO foundEvent = eventService.getEvent(1L);

		assertEquals("Assert ID: ", new Long(1), foundEvent.getId());
		assertEquals("Assert Name: ", "Event 1", foundEvent.getName());
		assertEquals("Assert Description: ", "Event Desc", foundEvent.getDescription());
		assertEquals("Event Status: ", EventStatus.NORMAL, EventStatus.values()[foundEvent.getStatus()]);
		assertEquals("Assert Last Dat To Pay: ", 5, foundEvent.getLastDayToPay());
	}

	@Test
	public void whenAddEvent_thenReturnEventAndCheckAttributes()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {

		eventService.addEvent(newEventDTO);

		verify(eventRepository, times(1)).save(argument.capture());
		savedEvent = argument.getValue();

		assertEquals("Assert Name: ", "Event 1", savedEvent.getName());
		assertEquals("Assert Description: ", "Test Desc", savedEvent.getDescription());
		assertEquals("Event Status: ", EventStatus.NORMAL, EventStatus.values()[savedEvent.getStatus().ordinal()]);
		assertEquals("Assert Last Dat To Pay: ", 2, savedEvent.getLastDayToPay());
		assertEquals("Assert Date: ", date.size(), savedEvent.getDate().size());
		assertEquals("Assert Num Of Attachments: ", attachments.size(), savedEvent.getAttachments().size());
		assertEquals("Assert Num Of Categories: ", categories.size(), savedEvent.getCategories().size());
		assertEquals("Assert Num Of Event Sections: ", sections.size(), savedEvent.getEventSections().size());
		assertEquals("Assert Sales Date: ", new Date(newEventDTO.getSalesDate()), savedEvent.getSalesDate());
		assertEquals("Assert Hall: ", hall.getId(), savedEvent.getHall().getId());
		assertEquals("Assert Location: ", location.getId(), savedEvent.getLocation().getId());
	}

	@Test(expected = SalesDateHasPassedException.class)
	public void whenAddEventSalesDateBeforeToday_thenSalesDateBeforeTodayException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Sets sales date 1 day before today.
		newEventDTO.setSalesDate(today.getTime() - (1000 * 60 * 60 * 24));

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());

		// Return to old values.
		newEventDTO.setSalesDate(today.getTime() + 5 * (1000 * 60 * 60 * 24));

	}

	@Test(expected = EventDateBeforeSalesDateException.class)
	public void whenAddEventDateBeforeToday_EventDateBeforeSalesDateException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds date 1 day before today.
		Long dateBeforeToday = new Date().getTime() - (1000 * 60 * 60 * 24);
		newEventDTO.getDate().add(dateBeforeToday);

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());

		// Return to old values.
		newEventDTO.getDate().remove(dateBeforeToday);

	}

	@Test(expected = EventDateBeforeSalesDateException.class)
	public void whenAddEventDateBeforeSalesDate_EventDateBeforeSalesDateException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds date 1 day before sales date.
		Long dateBeforeSalesDate = new Date().getTime() - 4 * (1000 * 60 * 60 * 24);
		newEventDTO.getDate().add(dateBeforeSalesDate);

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());

		// Return to old values.
		newEventDTO.getDate().remove(dateBeforeSalesDate);

	}

	@Test(expected = EventNameIsTakenException.class)
	public void whenAddEventNameExists_EventNameIsTakenException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Mocks Event with same name.
		Event eventWithSameName = new Event();
		Mockito.when(eventRepository.findByName("Event 1")).thenReturn(Optional.of(eventWithSameName));

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());

	}

	@Test(expected = HallNotExistsException.class)
	public void whenAddEventHallIsMissing_thenThrowHallNotExistsException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Changes Hall so it doesn't exist.
		newEventDTO.setHallID(15L);

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());

		// Return to old values.
		newEventDTO.setHallID(1L);
	}

	@Test(expected = LocationNotExistsException.class)
	public void whenAddEventLocationIsMissing_thenThrowLocationNotExistsException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Changes Location so it doesn't exist.
		newEventDTO.setLocationID(15L);

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());

		// Return to old values.
		newEventDTO.setHallID(6L);
	}

	@Test(expected = CategoryNotExistsException.class)
	public void whenAddEventCategoryIsMissing_thenThrowCategoryNotExistsException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds Category that doesn't exist.
		categories.add(15L);

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());

		// Return to old values.
		categories.remove(2);
	}

	@Test(expected = SectionNotExistsException.class)
	public void whenAddEventEventSectionIsMissing_thenThrowEventSectionNotExistsException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds EventSection that doesn't exist.
		sections.put(15L, new Float(150));

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());

		// Return to old values.
		sections.remove(15L);
	}

	@Test(expected = HallIsTakenException.class)
	public void whenAddEventHallIsTaken_thenThrowHallIsTakenExceptionException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds Event with conflicting date.
		Event event2 = new Event();
		Date today = new Date();
		Date event2Date = new Date(today.getTime() + 10 * (1000 * 60 * 60 * 24));
		HashSet<Date> dates2 = new HashSet<Date>();
		dates2.add(event2Date);
		event2.setDate(dates2);
		event2.setStatus(EventStatus.NORMAL);

		ArrayList<Event> foundByHall = new ArrayList<Event>();
		foundByHall.add(event2);

		Mockito.doReturn(foundByHall).when(eventService).findByHall(1L);

		eventService.addEvent(newEventDTO);

		// Checks if method has passed.
		verify(eventRepository, times(0)).save(argument.capture());
	}
}
