package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.converter.HallConverter;
import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.dto.SectionDTO;
import com.tickettakeit.tickettakeit.exceptions.HallAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallCreatingException;
import com.tickettakeit.tickettakeit.exceptions.HallDeleteException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallUpdateException;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.model.Section;
import com.tickettakeit.tickettakeit.repository.HallRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback(value = true)
public class HallServiceUnitTest {
	@Autowired
	HallService service;
	@MockBean
	LocationService locationService;
	@MockBean
	HallRepository repository;
	@MockBean
	private EventService eventService;

	@Before
	public void setUp() throws LocationNotExistsException, HallNotExistsException {
		Optional<Location> l = Optional
				.of(new Location("Spens", "Novi Sad", "Adresa", 10, 11, "", new HashSet<>(), true));
		l.get().setId(1l);
		Mockito.when(locationService.findOne(l.get().getId())).thenReturn(l);
		Mockito.when(locationService.save(l.get())).thenReturn(l.get());

		List<Hall> halls = new ArrayList<Hall>();
		Hall hall = new Hall("Hall1");
		hall.setId(1l);
		halls.add(hall);
		l.get().setHalls(new HashSet<Hall>(halls));
		Mockito.when(repository.findHallsFromLocation(1l)).thenReturn(Optional.of(halls));
	}

	@Test
	public void whencreateHall_thenCreateHall()
			throws LocationNotExistsException, HallAlreadyExistsException, HallCreatingException {
		HallDTO dto = new HallDTO();
		dto.setId(null);
		dto.setName("Hall name");
		dto.setSections(new ArrayList<SectionDTO>());
		Mockito.when(repository.save(ArgumentMatchers.any(Hall.class))).thenReturn(HallConverter.fromDto(dto));
		Hall hall = service.createHall(1l, dto);
		assertEquals( "Hall name", hall.getName());
	}

	@Test(expected = LocationNotExistsException.class)
	public void whencreateHall_thenLocationNotExist()
			throws LocationNotExistsException, HallAlreadyExistsException, HallCreatingException {
		HallDTO dto = new HallDTO();
		dto.setId(null);
		dto.setName("Hall name");
		dto.setSections(new ArrayList<SectionDTO>());
		service.createHall(2l, dto);
	}

	@Test(expected = HallAlreadyExistsException.class)
	public void whencreateHall_thenHallAlreadyExist()
			throws LocationNotExistsException, HallAlreadyExistsException, HallCreatingException {
		HallDTO dto = new HallDTO();
		dto.setId(null);
		dto.setName("Hall1");
		dto.setSections(new ArrayList<SectionDTO>());
		service.createHall(1l, dto);
	}

	@Test
	public void whenUpdateHall_thenUpdateHall() throws HallNotExistsException, HallUpdateException {
		Hall h = new Hall("Hall1");
		h.setId(1l);
		Mockito.when(repository.findHallsFromLocationAndHall(1l, 1l)).thenReturn(Optional.of(h));
		HallDTO dto = new HallDTO();
		dto.setName("Hall2");
		dto.setSections(new ArrayList<SectionDTO>());
		Mockito.when(repository.save(ArgumentMatchers.any(Hall.class))).thenReturn(HallConverter.fromDto(dto));
		Hall hall = service.updateHall(1l, 1l, dto);
		assertEquals("Hall2", hall.getName());
	}

	@Test(expected = HallNotExistsException.class)
	public void whenUpdateHall_thenHallNotExists() throws HallNotExistsException, HallUpdateException {
		Hall h = new Hall("Hall1");
		h.setId(1l);
		Mockito.when(repository.findHallsFromLocationAndHall(1l, 1l)).thenReturn(Optional.of(h));
		HallDTO dto = new HallDTO();
		dto.setName("Hall2");
		dto.setSections(new ArrayList<SectionDTO>());
		Mockito.when(repository.save(ArgumentMatchers.any(Hall.class))).thenReturn(HallConverter.fromDto(dto));
		Hall hall = service.updateHall(1l, 2l, dto);
		assertEquals("Hall2", hall.getName());
	}

	@Test
	public void whendeleteHall_thenDeleteHall() throws HallNotExistsException, LocationNotExistsException, HallDeleteException {
		Hall h = new Hall(10l, "Hall", new HashSet<Section>(), true);
		h.setActive(false);
		Mockito.when(repository.findHallsFromLocationAndHall(10l,10l)).thenReturn(Optional.of(h));
		Mockito.when(repository.save(ArgumentMatchers.any(Hall.class))).thenReturn(h);
		Hall hall = service.deleteHall(10l, 10l);
		
		assertFalse(hall.getActive());
	}

	@Test(expected = HallNotExistsException.class)
	public void whendeleteHall_thenHallNotExist() throws HallNotExistsException, LocationNotExistsException, HallDeleteException {
		service.deleteHall(2115l, 220l);
		
	}

	@Test(expected = HallDeleteException.class)
	public void whendeleteHall_thenHasEvent() throws HallNotExistsException, LocationNotExistsException, HallDeleteException, ParseException {
		Hall h = new Hall(10l, "Hall", new HashSet<Section>(), true);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date d = sdf.parse("11-11-2025");
		Set<Date> dates = new HashSet<Date>();
		dates.add(d);
		Event e = new Event(1l, "Event", "Opis",dates , new Date(),new Date(), EventStatus.NORMAL, 2, false, "Str", null, null, null, null, null);
		List<Event> events = new ArrayList<Event>();
		events.add(e);
		Mockito.when(eventService.selectEventsFromHall(20l)).thenReturn(Optional.of(events));
		Mockito.when(repository.findHallsFromLocationAndHall(20l,20l)).thenReturn(Optional.of(h));
		Mockito.when(repository.save(ArgumentMatchers.any(Hall.class))).thenReturn(h);
		service.deleteHall(20l, 20l);
	}

}
