package com.tickettakeit.tickettakeit.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.EventDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:ana-test.properties")
public class EventSearchControllerIntegrationTest {

	@Autowired
	private TestRestTemplate testRestTemplate;

	private static final String EVENT_NAME_SEARCH = "Event1";
	private static final String CITY_SEARCH = "Beograd";
	private static final String LOCATION_SEARCH = "Beogradska arena";
	private static final Long FROM_DATE = 1606777200000L; // 12. 02. 2020.
	private static final Long TO_DATE = 1613084400000L; // 12. 02. 2021.

	@Test
	@Transactional
	public void getEvents_getAll() {
		ResponseEntity<EventDTO[]> response = testRestTemplate.getForEntity("/api/events", EventDTO[].class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(4, response.getBody().length);
	}

	@Test
	@Transactional
	public void getEvents_getAll_Pageable() {
		ResponseEntity<EventDTO[]> response = testRestTemplate.getForEntity("/api/events?page=0&size=2",
				EventDTO[].class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(2, response.getBody().length);
	}

	@Test
	@Transactional
	public void getEvents_generalSearch() {
		ResponseEntity<EventDTO[]> response = testRestTemplate
				.getForEntity("/api/events?page=0&size=4&searchString=" + EVENT_NAME_SEARCH, EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(1, eventDTOs.length);
		assertEquals(EVENT_NAME_SEARCH, eventDTOs[0].getName());
	}

	@Test
	public void getEvents_eventNameSearch() {
		ResponseEntity<EventDTO[]> response = testRestTemplate
				.getForEntity("/api/events?page=0&size=4&eventName=" + EVENT_NAME_SEARCH, EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());

		assertEquals(1, eventDTOs.length);
		assertEquals(EVENT_NAME_SEARCH, eventDTOs[0].getName());
	}

	@Test
	public void getEvents_citySearch() {
		ResponseEntity<EventDTO[]> response = testRestTemplate
				.getForEntity("/api/events?page=0&size=4&city=" + CITY_SEARCH, EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(2, eventDTOs.length);

	}

	@Test
	public void getEvents_locationNameSearch() {
		ResponseEntity<EventDTO[]> response = testRestTemplate
				.getForEntity("/api/events?page=0&size=4&locationName=" + LOCATION_SEARCH, EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(2, eventDTOs.length);

	}

	@Test
	public void getEvents_priceSearch() {
		ResponseEntity<EventDTO[]> response = testRestTemplate
				.getForEntity("/api/events?page=0&size=4&fromPrice=0&toPrice=300", EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(4, eventDTOs.length);

	}

	@Test
	public void getEvents_datesSearch() {
		ResponseEntity<EventDTO[]> response = testRestTemplate.getForEntity(
				"/api/events?page=0&size=4&fromDateTs=" + FROM_DATE + "&toDateTs=" + TO_DATE, EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(3, eventDTOs.length);

	}

	@Test
	public void getEvents_categorySearch() {
		ResponseEntity<EventDTO[]> response = testRestTemplate.getForEntity("/api/events?page=0&size=4&categoryId=100",
				EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(2, eventDTOs.length);

	}

	// combined detailed search
	@Test
	public void getEvents_detailedSearch() {

		ResponseEntity<EventDTO[]> response = testRestTemplate.getForEntity(
				"/api/events?page=0&size=4&eventName=Event&city=Beograd&locationName=Beogradska&fromDate="
						+ FROM_DATE + "&toDate=" + TO_DATE,
				EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(2, eventDTOs.length);

	}

	// no results search
	@Test
	public void getEvents_noResult() {

		ResponseEntity<EventDTO[]> response = testRestTemplate.getForEntity(
				"/api/events?page=0&size=4&eventName=adfgaerg4",
				EventDTO[].class);

		EventDTO[] eventDTOs = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(0, eventDTOs.length);

	}

}
