package com.tickettakeit.tickettakeit.controller;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.SectionDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-mile.properties")
@Transactional
@Rollback(true)
public class HallControllerTest {
	@Autowired
	private TestRestTemplate testRest;
	HttpHeaders headers;
	String token;
	@Before
	public void setUp() {
		headers = new HttpHeaders();
		LogInDTO dto = new LogInDTO("pera", "peric");
		HttpEntity<LogInDTO> request = new HttpEntity<LogInDTO>(dto, headers);
		ResponseEntity<JwtResponseDTO> response = testRest.postForEntity("/auth/login", request, JwtResponseDTO.class);
		headers.add("Authorization", "Bearer "+response.getBody().getToken());
		token = "Bearer "+response.getBody().getToken();
		headers.add("Content-Type", "application/json");
	}
	@Test
	public void whenGetHalls_returnHalls() {
		ResponseEntity<HallDTO[]> response =  testRest.getForEntity("/api/halls/20", HallDTO[].class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(1, response.getBody().length);
	}
	@Test
	public void whenGetHalls_LocationNotFound() {
		ResponseEntity<ErrorDetailsDTO> response =  testRest.getForEntity("/api/halls/111", ErrorDetailsDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	@Test
	public void whenGetHall_ReturnHall() {
		ResponseEntity<HallDTO> response =  testRest.getForEntity("/api/halls/10/10", HallDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Hall1", response.getBody().getName());
	}
	@Test
	public void whenGetHall_HallNotFound() {
		ResponseEntity<HallDTO> response =  testRest.getForEntity("/api/halls/10/22", HallDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	@Test
	public void whenCreateHall_HallCreated() {
		HallDTO dto = new HallDTO();
		dto.setName("HallName");
		dto.setSections(new ArrayList<SectionDTO>());
		HttpEntity<HallDTO> request = new HttpEntity<HallDTO>(dto, headers);
		ResponseEntity<HallDTO> response = testRest.postForEntity("/api/halls/10", request, HallDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("HallName", response.getBody().getName());
	}
	@Test
	public void whenCreateHall_HallAlreadyExist() {
		HallDTO dto = new HallDTO();
		dto.setName("existing");
		dto.setSections(new ArrayList<SectionDTO>());
		HttpEntity<HallDTO> request = new HttpEntity<HallDTO>(dto, headers);
		ResponseEntity<HallDTO> response = testRest.postForEntity("/api/halls/10", request, HallDTO.class);
		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}
	@Test
	public void whenCreateHall_LocationNotExist() {
		HallDTO dto = new HallDTO();
		dto.setName("HallName");
		dto.setSections(new ArrayList<SectionDTO>());
		HttpEntity<HallDTO> request = new HttpEntity<HallDTO>(dto, headers);
		ResponseEntity<HallDTO> response = testRest.postForEntity("/api/halls/2", request, HallDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	@Test
	public void whenUpdateHall_HallUpdated() {
		HallDTO dto = new HallDTO();
		dto.setName("Name");
		dto.setSections(new ArrayList<SectionDTO>());
		HttpEntity<HallDTO> request = new HttpEntity<HallDTO>(dto, headers);
		ResponseEntity<HallDTO> response = testRest.exchange("/api/halls/10/10", HttpMethod.PUT, request, HallDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	@Test
	public void whenUpdateHall_HallNotExist() {
		HallDTO dto = new HallDTO();
		dto.setName("Name");
		dto.setSections(new ArrayList<SectionDTO>());
		HttpEntity<HallDTO> request = new HttpEntity<HallDTO>(dto, headers);
		ResponseEntity<HallDTO> response = testRest.exchange("/api/halls/10/111", HttpMethod.PUT, request, HallDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	@Test
	public void whenUpdateHall_LocationNotExist() {
		HallDTO dto = new HallDTO();
		dto.setName("Name");
		dto.setSections(new ArrayList<SectionDTO>());
		HttpEntity<HallDTO> request = new HttpEntity<HallDTO>(dto, headers);
		ResponseEntity<HallDTO> response = testRest.exchange("/api/halls/1/1", HttpMethod.PUT, request, HallDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	@Test
	public void whenDeleteHall_DeleteHall() {
		HttpEntity<Object> request = new HttpEntity<Object>(null,headers);
		ResponseEntity<Boolean> response = testRest.exchange("/api/halls/10/10", HttpMethod.DELETE, request, Boolean.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody());
	}
	@Test
	public void whenDeleteHall_HallNotFound() {
		HttpEntity<Object> request = new HttpEntity<Object>(null,headers);
		ResponseEntity<ErrorDetailsDTO> response = testRest.exchange("/api/halls/10/122", HttpMethod.DELETE, request, ErrorDetailsDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
}
