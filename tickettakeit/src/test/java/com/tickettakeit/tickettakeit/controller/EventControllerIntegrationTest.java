package com.tickettakeit.tickettakeit.controller;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.EventDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.NewEventDTO;
import com.tickettakeit.tickettakeit.dto.NewEventWithBackgroundDTO;

// Resets Application Context before every test.
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
@Rollback
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:event-integration-test.properties")
public class EventControllerIntegrationTest {

	@Autowired
	private TestRestTemplate testRest;

	private HttpHeaders headers;

	private String token;

	private NewEventDTO newEventDTO;
	private String name;
	private String description;
	private ArrayList<Long> date;
	private Long salesDate;
	private Long applicationDate;
	private int status;
	private int lastDayToPay;
	private boolean controlAccess;
	private ArrayList<String> attachments;
	private Long hallID;
	private ArrayList<Long> categoriesIDs;
	private HashMap<Long, Float> eventSectionsIDs;
	private Long locationID;
	private Date today;

	@Before
	public void setUp() {
		// Sets up Token for logged user (ADMIN user, user).
		headers = new HttpHeaders();
		LogInDTO dto = new LogInDTO("user", "user");
		HttpEntity<LogInDTO> request = new HttpEntity<LogInDTO>(dto, headers);
		ResponseEntity<JwtResponseDTO> response = testRest.postForEntity("/auth/login", request, JwtResponseDTO.class);
		headers.add("Authorization", "Bearer " + response.getBody().getToken());
		token = "Bearer " + response.getBody().getToken();
		headers.add("Content-Type", "application/json");

		// Sets acceptable data for adding new Event.
		today = new Date();

		name = "Event 2";
		description = "Event Desc 2";
		date = new ArrayList<Long>();
		date.add(today.getTime() + 10 * (1000 * 60 * 60 * 24));
		date.add(today.getTime() + 11 * (1000 * 60 * 60 * 24));
		salesDate = today.getTime() + 5 * (1000 * 60 * 60 * 24);
		applicationDate = today.getTime() + 4 * (1000 * 60 * 60 * 24);
		status = 0;
		lastDayToPay = 5;
		controlAccess = false;
		attachments = new ArrayList<String>();
		hallID = 10L;
		categoriesIDs = new ArrayList<Long>();
		categoriesIDs.add(10L);
		categoriesIDs.add(20L);
		eventSectionsIDs = new HashMap<Long, Float>();
		eventSectionsIDs.put(10L, new Float(150));
		eventSectionsIDs.put(20L, new Float(250));
		locationID = 10L;

		newEventDTO = new NewEventDTO(name, description, date, salesDate, status, lastDayToPay, controlAccess,
				attachments, hallID, categoriesIDs, eventSectionsIDs, locationID);
	}

	@Test
	public void whenGetEvent_thenReturnEventDTO() {
		String name = "Event 1";
		String description = "Event Desc";
		ArrayList<Long> date = new ArrayList<Long>();
		date.add(1602280800000L);
		date.add(1602367200000L);
		date.add(1602453600000L);
		Long salesDate = 1599688800000L;
		int status = 0;
		int lastDayToPay = 5;
		boolean controlAccess = false;
		// ArrayList<String> attachments = new ArrayList<String>();
		Long hallID = 10L;
		ArrayList<Long> categoriesIDs = new ArrayList<Long>();
		categoriesIDs.add(10L);
		categoriesIDs.add(20L);
		ArrayList<Long> eventSectionsIDs = new ArrayList<Long>();
		eventSectionsIDs.add(10L);
		eventSectionsIDs.add(20L);
		Long locationID = 10L;

		ResponseEntity<EventDTO> response = testRest.getForEntity("/api/events/10", EventDTO.class);
		EventDTO eventDTO = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(name, eventDTO.getName());
		assertEquals(description, eventDTO.getDescription());
		assertEquals(date.size(), eventDTO.getDate().size());
		assertEquals(salesDate, eventDTO.getSalesDate());
		assertEquals(status, eventDTO.getStatus());
		assertEquals(lastDayToPay, eventDTO.getLastDayToPay());
		assertEquals(controlAccess, eventDTO.isControllAccess());
		assertTrue(eventDTO.getAttachments().isEmpty());
		assertEquals(hallID, eventDTO.getHallID());
		assertEquals(categoriesIDs.size(), eventDTO.getCategories().size());
		assertEquals(eventSectionsIDs.size(), eventDTO.getEventSectionsIDs().size());
		assertTrue(eventDTO.getEventSectionsIDs().contains(eventSectionsIDs.get(0)));
		assertTrue(eventDTO.getEventSectionsIDs().contains(eventSectionsIDs.get(1)));
		assertEquals(locationID, eventDTO.getLocationID());
	}

	@Test
	public void whenGetEventWrongId_thenReturnEventNotFound() {
		ResponseEntity<ErrorDetailsDTO> response = testRest.getForEntity("/api/events/15", ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenAddEventWithoutBackground_thenReturnNewEventId() {
		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<Long> response = testRest.postForEntity("/api/events/add-event", request, Long.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void whenAddEventSalesDateBeforeToday_thenSalesDateBeforeTodayException() {
		// Sets sales date 1 day before today.
		newEventDTO.setSalesDate(today.getTime() - (1000 * 60 * 60 * 24));

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");
		ne.setApplicationDate(applicationDate);

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());

		// Return to old values.
		newEventDTO.setSalesDate(salesDate);

	}

	@Test
	public void whenAddEventDateBeforeToday_EventDateBeforeSalesDateException() {
		// Adds date 1 day before today.
		Long dateBeforeToday = new Date().getTime() - (1000 * 60 * 60 * 24);
		newEventDTO.getDate().add(dateBeforeToday);

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());

		// Return to old values.
		newEventDTO.getDate().remove(dateBeforeToday);
	}

	@Test
	public void whenAddEventDateBeforeSalesDate_EventDateBeforeSalesDateException() {
		// Adds date 1 day before sales date.
		Long dateBeforeSalesDate = new Date().getTime() - 4 * (1000 * 60 * 60 * 24);
		newEventDTO.getDate().add(dateBeforeSalesDate);

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());

		// Return to old values.
		newEventDTO.getDate().remove(dateBeforeSalesDate);
	}

	@Test
	public void whenAddEventNameExists_EventNameIsTakenException() {
		// Sets new Event name to existing one.
		newEventDTO.setName("Event 1");

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());

		// Returns new Event name to old one.
		newEventDTO.setName(name);
	}

	@Test
	public void whenAddEventHallIsMissing_thenThrowHallNotExistsException() {
		// Changes Hall so it doesn't exist.
		newEventDTO.setHallID(15L);

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

		// Return to old values.
		newEventDTO.setHallID(hallID);
	}

	@Test
	public void whenAddEventLocationIsMissing_thenThrowLocationNotExistsException() {
		// Changes Location so it doesn't exist.
		newEventDTO.setLocationID(15L);

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

		// Return to old values.
		newEventDTO.setHallID(locationID);
	}

	@Test
	public void whenAddEventCategoryIsMissing_thenThrowCategoryNotExistsException() {
		// Adds Category that doesn't exist.
		categoriesIDs.add(15L);

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

		// Return to old values.
		categoriesIDs.remove(2);
	}

	@Test
	public void whenAddEventEventSectionIsMissing_thenThrowEventSectionNotExistsException() {
		// Adds EventSection that doesn't exist.
		eventSectionsIDs.put(15L, new Float(150));

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

		// Return to old values.
		eventSectionsIDs.remove(15L);
	}

	@Test
	public void whenAddEventHallIsTaken_thenThrowHallIsTakenExceptionException() {
		// Sets new Event date to be conflicting with existing one.
		date.add(1602280800000L);

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());

		// Removes new added date.
		date.remove(2);
	}

	@Test()
	public void whenAddEventNameIsNull_thenThrowBadRequest() {
		// Event name is null
		newEventDTO.setName(null);

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

		// Return to old values.
		newEventDTO.setName(name);
	}

	@Test()
	public void whenAddEventNameIsShort_thenThrowBadRequest() {
		// Event name is shorter thank 4 words.
		newEventDTO.setName("Abc");

		NewEventWithBackgroundDTO ne = new NewEventWithBackgroundDTO(newEventDTO, null, "");

		HttpEntity<NewEventWithBackgroundDTO> request = new HttpEntity<NewEventWithBackgroundDTO>(ne, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/events/add-event", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

		// Return to old values.
		newEventDTO.setName(name);
	}
}
