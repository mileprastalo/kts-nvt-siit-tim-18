package com.tickettakeit.tickettakeit.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.ReportDTO;
import com.tickettakeit.tickettakeit.dto.ReportDataDTO;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:ana-test.properties")
public class ReportControllerIntegrationTest {

	@Autowired
	private TestRestTemplate testRestTemplate;

	HttpHeaders headers;

	String token;

	private static final String URL = "/api/reports/";
	private static final Long FROM_DATE = 1570658400000L; // 10. 10. 2019.
	private static final Long TO_DATE = 1571263200000L; // 17. 10. 2019.

	@Before
	public void setUp() {

		headers = new HttpHeaders();
		LogInDTO dto = new LogInDTO("pera", "user");
		HttpEntity<LogInDTO> request = new HttpEntity<LogInDTO>(dto, headers);
		ResponseEntity<JwtResponseDTO> response = testRestTemplate.postForEntity("/auth/login", request,
				JwtResponseDTO.class);
		headers.add("Authorization", "Bearer " + response.getBody().getToken());
		token = "Bearer " + response.getBody().getToken();
		headers.add("Content-Type", "application/json");
	}

	// ***** getReport *****
	@Test
	public void getReport_dailyReport_success() {

		HttpEntity<Object> request = new HttpEntity<Object>(null, headers);

		ResponseEntity<ReportDTO> response = testRestTemplate.exchange(
				URL + "?eventId=100&fromDateTs=" + FROM_DATE + "&toDateTs=" + TO_DATE + "&type=daily", HttpMethod.GET,
				request, ReportDTO.class);

		ArrayList<ReportDataDTO> result = response.getBody().getReportData();
		assertEquals(HttpStatus.OK, response.getStatusCode());

		assertNotNull(result);
		assertEquals(7, result.size());
		assertEquals(1, result.get(0).getSoldTickets());
		assertEquals(100, result.get(0).getEarnings(), 0);
		result.remove(0);
		assertThat(result).hasSize(6).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// Invalid date exception
	@Test
	public void getReport_invalidDates() {

		HttpEntity<Object> request = new HttpEntity<Object>(null, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRestTemplate.exchange(
				URL + "?eventId=100&fromDateTs=" + TO_DATE + "&toDateTs=" + FROM_DATE + "&type=daily", HttpMethod.GET,
				request, ErrorDetailsDTO.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

	}

	// Invalid report type
	@Test
	public void getReport_invalidReportType() {

		HttpEntity<Object> request = new HttpEntity<Object>(null, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRestTemplate.exchange(
				URL + "?eventId=100&fromDateTs=" + FROM_DATE + "&toDateTs=" + TO_DATE + "&type=dailyyyy",
				HttpMethod.GET, request, ErrorDetailsDTO.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

	}

}
