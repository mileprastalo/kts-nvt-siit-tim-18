/* Adds categories */
INSERT INTO `tickettakeit_test`.`category` (`id`, `color`, `icon`, `name`) VALUES ('10', 'Blue', 'Test Icon 1', 'Music Festival');
INSERT INTO `tickettakeit_test`.`category` (`id`, `color`, `icon`, `name`) VALUES ('20', 'Red', 'Test Icon 2', 'Art Exibition');

/* Adds locations */
INSERT INTO `tickettakeit_test`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('10', 'Address 1', 'Novi Sad', 'Test Image 1', '19.859807', '45.255826', 'Location 1', TRUE);
INSERT INTO `tickettakeit_test`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('20', 'Address 2', 'Belgrade', 'Test Image 2', '20.455915', '44.788317', 'Location 2', TRUE);

/* Adds halls */
INSERT INTO `tickettakeit_test`.`hall` (`id`, `name`, `active`) VALUES ('10', 'Hall 1', TRUE);
INSERT INTO `tickettakeit_test`.`hall` (`id`, `name`, `active`) VALUES ('20', 'Hall 2', TRUE);
INSERT INTO `tickettakeit_test`.`hall` (`id`, `name`, `active`) VALUES ('30', 'Hall 3', TRUE);

/* Adds sections */
INSERT INTO `tickettakeit_test`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`) VALUES ('10', '10', TRUE, 'Section 1', '100', '10');
INSERT INTO `tickettakeit_test`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`) VALUES ('20', '0', FALSE, 'Section 2', '1', '0');

/* Adds halls to locations */
INSERT INTO `tickettakeit_test`.`location_halls` (`location_id`, `halls_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`location_halls` (`location_id`, `halls_id`) VALUES ('10', '20');
INSERT INTO `tickettakeit_test`.`location_halls` (`location_id`, `halls_id`) VALUES ('20', '30');

/* Adds sections to halls */
INSERT INTO `tickettakeit_test`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('10', '20');

/* Adds registered users */
INSERT INTO `tickettakeit_test`.`registered_user` (`id`, `confirmed`, `email`, `first_name`, `last_name`, `password`, `phone`, `username`, `paypal`) VALUES ('10', TRUE, 'user@mail.com', 'User', 'Useric', '$2y$12$7agwOLLBYv6.GzIxJnK3QOd/yeRMfYb1A6TIHJf.nS2UDfDOD0MJu', '03024104', 'user', 'nema');

/* Adds authorities */
INSERT INTO `tickettakeit_test`.`authority` (`id`, `name`) VALUES ('10', 'REGISTERED');
INSERT INTO `tickettakeit_test`.`authority` (`id`, `name`) VALUES ('20', 'ADMIN');

/* Adds authorities to users */
INSERT INTO `tickettakeit_test`.`user_authority` (`user_id`, `authority_id`) VALUES ('10', '10');

/* Adds events, sale has began */
INSERT INTO `tickettakeit_test`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('10', FALSE, 'Event Desc', '5', 'Event 1', '2020-1-1', '0', '10', '10');
/* Event is BLOCKED */
INSERT INTO `tickettakeit_test`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('20', FALSE, 'Event Desc 2', '5', 'Event 2', '2020-1-1', '1', '10', '10');
/* Sale has not began yet */
INSERT INTO `tickettakeit_test`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('30', FALSE, 'Event Desc 3', '5', 'Event 3', '2020-8-1', '0', '10', '10');
/* Sale has passed */
INSERT INTO `tickettakeit_test`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('40', FALSE, 'Event Desc 3', '5', 'Event 3', '2020-1-1', '0', '10', '10');

/* Adds categories to events */
INSERT INTO `tickettakeit_test`.`event_categories` (`event_id`, `categories_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`event_categories` (`event_id`, `categories_id`) VALUES ('10', '20');

/* Adds dates to events */
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('10', '2020-10-10');
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('10', '2020-10-11');
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('10', '2020-10-12');
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('20', '2020-11-12');
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('30', '2020-9-10');
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('40', '2020-1-5');

/* Adds event sections */
INSERT INTO `tickettakeit_test`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('10', '10', TRUE, 'Section 1', '100', '200', '10', 100, 100);
INSERT INTO `tickettakeit_test`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('20', '0', FALSE, 'Section 2', '1', '100', '0', 100, 100);
INSERT INTO `tickettakeit_test`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('30', '10', TRUE, 'Section 3', '100', '200', '10', 100, 100);
INSERT INTO `tickettakeit_test`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('40', '10', TRUE, 'Section 4', '100', '200', '10', 100, 100);
INSERT INTO `tickettakeit_test`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`, `left_position`, `top`) VALUES ('50', '10', TRUE, 'Section 5', '100', '200', '10', 100, 100);

/* Adds event sections to events */
INSERT INTO `tickettakeit_test`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('10', '20');
INSERT INTO `tickettakeit_test`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('20', '30');
INSERT INTO `tickettakeit_test`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('30', '40');
INSERT INTO `tickettakeit_test`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('40', '50');

/* Adds Tickets */
INSERT INTO `tickettakeit_test`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`) VALUES ('10', TRUE, '10 10 Test', '5', '2020-1-3', '2020-10-11 00:00:00', '200', '5', '10', '10', '10');
INSERT INTO `tickettakeit_test`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`) VALUES ('20', false, '10 10 Test2', '0', '2020-1-3', '2020-11-10 00:00:00', '100', '0', '10', '20', '10');
/* Ticket for testing buying reserve tickets */
INSERT INTO `tickettakeit_test`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`) VALUES ('30', false, '10 10 Test Buy Reserved', '1', '2020-1-3', '2020-11-10 00:00:00', '200', '1', '10', '10', '10');
/* Ticket with BLOCKED Event */
INSERT INTO `tickettakeit_test`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`) VALUES ('40', false, '10 10 Test Buy Reserved 2', '1', '2020-1-3', '2020-11-10 00:00:00', '200', '1', '20', '30', '10');
/* Ticket with sale has passed Event */
INSERT INTO `tickettakeit_test`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`) VALUES ('50', false, '10 10 Test Buy Reserved 3', '1', '2020-1-4', '2020-1-5', '200', '1', '40', '50', '10');
/* Ticket not added to User */
INSERT INTO `tickettakeit_test`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`) VALUES ('60', false, '10 10 Test Buy Reserved 4', '1', '2020-1-3', '2020-11-10 00:00:00', '200', '1', '10', '10', '10');

/* Adds Ticket to it's User */
INSERT INTO `tickettakeit_test`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('10', '20');
INSERT INTO `tickettakeit_test`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('10', '30');
INSERT INTO `tickettakeit_test`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('10', '40');
INSERT INTO `tickettakeit_test`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('10', '50');
