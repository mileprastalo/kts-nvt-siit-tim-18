import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories-manager',
  templateUrl: './categories-manager.component.html',
  styleUrls: ['./categories-manager.component.scss']
})
export class CategoriesManagerComponent implements OnInit {

  selectedScreen = 0;
  categoryOperation: string;
  categoryId: number;

  constructor() { }

  ngOnInit() {

  }

  addCategoryEvent(id: number) {
    this.selectedScreen = 1;
    this.categoryId = id;
    this. categoryOperation = id ? 'update' : 'add';
  }

  cancel() {
    this.selectedScreen = 0;
    this.categoryId = null;
  }
}
