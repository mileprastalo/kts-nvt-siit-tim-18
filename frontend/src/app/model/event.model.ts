import { Category } from './category.model';

export class Event {

    id: number;
    name: string;
    description: string;
    date: Date[];
    salesDate: Date;
    applicationDate: Date;
    status: number;
    lastDayToPay: number;
    controllAccess: boolean;
    attachments: string[];
    locationID: number;
    locationName: string;
    hallID: number;
    hallname: string;
    eventSectionsIDs: number[];
    categories: Category[];
    background: string;
    backgroundName: string;
    totalSize: number;


}
