export class BuyTicket {
    row: number;
    column: number;
    forDay: number;
    eventSectionID: number;
    eventID: number;

    constructor( ticket?: BuyTicket, row?: number, column?: number) {
        this.row = row;
        this.column = column;
        this.forDay = ticket ? ticket.forDay : null;
        this.eventSectionID = ticket ? ticket.eventSectionID : null;
        this.eventID = ticket ? ticket.eventID : null;
    }
}
