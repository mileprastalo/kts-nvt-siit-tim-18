export class Attachment {

    id: number;
    attachment: string;

    constructor(id: number, attachment: string) {
        this.id = id;
        this.attachment = attachment;
    }
}
