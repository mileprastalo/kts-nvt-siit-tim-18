import { Hall } from './hall.model';

export class Location {

    id: number;
    name: string;
    city: string;
    address: string;
    longitude: string;
    latitude: string;
    image: string;
    halls: Array<Hall>;
    active: boolean;
    totalSize: number;

}
