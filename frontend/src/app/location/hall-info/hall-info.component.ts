import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Hall } from '../../model/hall.model';
import { Router } from '@angular/router';
import { HallService } from '../../service/hall.service';
import { fabric } from 'fabric';
import { Section } from '../../model/section.model';
import { SectionService } from '../../service/section.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-hall-info',
  templateUrl: './hall-info.component.html',
  styleUrls: ['./hall-info.component.scss']
})
export class HallInfoComponent implements OnInit {
  hall: Hall = new Hall();
  canvas;
  sectionMap: Map<Section, fabric.Rect> = new Map();
  selectedSection: Section;
  @Input() locationId: number;
  @Input() hallId: number;
  @Output() backToLocationInfo = new EventEmitter();
  @Output() updateHallEvent = new EventEmitter();
  @Output() createSectionEvent = new EventEmitter();
  @Output() sectionInfoEvent = new EventEmitter();

  constructor(private router: Router, private hallService: HallService,
              private sectionService: SectionService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.canvas = new fabric.Canvas('can', {});
    this.canvas.setWidth(400);
    this.canvas.setHeight(400);
    this.hallService.getHall(this.locationId, this.hallId).subscribe(
      (response => {
        this.hall = response;
        this.drawSections();
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      }
    );
  }
  createSection() {
    this.createSectionEvent.emit();
  }
  back() {
    this.backToLocationInfo.emit();
  }
  drawSections() {
    for (const section of this.hall.sections) {
      const rect = new fabric.Rect({
        top: section.top, left: section.left,
        width: this.getSectionSize(section, section.columns), height: this.getSectionSize(section, section.rows), fill: 'red'
      });
      rect.setControlsVisibility({
        mt: false,
        mb: false,
        ml: false,
        mr: false,
        bl: false,
        br: false,
        tl: false,
        tr: false,
        mtr: false,
    });
      this.canvas.add(rect);
      rect.on('mousedown', () => {
        this.selectedSection = section;
      });
      this.sectionMap.set(section, rect);
    }
  }
  save() {
    const list: Array<Section> = new Array();
    for (const section of this.hall.sections) {
      section.top = this.sectionMap.get(section).top;
      section.left = this.sectionMap.get(section).left;
      list.push(section);
    }
    this.sectionService.updateSectionPositions(list).subscribe(
      (response => {
        this.snackBar.open('Sections have been updated');
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      }
    );
  }
  viewSection() {
    this.sectionInfoEvent.emit(this.selectedSection.id);
  }
  getSectionSize(section: Section, property: number): number {
    if (section.numerated) {
      return property * 5;
    }
    return Math.sqrt(section.numberOfSeats) * 5;
  }
  updateHall() {
    this.updateHallEvent.emit();
  }

}
