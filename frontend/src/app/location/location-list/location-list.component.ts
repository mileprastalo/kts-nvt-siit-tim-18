import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { Location } from '../../model/location.model';
import { LocationService } from '../../service/location.service';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, PageEvent, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.scss']
})
export class LocationListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  locations: Location[];
  dataSource: MatTableDataSource<Location>;
  displayedColumns = ['name', 'city', 'address', 'longitude', 'latitude', 'status', 'deactivate'];
  pageEvent: PageEvent;
  datasource: null;
  pageIndex: number;
  pageSize: number;
  length: number;

  @Output() locationInfoEvent = new EventEmitter();
  @Output() locationCreateEvent = new EventEmitter();

  constructor(private locationService: LocationService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.locationService.getLocations(null).subscribe(
      (response => {
        this.locations = response;
        if (this.locations.length > 0) {
          this.length = this.locations[0].totalSize;
        } else {
          this.length = 0;
        }
        this.dataSource = new MatTableDataSource(this.locations);
        this.dataSource.paginator = this.paginator;
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      }
    );
  }
  getActive(element: Location): string {
    if (element.active) {
      return 'Active';
    } else {
      return 'Not active';
    }
  }
  deactivate(element: Location) {
    element.active = false;
    this.locationService.deleteLocation(element.id).subscribe(
      (response => {
        element.active = false;
        this.snackBar.open('Location has been deactivated');
      }),
      (error) => {
        element.active = true;
        this.snackBar.open(error.error.message);
      }
    );
  }
  activate(element: Location) {
    element.active = true;
    this.locationService.updateLocation(element).subscribe(
      (reponse => {
        this.snackBar.open('Location has been activated');
      }),
      (error => {
        this.snackBar.open('Error activating location');
      })
    );
  }
  newLocation() {
    this.locationCreateEvent.emit();
  }
  getServerData(event?: PageEvent) {
    this.locationService.getLocations(event).subscribe(
      (response => {
        this.locations = response;
        this.dataSource = new MatTableDataSource(this.locations);
        if (this.locations.length > 0) {
          this.length = this.locations[0].totalSize;
        } else {
          this.length = 0;
        }
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      }
    );
    return event;
  }
  info(element: Location) {
    this.locationInfoEvent.emit(element.id);
  }
}
