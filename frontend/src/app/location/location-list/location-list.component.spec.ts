import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationListComponent } from './location-list.component';
import { Observable } from 'rxjs';
import { MaterialModule } from 'src/app/material/material.module';
import { LocationService } from 'src/app/service/location.service';
import { DebugElement } from '@angular/core';
import { Hall } from 'src/app/model/hall.model';
import { Location } from 'src/app/model/location.model';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('LocationListComponent', () => {
  let component: LocationListComponent;
  let fixture: ComponentFixture<LocationListComponent>;
  let locationService: any;

  beforeEach(async(() => {
    const locationServiceMock = {
      getLocations: jasmine.createSpy('getLocations')
        .and.returnValue(Observable.of([{
          id: 100,
          name: 'Loc1',
          city: 'Beograd',
          address: 'Adresa',
          longitude: 15,
          latitude: 15,
          image: '',
          halls: [],
          active: true,
          totalSize: 1
        },
        ])),
      deleteLocation: jasmine.createSpy('deleteLocation')
        .and.returnValue(Observable.of({
          id: 100,
          name: 'Loc1',
          city: 'Beograd',
          address: 'Adresa',
          longitude: 15,
          latitude: 15,
          image: '',
          halls: [],
          active: false,
          totalSize: 1
        })),
      updateLocation: jasmine.createSpy('updateLocation')
        .and.returnValue(Observable.of({
          id: 100,
          name: 'Loc1',
          city: 'Beograd',
          address: 'Adresa',
          longitude: 15,
          latitude: 15,
          image: '',
          halls: [],
          active: true,
          totalSize: 1
        }))
    };
    TestBed.configureTestingModule({
      declarations: [LocationListComponent],
      imports: [MaterialModule, RouterTestingModule.withRoutes([]), BrowserAnimationsModule],
      providers: [{ provide: LocationService, useValue: locationServiceMock },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationListComponent);
    component = fixture.componentInstance;
    locationService = TestBed.get(LocationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should fetch locations', async(() => {
    component.ngOnInit();
    expect(locationService.getLocations).toHaveBeenCalled();

    fixture.whenStable()
      .then(() => {
        expect(component.locations.length).toBe(1); // mock categoriesService returned 3 empty objects
        fixture.detectChanges(); // synchronize HTML with component data
        expect(component.locations[0].name).toBe('Loc1');

      });
  }));
  it('should emit createLocation', () => {
    spyOn(component.locationCreateEvent, 'emit').and.callThrough();
    component.newLocation();
    const arg: any = (component.locationCreateEvent.emit as any).calls.mostRecent().args[0];
    expect(component.locationCreateEvent.emit).toHaveBeenCalled();
  });

  it('should call delete location', () => {
    const id = 100;
    const l: Location = {
      id: 100,
      name: 'Loc1',
      city: 'Beograd',
      address: 'Adresa',
      longitude: '15',
      latitude: '15',
      image: '',
      halls: new Array<Hall>(),
      active: true,
      totalSize: 1
    };
    component.deactivate(l);
    expect(locationService.deleteLocation).toHaveBeenCalledWith(100);
    expect(l.active).toBe(false);
  });

  it('should call activate location', () => {
    const id = 100;
    const l: Location = {
      id: 100,
      name: 'Loc1',
      city: 'Beograd',
      address: 'Adresa',
      longitude: '15',
      latitude: '15',
      image: '',
      halls: new Array<Hall>(),
      active: false,
      totalSize: 1
    };
    component.activate(l);
    expect(locationService.updateLocation).toHaveBeenCalledWith(l);
    expect(l.active).toBe(true);
  });
});
