import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppCommonModule } from '../app-common/app-common.module';
import { LocationManagerComponent } from './location-manager/location-manager.component';
import { LocationCreateComponent } from './location-create/location-create.component';
import { LocationListComponent } from './location-list/location-list.component';
import { LocationInfoComponent } from './location-info/location-info.component';
import { HallInfoComponent } from './hall-info/hall-info.component';
import { HallCreateComponent } from './hall-create/hall-create.component';
import { SectionInfoComponent } from './section-info/section-info.component';
import { SectionCreateComponent } from './section-create/section-create.component';
import { ShowOnMapComponent } from './show-on-map/show-on-map.component';
import { AngularYandexMapsModule } from 'angular8-yandex-maps';

@NgModule({
  declarations: [
    LocationManagerComponent,
    LocationCreateComponent,
    LocationListComponent,
    LocationInfoComponent,
    HallInfoComponent,
    HallCreateComponent,
    SectionInfoComponent,
    SectionCreateComponent,
    ShowOnMapComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    AppCommonModule,
    FormsModule,
    AngularYandexMapsModule
  ],
  exports: [
    LocationManagerComponent,
    LocationCreateComponent,
    LocationListComponent,
    LocationInfoComponent,
    HallInfoComponent,
    HallCreateComponent,
    SectionInfoComponent,
    SectionCreateComponent,
    ShowOnMapComponent
  ], entryComponents: [ShowOnMapComponent],
})
export class LocationModule { }
