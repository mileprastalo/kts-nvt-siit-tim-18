import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '../../model/location.model';
import { LocationService } from '../../service/location.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Location as BackLocation } from '@angular/common';

@Component({
  selector: 'app-location-create',
  templateUrl: './location-create.component.html',
  styleUrls: ['./location-create.component.scss']
})
export class LocationCreateComponent implements OnInit {
  locationData: Location = new Location();
  locationForm: FormGroup;
  hide = true;
  originalName: string;
  @Input() operation: string;
  @Input() locationId: number;
  @Output() backToLocationInfo = new EventEmitter();
  @Output() backToLocationEvent = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private locationService: LocationService, private router: Router,
              private backLocation: BackLocation, private snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.locationForm = this.formBuilder.group({
      name: ['', [
        Validators.required
      ]],
      city: ['', [
        Validators.required,
      ]],
      address: ['', [
        Validators.required,
      ]],
      longitude: ['', [
        Validators.required,
      ]],
      latitude: ['', [
        Validators.required,
      ]]
    });
    if (this.operation === 'update') {
      this.locationService.getLocation(this.locationId).subscribe(
        (response => {
          this.locationForm.get('name').setValue(response.name);
          this.locationForm.get('city').setValue(response.city);
          this.locationForm.get('address').setValue(response.address);
          this.locationForm.get('longitude').setValue(response.longitude);
          this.locationForm.get('latitude').setValue(response.latitude);
          this.originalName = this.locationData.name;
          this.locationData.id = response.id;
          this.locationData.image = response.image;
          this.locationData.halls = response.halls;
        }),
        (error) => {
          this.snackBar.open(error.error.message);
        }
      );
    }
  }
  onLocationSubmit() {
    if (this.locationForm.valid) {
      const location = this.getLocation();
      this.locationService.createLocation(location).subscribe(
        (response => {
          this.backToLocationEvent.emit();
        }),
        (error) => {
          this.snackBar.open(error.error.message);
        }
      );
    } else {
      this.snackBar.open('Form is not valid');
    }
  }
  onLocationUpdate() {
    if (this.locationForm.valid) {
      const location = this.getLocation();
      this.locationService.updateLocation(location).subscribe(
        (response => {
          this.snackBar.open('Location has been updated');
          this.backToLocationInfo.emit();
        }),
        (error) => {
          this.snackBar.open(error.error.message);
        }
      );
    } else {
      this.snackBar.open('Form is not valid');
    }
  }
  cancel() {
    if (this.operation === 'create') {
      this.backToLocationEvent.emit();
    } else {
      this.backToLocationInfo.emit();
    }
  }
  openInput() {
    document.getElementById('fileInput').click();
  }
  fileChange(files) {
    const file = files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.locationData.image = reader.result.toString();
      console.log(this.locationData);
      this.snackBar.open('Image loaded');
    };
    reader.onerror = error => {
      console.log('Error: ', error);
      this.snackBar.open('error loading image');
    };
  }
  getImage(): string {
    if (this.locationData.image !== undefined) {
      if (this.locationData.image.startsWith('/images')) {
        return 'http://localhost:8080' + this.locationData.image;
      } else {
        return this.locationData.image;
      }
    }
    return '';
  }
  getLocation(): Location {
    const location = new Location();
    location.id = this.locationData.id;
    location.name = this.locationForm.controls.name.value;
    location.active = true;
    location.city = this.locationForm.controls.city.value;
    location.address = this.locationForm.controls.address.value;
    location.image = this.locationData.image;
    location.latitude = this.locationForm.controls.latitude.value;
    location.longitude = this.locationForm.controls.longitude.value;
    location.halls = this.locationData.halls;
    return location;
  }

}
