
export class ReportLocation {

    locationId: number;
    fromDateTs: number;
    toDateTs: number;
    type: string;

   constructor(locationId?: number, fromDate?: number, toDate?: number, reportType?: string) {

        this.locationId = locationId;
        this.fromDateTs = fromDate;
        this.toDateTs = toDate;
        this.type = reportType;
    }

}
