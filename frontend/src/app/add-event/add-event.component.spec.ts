import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEventComponent } from './add-event.component';
import { Observable } from 'rxjs';
import { MaterialModule } from '../material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CategoryService } from '../service/category.service';
import { LocationService } from '../service/location.service';
import { EventService } from '../service/event.service';
import { FormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { Event } from '../model/event.model';
import { AddEvent } from '../model/add-event.model';
import { Category } from '../model/category.model';

describe('AddEventComponent', () => {
  let component: AddEventComponent;
  let fixture: ComponentFixture<AddEventComponent>;
  let eventService: any;
  let locationService: any;
  let categoriesService: any;

  beforeEach(async(() => {

    const locationServiceMock = {
      getAllLocationNames: jasmine.createSpy('getAllLocationNames')
        .and.returnValue(Observable.of([
          'Spens', 'Beogradska arena', 'Sava Centar'
        ])),

    };

    const categoryServiceMock = {
      getAllCategories: jasmine.createSpy('getAllCategories')
        .and.returnValue(Observable.of([{
          id: 100,
          name: 'music',
          color: '#32a852',
          icon: '/category-icons//music.png'
        },
        {
          id: 200,
          name: 'sports',
          color: '#3863ba',
          icon: '/category-icons//sports.png'
        },
        {
          id: 300,
          name: 'cinema',
          color: '#8438ba',
          icon: '/category-icons//cinema.png'
        }])),
    };

    const eventServiceMock = {
      addEvent: jasmine.createSpy('addEvent')
        .and.returnValue(Observable.of(5)),
    };
    TestBed.configureTestingModule({
      declarations: [AddEventComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [MaterialModule, BrowserAnimationsModule, FormsModule, OwlDateTimeModule, OwlNativeDateTimeModule],
      providers: [{ provide: CategoryService, useValue: categoryServiceMock },
      { provide: LocationService, useValue: locationServiceMock },
      { provide: EventService, useValue: eventServiceMock }]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(AddEventComponent);
    component = fixture.componentInstance;
    locationService = TestBed.get(LocationService);
    categoriesService = TestBed.get(CategoryService);
    eventService = TestBed.get(EventService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch categories', async(() => {
    component.ngOnInit();
    expect(categoriesService.getAllCategories).toHaveBeenCalled();

    fixture.whenStable()
      .then(() => {
        expect(component.categories.length).toBe(3); // mock categoriesService returned 3 empty objects
        fixture.detectChanges(); // synchronize HTML with component data
        expect(component.categories[0].name).toBe('music');

      });
  }));

  it('should fetch locations', async(() => {
    component.ngOnInit();
    expect(locationService.getAllLocationNames).toHaveBeenCalled();

    fixture.whenStable()
      .then(() => {
        expect(component.locationNames.length).toBe(3); // mock categoriesService returned 3 empty objects
        fixture.detectChanges(); // synchronize HTML with component data
        expect(component.locationNames[0]).toBe('Spens');

      });
  }));

  it('should transform event sections', () => {
    const event = new AddEvent();
    event.eventSections = new Map().set(100, 200);
    component.newEvent = event;
    component.getEventSections();
    expect(component.getEventSections()).toBe('({"100":200})');
  });

  it('should call add event', () => {
    const category: Category = {
      id: 300,
      name: 'cinema',
      color: '#8438ba',
      icon: '/category-icons//cinema.png'
    };
    const event: AddEvent = {
      name: 'Event Test',
      description: 'Event test description.',
      date: [new Date(2020, 10, 10)],
      salesDate: new Date(2020, 4, 4),
      applicationDate: new Date(2020, 3, 3),
      status: 0,
      lastDayToPay: 1,
      controllAccess: true,
      attachments: [],
      locationID: 100,
      hallID: 100,
      eventSections: new Map().set(100, 200),
      categories: [category],
      background: null,
      backgroundName: ''
    };
    const eventSection = '({"100":200})';
    component.newEvent = event;
    component.addEvent();
    expect(eventService.addEvent).toHaveBeenCalledWith(event, eventSection);
  });
});
