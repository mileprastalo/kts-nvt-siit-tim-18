import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { LocationModule } from '../location/location.module';

import { EventCardComponent } from './event-card/event-card.component';

@NgModule({
  declarations: [
    EventCardComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    LocationModule
  ],
  exports: [
    EventCardComponent
  ]
})
export class EventsModule { }
