import {
  ValidationErrors,
  ValidatorFn,
  Validators,
  AbstractControl
} from '@angular/forms';

export function atLeastOneRequired(...controls: AbstractControl[]): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    for (const aControl of controls) {
      if (!Validators.required(aControl)) {
        return null;
      }
    }
    return { atLeastOneRequired: { valid: false } };
  };
}
