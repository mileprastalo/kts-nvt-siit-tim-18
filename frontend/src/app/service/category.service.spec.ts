import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CategoryService } from './category.service';
import { ConstantsService } from './constants.service';

describe('CategoryService', () => {
  let categoryService: CategoryService;
  let constantsService: ConstantsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ CategoryService, ConstantsService ]
    });

    categoryService = TestBed.get(CategoryService);
    constantsService = TestBed.get(ConstantsService);
    httpMock = TestBed.get(HttpTestingController);

  });

  it('should be created', () => {
    const service: CategoryService = TestBed.get(CategoryService);
    expect(service).toBeTruthy();
  });

  it(`getAllCategories() should return all categories`, async(inject([HttpTestingController, CategoryService],
    (httpClient: HttpTestingController, catService: CategoryService) => {

      const postItem = [
        {
          id: 100,
          name: 'music',
          color: '#32a852',
          icon: '/category-icons//music.png'
        },
        {
          id: 200,
          name: 'sports',
          color: '#3863ba',
          icon: '/category-icons//sports.png'
        },
        {
          id: 300,
          name: 'cinema',
          color: '#8438ba',
          icon: '/category-icons//cinema.png'
        }
      ];


      catService.getAllCategories()
        .subscribe((posts: any) => {
          expect(posts.length).toBe(3);

          expect(posts[0].id).toBe(100);
          expect(posts[0].name).toBe('music');
          expect(posts[0].color).toBe('#32a852');
          expect(posts[0].icon).toBe('/category-icons//music.png');

          expect(posts[1].id).toBe(200);
          expect(posts[1].name).toBe('sports');
          expect(posts[1].color).toBe('#3863ba');
          expect(posts[1].icon).toBe('/category-icons//sports.png');

          expect(posts[2].id).toBe(300);
          expect(posts[2].name).toBe('cinema');
          expect(posts[2].color).toBe('#8438ba');
          expect(posts[2].icon).toBe('/category-icons//cinema.png');
        });

      const reqest = httpMock.expectOne(constantsService.categoriesPath);
      expect(reqest.request.method).toBe('GET');

      reqest.flush(postItem);
      httpMock.verify();

    })));

  it(`getCategoryById() should return category with specified id`, async(inject([HttpTestingController, CategoryService],
      (httpClient: HttpTestingController, catService: CategoryService) => {

        const categoryId = 200;
        const postItem = [
          {
            id: categoryId,
            name: 'sports',
            color: '#3863ba',
            icon: '/category-icons//sports.png'
          }
        ];

        catService.getCategoryById(categoryId)
          .subscribe((posts: any) => {
            expect(posts.length).toBe(1);
            expect(posts[0].id).toBe(categoryId);
          });

        const reqest = httpMock.expectOne(constantsService.categoriesPath + '/' + categoryId);
        expect(reqest.request.method).toBe('GET');

        reqest.flush(postItem);
        httpMock.verify();

      })));

  it(`addCategory() should query url and save a category`, async(inject([HttpTestingController, CategoryService],
        (httpClient: HttpTestingController, catService: CategoryService) => {

          const newCategory = {
            id: null,
            name: 'fun',
            color: '#3863ba',
            icon: '/category-icons//theater.png'
          };

          const postItem = [{
            id: 101,
            name: 'fun',
            color: '#3863ba',
            icon: '/category-icons//theater.png'
          }];

          catService.addCategory(newCategory)
            .subscribe((posts: any) => {
              expect(posts.length).toBe(1);
              expect(posts[0].id).toBe(101);
              expect(posts[0].name).toBe('fun');
              expect(posts[0].color).toBe('#3863ba');
              expect(posts[0].icon).toBe('/category-icons//theater.png');
            });

          const reqest = httpMock.expectOne(constantsService.categoriesPath);
          expect(reqest.request.method).toBe('POST');

          reqest.flush(postItem);
          httpMock.verify();

        })));

  it(`updateCategory() should query url and update a category`, async(inject([HttpTestingController, CategoryService],
          (httpClient: HttpTestingController, catService: CategoryService) => {

            const categoryId = 101;
            const updatedCategory = {
              id: categoryId,
              name: 'fun updated',
              color: '#3863ba',
              icon: '/category-icons//theater.png'
            };

            const postItem = [updatedCategory];

            catService.updateCategory(updatedCategory)
              .subscribe((posts: any) => {
                expect(posts.length).toBe(1);
                expect(posts[0].id).toBe(categoryId);
                expect(posts[0].name).toBe('fun updated');
                expect(posts[0].color).toBe('#3863ba');
                expect(posts[0].icon).toBe('/category-icons//theater.png');
              });

            const reqest = httpMock.expectOne(constantsService.categoriesPath + '/' + categoryId);
            expect(reqest.request.method).toBe('PUT');

            reqest.flush(postItem);
            httpMock.verify();

          })));

  it('deleteCategory() should query url and delete a category', async(inject([HttpTestingController, CategoryService],
            (httpClient: HttpTestingController, catService: CategoryService) => {

              const categoryId = 101;
              const postItem = [true];

              catService.deleteCategory(categoryId)
                .subscribe((posts: any) => {
                  expect(posts.length).toBe(1);
                  expect(posts[0]).toBe(true);
                });

              const reqest = httpMock.expectOne(constantsService.categoriesPath + '/' + categoryId);
              expect(reqest.request.method).toBe('DELETE');

              reqest.flush(postItem);
              httpMock.verify();

            })));
});
