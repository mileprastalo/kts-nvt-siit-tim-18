import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Location } from '../model/location.model';
import { PageEvent, MatDialog } from '@angular/material';
import { ShowOnMapComponent } from '../location/show-on-map/show-on-map.component';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient, public dialog: MatDialog, private constants: ConstantsService) { }

  getLocations(event: PageEvent): Observable<Location[]> {
    if (!event) {
      return this.http.get<Location[]>(this.constants.locationsPath);
    } else {
      return this.http.get<Location[]>(this.constants.locationsPath + '?page=' + event.pageIndex + '&size=' + event.pageSize);
    }
  }
  getAllLocationNames(): Observable<string[]> {
    return this.http.get<string[]>(this.constants.locationsPath + '/getAllLocationNames');
  }
  getLocationByName(name: string): Observable<Location> {
    return this.http.get<Location>(this.constants.locationsPath + '/getLocationByName/' + name);
  }
  getLocation(id: number): Observable<Location> {
    return this.http.get<Location>(this.constants.locationsPath + '/' + id);
  }
  createLocation(dto: Location): Observable<Location> {
    return this.http.post<Location>(this.constants.locationsPath, dto);
  }
  updateLocation(dto: Location): Observable<Location> {
    return this.http.put<Location>(this.constants.locationsPath, dto);
  }
  deleteLocation(id: number): Observable<Location> {
    return this.http.delete<Location>(this.constants.locationsPath + '/' + id);
  }

  showOnMap(locationName: string) {
    // get location and get its coords
    this.getLocationByName(locationName).subscribe(
      (response => {

        if (response != null) {
          const dialogRef = this.dialog.open(ShowOnMapComponent, {
            data: { latitude: response.latitude, longitude: response.longitude }
          });
        }
      }),
      (error => {
        console.log(error.error.message);
      }));

  }

}
