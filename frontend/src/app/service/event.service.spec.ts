import { TestBed, inject, async } from '@angular/core/testing';

import { EventService } from './event.service';
import { ConstantsService } from './constants.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from '../material/material.module';
import { AddEvent } from '../model/add-event.model';
import { Category } from '../model/category.model';
import { EventApplication } from '../model/event-application.model';

fdescribe('EventService', () => {
  let eventService: EventService;
  let constantsService: ConstantsService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MaterialModule],
      providers: [EventService, ConstantsService]
    });
    eventService = TestBed.get(EventService);
    constantsService = TestBed.get(ConstantsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: EventService = TestBed.get(EventService);
    expect(service).toBeTruthy();
  });

  it(`addEvent() should return Event id`, async(inject([HttpTestingController, EventService],
    (httpClient: HttpTestingController, eService: EventService) => {

      const category: Category = {
        id: 300,
        name: 'cinema',
        color: '#8438ba',
        icon: '/category-icons//cinema.png'
      };
      const event: AddEvent = {
        name: 'Event Test',
        description: 'Event test description.',
        date: [new Date(2020, 10, 10)],
        salesDate: new Date(2020, 4, 4),
        applicationDate: new Date(2020, 3, 3),
        status: 0,
        lastDayToPay: 1,
        controllAccess: true,
        attachments: [],
        locationID: 100,
        hallID: 100,
        eventSections: new Map().set(100, 200),
        categories: [category],
        background: null,
        backgroundName: ''
      };
      const eventSection = '({"100":200})';
      eService.addEvent(event, eventSection)
        .subscribe((response: any) => {

          expect(response).toBe(5);

        });

      const reqest = httpMock.expectOne(constantsService.eventsPath + '/add-event');
      expect(reqest.request.method).toBe('POST');

      reqest.flush(5);
      httpMock.verify();

    })));

  it(`getEventApplicationByEvent() should return EventApplication`, async(inject([HttpTestingController, EventService],
    (httpClient: HttpTestingController, eService: EventService) => {

      const eventApplication: EventApplication = {
        id: 1,
        userId: 1,
        eventId: 1,
        queueNumber: 5,
        downInterval: 515125124124,
        upInterval: 515125967969,
        eventName: 'Test Event'
      };

      eService.getEventApplicationByEvent(1)
        .subscribe((response: any) => {

          expect(response.id).toBe(1);
          expect(response.userId).toBe(1);
          expect(response.eventId).toBe(1);
          expect(response.queueNumber).toBe(5);
          expect(response.downInterval).toBe(515125124124);
          expect(response.upInterval).toBe(515125967969);
          expect(response.eventName).toBe('Test Event');

        });

      const reqest = httpMock.expectOne(constantsService.eventsPath + '/get-event-application/1');
      expect(reqest.request.method).toBe('GET');

      reqest.flush(eventApplication);
      httpMock.verify();

    })));

  it(`addEventApplication() should return EventApplication id`, async(inject([HttpTestingController, EventService],
    (httpClient: HttpTestingController, eService: EventService) => {

      eService.addEventApplication(1)
        .subscribe((response: any) => {

          expect(response).toBe(1);

        });

      const reqest = httpMock.expectOne(constantsService.eventsPath + '/add-event-application/1');
      expect(reqest.request.method).toBe('PUT');

      reqest.flush(1);
      httpMock.verify();

    })));
});
