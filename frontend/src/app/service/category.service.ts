import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Category } from '../model/category.model';
import { ConstantsService } from './constants.service';

@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    constructor(private http: HttpClient, private constants: ConstantsService) { }

    getAllCategories(): Observable<Category[]> {
        return this.http.get<Category[]>(this.constants.categoriesPath);
    }

    getCategoryById(id: number): Observable<Category> {
        return this.http.get<Category>(this.constants.categoriesPath + '/' + id);
    }

    addCategory(category: Category): Observable<Category> {
        return this.http.post<Category>(this.constants.categoriesPath, category);
    }

    updateCategory(category: Category): Observable<Category> {
        return this.http.put<Category>(this.constants.categoriesPath + '/' + category.id, category);
    }

    deleteCategory(id: number): Observable<boolean> {
        return this.http.delete<boolean>(this.constants.categoriesPath + '/' + id);
    }
}
