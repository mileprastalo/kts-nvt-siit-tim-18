import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { AppCommonModule } from 'src/app/app-common/app-common.module';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { User } from 'src/app/model/user.model';
import { DebugElement } from '@angular/core';

fdescribe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let userService: any;
  const newUser = new User();
  newUser.id = 1;
  newUser.username = 'pera';
  newUser.password = 'password';
  newUser.repeatedPassword = 'password';
  newUser.firstName = 'Pera';
  newUser.lastName = 'Peric';
  newUser.email = 'pera@gmail.com';
  newUser.phone = '023/848-848';

  beforeEach(async(() => {

    const userServiceMock = {
      registerUser: jasmine.createSpy('registerUser').and.returnValue(Observable.of(newUser))
    };

    TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      imports: [AppCommonModule, MaterialModule, ReactiveFormsModule, BrowserAnimationsModule],
      providers: [{provide: UserService, useValue: userServiceMock },
        {provide: Router }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should register user', () => {
    component.registerForm.controls.username.setValue('mira');
    component.registerForm.controls.password.setValue('password');
    component.registerForm.controls.repeatedPassword.setValue('password');
    component.registerForm.controls.firstName.setValue('Mira');
    component.registerForm.controls.lastName.setValue('Miric');
    component.registerForm.controls.email.setValue('mira@gmail.com');
    component.registerForm.controls.phone.setValue('023/848-678');

    component.onRegisterSubmit();
    expect(userService.registerUser).toHaveBeenCalled();

    fixture.whenStable()
      .then(() => {

        fixture.detectChanges(); // synchronize HTML with component data
        const element: DebugElement =
          fixture.debugElement.nativeElement.query('#registerMessage');
        expect(element).toContain('Successful registration. You will soon recieve email for confirmation your account.');

      });
  });
});
