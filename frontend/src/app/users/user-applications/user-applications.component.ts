import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { EventApplication } from 'src/app/model/event-application.model';
import { EventService } from 'src/app/service/event.service';

@Component({
  selector: 'app-user-applications',
  templateUrl: './user-applications.component.html',
  styleUrls: ['./user-applications.component.scss']
})
export class UserApplicationsComponent implements OnInit {


  applications: EventApplication[];
  displayedColumns = ['eventName', 'downInterval', 'upInterval',  'queueNumber'];
  dataSource: MatTableDataSource<EventApplication>;

  constructor(private eventService: EventService, private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.eventService.getEventApplications().subscribe(
      (response => {
          this.applications = response;
          this.dataSource = new MatTableDataSource(this.applications);
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  transformDate(d: number) {
    return new Date(d);
  }

}
