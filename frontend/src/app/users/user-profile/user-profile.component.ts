import { Component, OnInit, Output } from '@angular/core';
import { User } from '../../model/user.model';
import { MatSnackBar, PageEvent } from '@angular/material';
import { UserService } from '../../service/user.service';
import { UserTicket } from '../../model/user-ticket.model';
import { PayPal } from '../../model/paypal.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  user: User;

  page: PageEvent = new PageEvent();
  pageBought: PageEvent = new PageEvent();
  boughtUserTickets: UserTicket[];
  reservedUserTickets: UserTicket[];

  paypal: PayPal;

  constructor(private userService: UserService, private snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.page.pageIndex = 0;
    this.pageBought.pageIndex = 0;
    this.page.pageSize = 4;
    this.pageBought.pageSize = 4;
    this.getUserProfile();
    this.getUserTickets(true);
    this.getUserTickets(false);
  }
  // Get UserProfile.
  getUserProfile() {
    this.userService.getUser().subscribe(
      (response => {
        if (response) {
          this.user = response;
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  receivePage($event, bought: boolean) {
    console.log('Recieved');
    this.page = $event;
    this.getUserTickets(bought);
  }

  receivePageBought($event, bought: boolean) {
    console.log('Recieved');
    this.pageBought = $event;
    this.getUserTickets(bought);
  }

  getUserTickets(bought: boolean) {
    let page: PageEvent = null;
    if (bought) {
      page = this.pageBought;
    } else {
      page = this.page;
    }
    this.userService.getUserTickets(bought, page).subscribe(
      (response: UserTicket[]) => {

        if (response != null) {
          if (bought) {
            this.boughtUserTickets = response;
          } else {
            this.reservedUserTickets = response;
          }
          if (response.length > 0) {
            page.length = response[0].totalSize;
          } else {
            page.length = 0;
          }
        }
      });
  }
}
