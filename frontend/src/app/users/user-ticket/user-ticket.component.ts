import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserTicket } from '../../model/user-ticket.model';
import { PageEvent, MatSnackBar } from '@angular/material';
import { ConstantsService } from '../../service/constants.service';
import { Router, ActivatedRoute } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { PayPal } from 'src/app/model/paypal.model';
import { PaymentCanceled } from 'src/app/model/payment-canceled.model';
import { TicketService } from 'src/app/service/ticket.service';
import { ConfirmationDialogComponent } from 'src/app/app-common/confirmation-dialog/confirmation-dialog.component';
import { LocationService } from 'src/app/service/location.service';



@Component({
  selector: 'app-user-ticket',
  templateUrl: './user-ticket.component.html',
  styleUrls: ['./user-ticket.component.scss']
})
export class UserTicketsComponent implements OnInit {

  @Input() userTickets: Array<UserTicket>;
  @Input() page: PageEvent;
  @Output() messageEvent = new EventEmitter<PageEvent>();
  @Input() paypal: PayPal;
  @Input() paymentCanceled: PaymentCanceled;
  @Input() reservations = false;


  constructor(private constants: ConstantsService,
              private router: Router,
              private ticketService: TicketService,
              private locationService: LocationService,
              private snackBar: MatSnackBar,
              public dialog: MatDialog,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {

    if (this.reservations) {
      this.paypal = this.activatedRoute.snapshot.data.paypal;
      this.paymentCanceled = this.activatedRoute.snapshot.data.paymentCanceled;

      if ( this.paypal.paymentID ) {
        this.buyReservationCompletePayment();
      } else if ( this.paymentCanceled.canceled ) {
        if ( this.paymentCanceled.deleteReservations) {
          this.cancelTickets();
        }
        this.snackBar.open('Payment is canceled', '', {
          duration: 3000
        });
      }
      this.router.navigateByUrl('/user-profile');

  }

  }

  sendPage($event) {
    console.log('Sent');
    this.page = $event;
    console.log(this.page);
    this.messageEvent.emit(this.page);
  }

  sortByDateOFPurchase() {
    this.userTickets = this.userTickets.sort((t1, t2) => {
      const d1 = this.getDateFromString(t1.dayOfPurchase);
      const d2 = this.getDateFromString(t2.dayOfPurchase);
      if (d1 > d2) {
        return 1;
      }
      return -1;
    });
  }

  sortByEventDate() {
    this.userTickets = this.userTickets.sort( (t1, t2) => {
      const d1 = this.getDateFromString(t1.forDay);
      const d2 = this.getDateFromString(t2.forDay);
      if (d1 > d2) {
        return 1;
      }
      return -1;
    });
  }
  sortByPrice() {
    this.userTickets = this.userTickets.sort((t1, t2) => {
      if (t1.price > t2.price) {
        return 1;
      }
      return -1;
    });
  }

  getDateFromString( date: string): Date {
    const parts = date.split('.');
    const stringDate: string =  parts[1] + '.' + parts[0] + '.' + parts[2] + parts[3];
    return new Date(stringDate);
  }

  goToDetails(eventId: number) {
    this.router.navigateByUrl('/show-event/' + eventId);
  }

  showOnMap(locationName: string) {
    this.locationService.showOnMap(locationName);
  }

  buyReservationCompletePayment() {

    const ids: number[] = JSON.parse(localStorage.getItem('buyReservationIds'));

    this.paypal.reservationIds = ids;
    let observable: Observable<any> = null;

    if ( this.paypal.cancelIfFail) {
      observable = this.ticketService.buyTicketCompletePayment(this.paypal);
    } else {
      observable = this.ticketService.buyReservedTicketCompletePayment(this.paypal);
    }

    observable.subscribe(
      (response => {
        console.log('Complete response: ' + JSON.stringify(response));
        localStorage.setItem('buyReservationIds', null);
        this.snackBar.open('Your purchase was successful', '', {
          duration: 3000
        });
        this.router.navigateByUrl('/userProfile');
      }),
      (error => {
        console.log('Complete response: ' + JSON.stringify(error));
        this.snackBar.open(error.error.message, '', {
          duration: 4000,
          panelClass: ['error-snack-bar']
        });
      })
    );
  }

  buy(ticket: UserTicket) {

    const ids = [ticket.id];
    localStorage.setItem('buyReservationIds', JSON.stringify(ids));

    this.ticketService.buyReservedTicketCreatePayment(ticket.id).subscribe(
      (response => {
        console.log(JSON.stringify(response));
        window.location = response.redirect_url;
      }),
      (error => {
        console.log(error.error.message);
        this.snackBar.open(error.error.message, '', {
          duration: 4000,
          panelClass: ['error-snack-bar']
        });
      })
    );
  }

  cancel(ticket: UserTicket) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: 'Are you sure you want to cancel reservation for ' + ticket.eventName + '?'
     });

    dialogRef.afterClosed().subscribe(result => {
       if ( result === true ) {
         this.cancelConfirmed(ticket.id);
        }
     });
  }

  cancelConfirmed(id: number) {
    this.ticketService.cancelTicket([id]).subscribe(
      (response => {
        this.snackBar.open('Ticket canceled');
      }),
      (error => {
        console.log(error.error.message);
        this.snackBar.open(error.error.message);
      })
    );
  }

  cancelTickets() {
    const ids: number[] = JSON.parse(localStorage.getItem('buyReservationIds'));
    this.ticketService.cancelTicket(ids).subscribe(
      (response => {

      }),
      (error => {
        console.log(error.error.message);
        this.snackBar.open(error.error.message);
      })
    );
  }

}
