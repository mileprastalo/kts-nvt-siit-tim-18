import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AppCommonModule } from '../app-common/app-common.module';
import { EventsModule } from '../events/events.module';

import { HomeComponent } from './home/home.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { LocationModule } from '../location/location.module';
import { AngularYandexMapsModule } from 'angular8-yandex-maps';

@NgModule({
  declarations: [
    HomeComponent,
    SearchFormComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    AppCommonModule,
    EventsModule,
    LocationModule,
    AngularYandexMapsModule
  ],
  exports: [
    HomeComponent,
    SearchFormComponent
  ]
})
export class HomeModule { }
